function loadReport(){
var classname = $('.class-name').html();
var examname = $('#exam_dropdown').val();
var ajaxurl = '/index.php/class/reports/loadreport';
var subjectname = $('#sub_name').html();
data={'action': 'loadReport', 'classname':classname, 'examname':examname,'subjectname': subjectname};
$.post(ajaxurl, data, function(response){
if(response){
$('#reportcontent').html(response);
}
}).fail(function(error) {
    console.log(JSON.stringify(error));
  });
}

function loadswReport(){
var classname = $('.class-name').html();
var examname = $('#exam_dropdown').val();
var ajaxurl = '/index.php/class/reports/loadswreport';
/*var subjectname = $('#sub_name').html();*/
data={'action': 'loadswReport', 'classname':classname, 'examname':examname};//,'subjectname': subjectname};
$.post(ajaxurl, data, function(response){
if(response){
//alert(response);
$('#reportcontent').html(response);
}
}).fail(function(error) {
    console.log(JSON.stringify(error));
  });
}

function loaddrillreport(subjectname){
var classname = $('.class-name').html();
var examname = $('#exam_dropdown').val();
var ajaxurl = '/index.php/class/reports/loadreport';
var subjectname = "subject_"+subjectname;
data={'action': 'loadReport', 'classname':classname, 'examname':examname,'subjectname': subjectname,'drill': '1'};
$.post(ajaxurl,data,function(response){
if(response){
$('#reportcontent').html(response);
}
}).fail(function(error){
	console.log(JSON.stringify(error));
});
}

$(document).ready(function(){
$('#exam-value').click(function(){
$("#exam_dropdown div").toggle( "drop",{direction: "up"}, 200 );
});

$("#exam_dropdown > div").click(function() {
$('#exam-value').html($(this).html());
$('#exam-value').attr('class',$(this).attr('id'));
$("#exam_dropdown div").toggle( "drop",{direction: "up"}, 200 );
loadReport();
});

$('#exam_dropdown').change(function(){
$('#nav-addedit').remove();
$('#nav-calrank').remove();
loadReport();
});

});
