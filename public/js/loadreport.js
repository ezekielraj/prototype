function markUpdate(stuname,mark){
var examname = $('#exam_dropdown').val();
var subjectname = $('#sub_name').html();
//alert(stuname+mark+examname+subjectname);
var ajaxurl = '/index.php/class/reports/markupdate';
data={'action': 'markUpdate', 'studentname':stuname, 'examname':examname,'subjectname': subjectname,'mark':mark};
$.post(ajaxurl, data, function(response){
if(response=="no"){
$('#mymodal').text("Mark Cannot be updated");
//$('.'+stuname+'_status').text("Mark Cannot be updated");
}else{
$('#mymodal').text("Mark Updated");
//..$('.'+stuname+'_status').text("updated");
}
//$('.'+stuname).css('background-color','#E1E1E1');
});
}
function outofmarkUpdate(stuname,mark){
var examname = $('#exam_dropdown').val();
var subjectname = $('#sub_name').html();
//alert(stuname+mark+examname+subjectname);
var ajaxurl = '/index.php/class/reports/outofmarkupdate';
data={'action': 'outofmarkUpdate', 'studentname':stuname, 'examname':examname,'subjectname': subjectname,'mark':mark};
$.post(ajaxurl, data, function(response){
if(response=="no"){
$('#mymodal').text("Out of Mark Cannot be updated");
//$('.'+stuname+'_status').text("Out of Mark Cannot be updated");
}else{
$('#mymodal').text("OutofMark Updated");
//$('.'+stuname+'_status').text("updated");
}
});
}

function loadAddedit(){
var classname = $('.class-name').html();
var examname = $('#exam_dropdown').val();
var ajaxurl = '/index.php/class/reports/loadaddedit';
var subjectname = $('#sub_name').html();
data={'action': 'loadAddedit', 'classname':classname, 'examname':examname,'subjectname': subjectname};
$.post(ajaxurl, data, function(response){
if(response){
$('#nav-addedit').remove();
$('#for-afterload').css('display','none');
$('#loadreportcontainer').html(response);
$('#gp_mark,#gp_outofmark').attr('contenteditable','true');
}
});

}
$(document).ready(function(){
window.ovmark=0;
window.ovoutofmark=0;
window.oldcontent="";
window.reportoldcontent="";
$('#gp_mark,#gp_outofmark').attr('contenteditable','true');
$(document).on('click','#nav-report',function(){
loadReport();
});

$(document).on('click','#nav-swreport',function(){
loadswReport();
});

$(document).on('click','#nav-addedit',function(){
loadAddedit();
});

$(document).on('click','#gp_mark,#gp_outofmark',function(){
    $(this).attr('contenteditable','true');
//$(this).css('background-color','#ffffff');
});
$(document).on('focusin','#gp_outofmark',function(){
ovoutofmark=$(this).text();
});
$(document).on('focusin','#gp_mark',function(){
ovmark=$(this).text();
});
$(document).on('focusout','#gp_mark',function(){
$('#mymodal').text("Loading...")
$('#mymodal').show();
//$('.'+$(this).attr('class') + '_status').text("Loading...");
if(ovmark==$(this).text()){
ovmark=0;
$('#mymodal').text("Mark Updated");
setTimeout(function() {$('#mymodal').hide();}, 4000);
}else{
markUpdate($(this).attr('class'),$(this).text());
setTimeout(function() {$('#mymodal').hide();}, 4000);
}
});

$(document).on('focusout','#gp_outofmark',function(){
$('#mymodal').text("Loading...")
$('#mymodal').show();
//$('.'+$(this).attr('class') + '_status').text("Loading...");
if(ovoutofmark==$(this).text()){
ovoutofmark=0;
$('#mymodal').text("OutofMark Updated");
setTimeout(function() {$('#mymodal').hide();}, 4000);
}else{
outofmarkUpdate($(this).attr('class'),$(this).text());
setTimeout(function() {$('#mymodal').hide();}, 4000);
}
});
$(document).on('click','#old-content',function(){
loadReport();
$('#for-afterload').css('display','block');
//$('#loadreportcontainer').html(oldcontent);
oldcontent="";
});
$(document).on('click','#reportback',function(){
loadReport();
$('#for-afterload').css('display','block');
});
});
