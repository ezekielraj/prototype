window.gscontnum=0;
window.gacontnum=0;
window.wcontent=1;
function strip_tags(input, allowed){
  allowed = (((allowed || '') + '')
    .toLowerCase()
    .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); 
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<![\s\S]*?>|<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

function loadannounceContent(){

var ajaxurl = '/index.php/class/classfeed/loadannouncecontent';
var grpname = $('#grp_num').text();
data={'action': 'LAnnounceContent','grpnum': grpname,'gacontnum':gacontnum};
$.post(ajaxurl, data, function(response){
if(response){
if(gacontnum==0){
$('#shrdcont').html(response);
}else{
$('#shrdcont').append(response);

}
$('#spinner-loader1').css('display','none');
}
});


}

function loadSharedContent(){
var ajaxurl = '/index.php/class/classfeed/loadsharedcontent';
var grpname = $('#grp_num').text();
data={'action': 'LSContent','grpnum': grpname,'gscontnum':gscontnum};
$.post(ajaxurl, data, function(response){
if(response){
if(gscontnum==0){
$('#shrdcont').html(response);
}else{
$('#shrdcont').append(response);
}
$('#spinner-loader1').css('display','block');
}
});
}

function FeedUpdate(content,grpname,image,title,description){
var announce="0";
if($('#cf_announce:checked').val()=="1"){
var announce="1";
}
jQuery.ajax({
url: '/index.php/class/classfeed/feedupdate',
type: "POST",
                contentType: false,
                processData: false,
data: function(){
var data = new FormData();
data.append("action","FeedUpdate");
data.append("content",content);
data.append("image",image);
data.append("title",title);
data.append("description",description);
data.append("grpnum",grpname);
data.append("announce",announce);
data.append("attachment",$("#cf-browse-file").get(0).files[0]);
return data;
}(),
                error: function(response) {
                    alert(response.toSource());
                },
                success: function(response, textStatus) {
                    if(response){
			loadSharedContent();
$('#sharecont').val("");
$("#url_load").html("");
$('#cf_announce').prop('checked', false);
                    }
                    console.log(response, textStatus);
                }
});
}

function secthank(id){
var ajaxurl = '/index.php/class/classfeed/secthank';
data={'action': 'secThank', 'slno': id};
$.post(ajaxurl, data, function(response){
if(response){
$('#'+id).html(response);
var secres = response.split(',');
$('.'+id).html(secres[0]);
}
});
}

function updateLike(id){
var ajaxurl = '/index.php/class/classfeed/updatelike';
data={'action': 'likeUpdate', 'slno': id};
$.post(ajaxurl, data, function(response){
if(response){
var myarr = response.split(',');
$('.'+id).html(myarr[0]);
$('.lktxt_'+id+' > span').html(myarr[1]);
}
});
}

function loadcomments(id){
var ent=id.split('_');
var ajaxurl = '/index.php/class/classfeed/loadcomments';
data={'action': 'loadcomments', 'slno': id};
$.post(ajaxurl, data, function(response){
if(response){
//alert(response);
$('#comments_'+ent[2]).html(response);
$('.'+id).val("");
$('#cf-browse-file').val("");
}
});
}

function updateComment(textcont,id,grpnum){
//var grpnum = $('#grp_num').text();
var ajaxurl = '/index.php/class/classfeed/updatecomment';
data={'action': 'commentUpdate', 'textcont':textcont, 'slno': id,'grpnum':grpnum};
$.post(ajaxurl, data, function(response){
if(response=="s"){
$('#sharecont').val("");
loadcomments(id);
}
});
}

function getURL(url){
$('#spinner-loader').css('display','block');
var ajaxurl = '/index.php/class/classfeed/geturl';
var data={'action':'geturl','url':url};
$.post(ajaxurl, data, function(response){
if(response){
$('#url_load').html(response);
$('#spinner-loader').css('display','none');
}else{
$('#url_load').html("");
$('#spinner-loader').css('display','none');
}
}).error(function(){
$('#url_load').html("");
$('#spinner-loader').css('display','none');
});
}

function deleteFeed(id){
var ajaxurl='/index.php/class/classfeed/deletefeed';
data={'action':'deleteFeed','id':id};
$.post(ajaxurl,data, function(response){
if(response=="s"){
$('.feedno_'+id).remove();
}else{
alert(response);
}
});
}

$(document).ready(function() {
window.surl="";
loadSharedContent();

$(document).on('click','.cf_delete',function(){
var delid = $(this).attr('id');
var delidd=delid.split('_');
deleteFeed(delidd[1]);
});

$(document).on('keypress','#shcomment', function(event) {
var keycode = (event.keyCode ? event.keyCode : event.which);
var textcont = strip_tags($(this).val(), "");
if(keycode === '13'){
var grpnum=$(this).data('group');
updateComment(textcont,$(this).data('id'),grpnum);
}
});

$(document).on('click','.sec_submit', function(event) {
    var cla = $(this).attr('id');
    var textcont = strip_tags($('.'+cla).val(), "");
    var grpnum=$(this).data('group');
    updateComment(textcont,$(this).data('id'),grpnum);
});

$(document).on('click','.lkandcm1',function(){
 var id=$(this).data('clas');
if($(this).text()==="Reply"){
$('.txt_'+id).focus();
}
if($(this).text()==="Thank" || $(this).text()==="Unthank"){
 updateLike(id);
}
});

$(document).on('focus','#sharecont',function(){
$(this).attr('rows', '4');
$('#sharesubmit').css('display','block');
$('#head-h3').css('display','block');
$('#browse-file').css('display','block');
$('.cf_announce').css('display','block');
});
/*$(document).on('focusout','#sharecont',function(){
$(this).attr('rows', '1');
$('#sharesubmit').css('display','none');
});*/
$(document).on('click','#textboxshare #status',function(){
wcontent=1;
gscontnum=0;
loadSharedContent();
$(this).attr('class','btn btn-info btn-sm');
$('#Announcements').attr('class','btn btn-normal btn-sm');
});

$(document).on('click','#textboxshare #Announcements',function(){
wcontent=2;
gacontnum=0;
loadannounceContent();
$(this).attr('class','btn btn-info btn-sm');
$('#status').attr('class','btn btn-normal btn-sm');
});
/*
$(document).on('click','#textboxshare > div:not(#sharesubmit,.groups_names,.cf_announce)',function() {


$('#textboxshare > div:not(#sharesubmit,.groups_names,.cf_announce)').attr('class','');
$(this).attr('class','cfactive');
if($(this).attr('id')=="status"){
loadSharedContent();
}
if($(this).attr('id')=="Announcements"){
loadannounceContent();
}
});
*/
$('#sharecont').on('paste', function(e) {
    clearTimeout($(this).data('timeout'));
    $(this).data('timeout', setTimeout(function(){

var url="";
var textareacontent = strip_tags($('#sharecont').val(), "");
$(this).val(textareacontent);
if(textareacontent.indexOf('http://')>-1){
var purl = textareacontent.split('http://');
url = 'http://'+purl[1];
}else if(textareacontent.indexOf('https://')>-1){
var purl = textareacontent.split('https://');
url = 'https://'+purl[1];
}
url=url.trim();
if(url!=""){
if(surl.toString()==url.toString()){
}else{
getURL(url);
surl=url;
}
}else{
$('#url_load').html("");
}
//        getURL($('#textboxshare > textarea').val());
    }, 200));
});

$(document).on('keyup','#sharecont',function(){
var url="";
var textareacontent = strip_tags($(this).val(), "");
$(this).val(textareacontent);
if(textareacontent.indexOf('http://')>-1){
var purl = textareacontent.split('http://');
url = 'http://'+purl[1];
}else if(textareacontent.indexOf('https://')>-1){
var purl = textareacontent.split('https://');
url = 'https://'+purl[1];
}
url=url.trim();
if(url!=""){
if(surl.toString()==url.toString()){
}else{
getURL(url);
surl=url;
}
}else{
$('#url_load').html("");
}
});

$(document).on('click','#sharesubmit', function() {
var content = $('#sharecont').val().replace(/\n/g, '<br />').trim();
var grpname="";
if($('#select-group').length){
var grpname=$('#select-group').val();
}
var image = "";
var title = "";
var description = "";
if($('#url_load_full').length){
var image = $('#url_load_img[style*="display: block"]').attr('src');
var title = $(".url_load_title").val();
var description = $(".url_load_description").val();
}
if(grpname==""){
grpname = $('#grp_num').text();
}
if(content != ""){
FeedUpdate(content,grpname,image,title,description);
}
});

$(document).on('click','#sec_thank',function(){
var id=$(this).data('clas');
secthank(id);
});


             var $win = $(window);

             $win.scroll(function () {
                 if ($win.height() + $win.scrollTop()
                                == $(document).height()) {
if(wcontent==1){
$('#spinner-loader1').css('display','block');
gscontnum=gscontnum+10;
loadSharedContent();
}else if(wcontent==2){
$('#spinner-loader1').css('display','block');
gacontnum=gacontnum+10;
loadannounceContent();
}
}
             });


});
