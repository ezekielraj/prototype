window.offsetter = 0;
$(window).load(function() {
       $('#as_description').prop('contentEditable',true);
$('#eas_description').prop('contentEditable',true);
});

function loadassignments(off){
var eresponse = $('#all_assignments').html();
var ajaxurl = "/index.php/class/assignments/getassignments";
data={'action': 'getassignments','offset': off,"groupnum": $('#grp_num').html()};
$.post(ajaxurl, data, function(response){
if(response){
if(off==0){
$('#all_assignments').html(response);
}else{
$('#all_assignments').html(eresponse + response);
}
}else{
offsetter = offsetter-10;
}
});
}

function updateassignmarkb(id,gid){
var ajaxurl="/index.php/class/assignments/getassignmarks";
data={'action': 'getassignmarks','aid': id,"groupnum": gid};
$.post(ajaxurl, data, function(response){
if(response){
var obj = $.parseJSON(response);
$.each(obj,function(index,element){
$('#student_assigngrade .'+element.stu_uname).html("0");
$('#student_assigngrade .'+element.stu_uname).html(element.mas_mark);
$('#student_assigngrade .outof_'+element.stu_uname).html(element.mas_outof);
});
}
});
}

$(document).ready(function(){
loadassignments(0);

$(document).on('click','#assign_delete',function(){
var neww = $(this).attr("class");
var ajaxurl = "/index.php/class/assignments/delassignment";
data={'action': 'delassignment','assignid':$(this).attr('class'),"groupnum": $('#grp_num').html()};
$.post(ajaxurl,data, function(response){
if(response){
$( ".assign_" + neww).remove();
}
});
});
$(document).on('click','#assign_edit',function(){
var iddd=$(this).attr("class");
$('#eas_id').html(iddd);
$('#asedit_whole').css('display','block');
$('#asedit_whole > div').css('display','block');
$('#eastitle_name').val($('.assign_'+iddd+' > #assign_title').html());
$('#eas_description').html($('.assign_'+iddd+' > #assign_fulldesc').html());
$('#estdate').val($('.assign_'+iddd+' > #assign_sdate > span').html());
$('#eenddate').val($('.assign_'+iddd+' > #assign_edate > span').html());
});

$(document).on('click','#assign_mark',function(){
var iddd=$(this).attr("class");
var title=$('.title_'+iddd).html();
$('#asmark_whole').css('display','block');
$('#asmark_whole > div').css('display','block');
$('#massign_id > span').html(iddd);
$('#massign_title > span').html(title);
$('div[id=massign_assignmentmark]').each(function(){
$(this).html("0");
});
updateassignmarkb(iddd,$(this).data('gnum'));
});

$(document).on('click', '#singleassign,#assign_edit,#assign_delete,#assign_mark',function(e){
if($(this).attr('id')=="assign_edit" || $(this).attr('id')=="assign_delete" || $(this).attr('id')=="assign_mark"){
return false;
}
var assstyle = $(this).children('#assign_fulldesc').css('display');
if(assstyle == "none"){
$(this).children('#assign_desc').css('display','none');
$(this).children('#assign_fulldesc').css('display','block');
$(this).children('#assign_sdate').css('display','block');
$(this).children('#assign_edate').css('display','block');
$(this).children('#assign_delete').css('display','block');
$(this).children('#assign_attach').css('display','block');
$(this).children('#assign_edit').css('display','block');
$(this).children('#assign_mark').css('display','block');
}
if(assstyle == "block"){
$(this).children('#assign_desc').css('display','block');
$(this).children('#assign_fulldesc').css('display','none');
$(this).children('#assign_sdate').css('display','none');
$(this).children('#assign_edate').css('display','none');
$(this).children('#assign_delete').css('display','none');
$(this).children('#assign_attach').css('display','none');
$(this).children('#assign_edit').css('display','none');
$(this).children('#assign_mark').css('display','none');
}

});


$('#ass_morebutton').click(function(){
offsetter = offsetter+10;
loadassignments(offsetter);
});
$('#ass_prevbutton').click(function(){
if(offsetter!==0){
offsetter = offsetter-10;
loadassignments(offsetter);
}
});

$('#as_description,#eas_description').prop('contentEditable',true);
$('#as_description,#eas_description').focus();
$('.chtext,.echtext').mousedown(function(event){ event.preventDefault() });
$('#d_bold').click(function(){document.execCommand('bold', false, null);$('#as_description').focus();return false;});
$('#d_italics').click(function(){document.execCommand('italic', false, null);$('#as_description').focus();return false;});
$('#ed_bold').click(function(){document.execCommand('bold', false, null);$('#eas_description').focus();return false;});
$('#ed_italics').click(function(){document.execCommand('italic', false, null);$('#eas_description').focus();return false;});

$(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        showButtonPanel: false,
onSelect: function(selected,evnt){
if($('#stdate').val()!="" && $('#enddate').val()!=""){
var datepickerBegin = $("#stdate").val(); 
var datepickerEnd = $("#enddate").val(); 
if($.datepicker.parseDate('yy-mm-dd', datepickerBegin)>$.datepicker.parseDate('yy-mm-dd', datepickerEnd)){
$("#enddate").val("");
$('.dterr').html("Error, End Date occurs before Start Date");
}else{
$('.dterr').html("");
}
}
}
        });

$(".edatepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        showButtonPanel: false,
onSelect: function(selected,evnt){
if($('#estdate').val()!="" && $('#eenddate').val()!=""){
var datepickerBegin = $("#estdate").val(); 
var datepickerEnd = $("#eenddate").val(); 
if($.datepicker.parseDate('yy-mm-dd', datepickerBegin)>$.datepicker.parseDate('yy-mm-dd', datepickerEnd)){
$("#eenddate").val("");
$('.edterr').html("Error, End Date occurs before Start Date");
}else{
$('.edterr').html("");
}
}
}
        });


$('#createassign').click(function(){
if($('#as_create').css('display')=="none"){
$('#as_create').slideDown( "fast" );
}else{
$('#as_create').css('display','none');
}
});

$('#as_attachment img').click(function(){
$('input[type=file]').trigger('click');
});
$('input[type=file]').change(function() {
$('.uploadfile').html($(this).val());
});

$('#eas_attachment img').click(function(){
$('#effile').trigger('click');
});
$('#effile').change(function() {
$('.euploadfile').html($(this).val());
});

$('#eashead > span').click(function(){
$('#asedit_whole > div').css('display','none');
$('#asedit_whole').css('display','none');
});

$('#mashead > span').click(function(){
$('#asmark_whole > div').css('display','none');
$('#asmark_whole').css('display','none');
});


$('#grpselected').focus(function() {
$(".selgrpy").toggle( "drop",{direction: "up"}, 200 );
});
$('#grpselected').focusout(function() {
//$(".selgrpy").css('display','none');
});

$('#grpselected').keyup(function() {
var keay = $('#grpselected').val();
$('.selgrpy').each(function() {
if($(this).text().indexOf(keay)==-1){
$(this).css('display','none');
}else{
$(this).css('display','block');
}
});
});

$('.selgrpy').click(function() {
$('#grpselected').val($(this).text());
$(".selgrpy").css('display','none');
});

$('#eas_submit').click(function() {
var asid = $('#eas_id').html();
var astitle=$('#eastitle_name').val();
var startdate=$('#estdate').val();
var enddate = $('#eenddate').val();
var description=$('#eas_description').text();
jQuery.ajax({
                url: "/index.php/class/assignments/updateassignment",
                type: "POST",
                contentType: false,
                processData: false,
                data: function() {
                    var data = new FormData();
		    data.append("action","updateassignment");
		    data.append("asid",asid);
		    data.append("astitle",astitle);
		    data.append("startdate",startdate);
		    data.append("enddate",enddate);
		    data.append("description",description);
		    data.append("groupnum",$('#grp_num').html());
                    data.append("attachment", jQuery("#effile").get(0).files[0]);
		    return data;		
		}(),
                error: function(response) {

                    alert(response.toSource());
                },
                success: function(response, textStatus) {
		    if(response){
                    alert(response);
$('#asedit_whole > div').css('display','none');
$('#asedit_whole').css('display','none');
$('.assign_'+asid+' > #assign_title').html(astitle);
$('.assign_'+asid+' > #assign_fulldesc').html(description);
$('.assign_'+asid+' > #assign_sdate > span').html(startdate);
$('.assign_'+asid+' > #assign_edate > span').html(enddate);

loadassignments(offsetter);
		    }
                    console.log(response, textStatus);
                }


});

});

$('#as_submit').click(function() {
var astitle=$('#astitle_name').val();

var startdate=$('#stdate').val();
var enddate = $('#enddate').val();
var description=$('#as_description').text();
jQuery.ajax({
                url: "/index.php/class/assignments/createassignment",
                type: "POST",
                contentType: false,
                processData: false,
                data: function() {
                    var data = new FormData();
		    data.append("action","createassignment");
		    data.append("astitle",astitle);
		    data.append("startdate",startdate);
		    data.append("enddate",enddate);
		    data.append("description",description);
		    data.append("groupnum",$('#grp_num').html());
                    data.append("attachment", jQuery("#ffile").get(0).files[0]);
		    return data;		
		}(),
                error: function(response) {
                    alert(response.toSource());
                },
                success: function(response, textStatus) {
		    if(response){
                    alert(response);
loadassignments(0);
		    }
                    console.log(response, textStatus);
                }


});

/*var client = new XMLHttpRequest();
      var file = $('input[type=file]').val();
      var formData = new FormData();
      formData.append("upload", file.files[0]);
      client.open("post", "/upload", true);
      client.setRequestHeader("Content-Type", "multipart/form-data");
      client.send(formData); 

   client.onreadystatechange = function() 
   {
      if (client.readyState == 4 && client.status == 200) 
      {
         alert(client.statusText);
      }
   }
*/
});

$(document).on('click','#massign_save',function(){
var students = [];
$('div[id=massign_assignmentmark]').each(function(){
students.push($(this).attr('class')+","+$(this).html()+"_"+$('.outof_'+$(this).attr('class')).html());
});
var ajaxurl="/index.php/class/assignments/assignmark";
data={'action':'assignmark','students':students,'groupnum':$('#grp_num').html(),'assignid':$('#massign_id > span').html()};
$.post(ajaxurl, data, function(response){
if(response){
alert(response);
}
});
});

$(document).on('keypress keydown keyup', '#massign_stupoints > span', function() {
$('[id^=massign_outofmark]').html($(this).html());
});

});
