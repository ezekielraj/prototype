function loadDays(count){
var ajaxurl = '/index.php/class/attendance/loaddays';
data={'action': 'LoadDays', 'count':count};
$.post(ajaxurl, data, function(response){
if(response){
$('#at_thisweekdates').html(response);
}
});
}
function loadweek(count){
var ajaxurl = '/index.php/class/attendance/loadweek';
data={'action': 'LoadWeek', 'count':count};
$.post(ajaxurl, data, function(response){
if(response){
$('#at_headtext > span').html(response);
}
});
}
$(document).ready(function() {
window.count=0;
//loadDays(0);
loadweek(0);
//loadtodayAtten();
$('#at_left').click(function(){ 
count = (count-7);
loadDays(count);
loadweek(count);
});
$('#at_right').click(function() {
if(count !== 0){
count = (count+7);
loadDays(count);
loadweek(count);
}
});
$('#at_weekcount').click(function(){
count = 0;
loadDays(count);
loadweek(count);
});
$(document).on('click','#markallpresent',function() {
$('div[id^="atten_"]').html("Present");
$('div[id^="atten_"]').attr("data-atvalue","1");
});
$('#markallabsent').click(function() {
$('.grp_attstatusclass').html("Absent");
$('.grp_attstatusclass').attr("data-atvalue","0");
});
$(document).on('click','#marksinglepresent',function(){
var stuname=$(this).attr('class');
$('#atten_'+stuname).html("Present");
$('#atten_'+stuname).attr("data-atvalue","1");
});

$(document).on('click','#marksingleabsent',function(){
var stuname=$(this).attr('class');
$('#atten_'+stuname).html("Absent");
$('#atten_'+stuname).attr("data-atvalue","0");
});

$(document).on('click','#attendance_save',function(){
var stuattendance = [];
$('.grp_attstatusclass').each(function(){
var idd = $(this).attr('id');
stuattendance.push(idd+","+$('#'+idd).attr('data-atvalue'));
//stuattendance[ idd ]=$('#'+idd).attr('data-atvalue');
//alert(stuattendance[ idd ]);
});
//alert(stuattendance);
//alert($('#atten-noon').html());
var ajaxurl = '/index.php/class/attendance/updateattendance';
data={'action': 'updateattendance', 'stuatt':stuattendance,'attendate':$('#atten-dater').html(),'attennoon':$('#atten-noon').html()};
$.post(ajaxurl, data, function(response){
alert(response);
});


});

$(document).on('click','#at_atten > *',function(){
var timmer = $(this).attr('class');
var tarr = timmer.split('_');
$('#atten-dater').html(tarr[1]);
$('#atten-noon').html(tarr[0]);
var status="";
var ajaxurl='/index.php/class/attendance/getattendancevalue';
data={'action': 'getattendancevalue', 'groupnum':$('#grp_num').html(),'attendate':$('#atten-dater').html(),'attennoon':$('#atten-noon').html()};
$.post(ajaxurl, data, function(response){
var obj = $.parseJSON(response);
$.each(obj,function(key, value){
var allv = value.toString();
var newv = allv.split(',');

if(newv[1]=="1"){
status="Present";
}else{
if(newv[1]=="0"){
status="Absent";
}else{
if(newv[1]=="2"){
status="Not Marked";
}
}
}

$('#atten_'+newv[0]).html(status);
$('#atten_'+newv[0]).attr('data-atvalue',newv[1]);
});

});


});

});
