	function awardUpdate(user,award){
var ajaxurl='/index.php/class/group/awardupdate';
var subjectname = $('#sub_name').html();
data= {'action':"awardUpdate",'user': user,'award': award,'subjectname': subjectname,'group_num':$('#grp_num').html()};
$.post(ajaxurl, data, function(response) {
if(response=="s"){
$(document).find('#sel_pos').remove();
$(document).find('#sel_neg').remove();
if($('#mulselector').css('display')=="none"){
var value = (parseInt($('.'+user).find('#gp_grade').html())+1);
$('.'+user).find('#gp_grade').html(value);
}else{
location.reload(true);
}
}else{
$(document).find('#sel_pos').remove();
$(document).find('#sel_neg').remove();
if(response){
alert(response+"cannot be updated");
}
}
});
}


$(document).ready(function(){
window.topmenu = "";
window.awardvaluepos = $('#awards-value-pos').html();
window.awardvalueneg = $('#awards-value-neg').html();
window.student = "";
window.availTuesday = [];
$(document).on('click','#grpaward',function() {
if(topmenu === ""){
topmenu=$('#grp_selection').html();
}
$('#grp_selection > div').remove();
$('#grp_selection').html("<div id='grp_choose' class='col-lg-7'><p>select students below or <span class='sa'>select all</span><span class='da' style='display:none;'>unselect all</span></p></div><div class='btn-group pull-right'><button type='button' class='btn btn-success pull-right dropdown-toggle' data-toggle='dropdown' id='aw-pos'> Add Awards <span class='caret'</span></button><ul class='dropdown-menu' role='menu'><li><a href='#' id='pos_ontask' class='pos_awdmulstu'>On task</a></li><li><a href='#' id='pos_participating' class='pos_awdmulstu'>Participating</a></li><li><a href='#' id='pos_workinghard' class='pos_awdmulstu'>Working hard</a></li></ul></div><button type='button' class='btn btn-default pull-right' id='mulcancel'>Cancel</button>");
$('.gp_stuname > div').css('display','block');
});

$(document).on('click','#mulcancel',function() {
$('#grp_selection > *').remove();
$('#grp_selection').html("");
$('#grp_selection').html(topmenu);
$('.gp_stuname > div').not('#gp_photo').css('display','none');
$('input[type=checkbox]').prop('checked',false);
});

$(document).on('click','.sa',function(){
$('input[type=checkbox]').prop('checked',true);
$(this).css('display','none');
$('.da').css('display','inline-block');
});

$(document).on('click','.da',function(){
$('input[type=checkbox]').prop('checked',false);
$('.sa').css('display','inline-block');
$('.da').css('display','none');
});

$('.gp_pos').click(function(){
student = $(this).attr('id');
if($(this).html()=='+'){
$(document).find('#sel_pos').remove();
$(document).find('#sel_neg').remove();
$(this).append(awardvaluepos);
}else{
$(document).find('#sel_pos').remove();
}
});

$('.gp_neg').click(function(){
student = $(this).attr('id');
if($(this).html()=='-'){
$(document).find('#sel_pos').remove();
$(document).find('#sel_neg').remove();
$(this).append(awardvalueneg);
}else{
$(document).find('#sel_pos').remove();
$(document).find('#sel_neg').remove();
}
});

$(document).on('click','.pos_awdmulstu,#sel_pos > div',function() {
var id = $(this).attr('id');
if($('#mulselector').css('display')=="none"){
awardUpdate(student,id);
}else{
$('input[type=checkbox][name="student"]:checked').each(function() {
       availTuesday.push($(this).val()); 
    });
awardUpdate(availTuesday,id);
}
});

$(document).on('click','#sel_neg > div',function() {
id = $(this).attr('id');
if($('#mulselector').css('display')=="none"){
awardUpdate(student,id);
}else{
$('input[type=checkbox][name="student"]:checked').each(function() {
       availTuesday.push($(this).val()); 
    });
awardUpdate(availTuesday,id);
}
});
});
