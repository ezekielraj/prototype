var freeDays = [];
fetchFreeDays();

function getEvents(dater){
var ajaxurl = '/index.php/class/calender/getevents';
data={'action':'getEvents','dater':dater,'groupnum':$('#grp_num').html()};
$.post(ajaxurl, data, function(response){
$('#calevents').html(response);
});
}
function fetchFreeDays(year, month)
{
var curl = window.location.pathname; 
var curl1=curl.split('/');
    var start_date = '';
    if (year != undefined && month != undefined) {
      start_date = year +'-';
      start_date += month +'-';
      start_date += '01';
    }
$.ajax({
              url: "/index.php/class/calender/getdates",
                type: "POST",
                contentType: false,
                processData: false,
                data: function() {
                    var data = new FormData();
                    data.append("action","getDate");
                    data.append("dater",start_date);
                    data.append("groupnum",curl1[4]);
                                             return data;
                }(),
		async: false,
                success: function(response, textStatus) {
freeDays=[];
		freeDays = response.split(',');
//alert(response);
               },
		error: function(response) {
console.log(response);

}
}); 
}

function highlightDays(date)
{
    for (var i = 0; i < freeDays.length; i++) {
      if (new Date(freeDays[i]).toString() == date.toString()) {
         return [true, 'highlight', 'no to-do items due']; // [0] = true | false if this day is selectable, [1] = class to add, [2] = tooltip to display
      }
    }
 
    return [true, ''];
}
$(window).load(function(){
window.dater1="";
$('#side-calender').datepicker({
      dateFormat: 'yy-m-d',
      inline: true,
      onSelect: function(dateText, inst) { 
if($(this).datepicker('getDate').toString() === dater1.toString()){

$('#addnewevent').css('display','block');
        var now = new Date($('#seldate').html());
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = (month)+"/"+(day)+"/"+now.getFullYear();
        $('#fromdate').val(today);
        $('#todate').val(today);

        var hours = now.getHours() % 12;
        if (hours == 0) {
            hours += 12;
        }
        if(now.getHours()<12){
        mer="am";
        }else{
        mer="pm";
        }
        $('#fromhours').val(hours);
        $('#frommin').val(now.getMinutes());
        $('#fromampm').val(mer);
        $('#tohours').val(hours);
        $('#tomin').val(now.getMinutes());
        $('#toampm').val(mer);

}else{
dater1=$(this).datepicker('getDate');
var dater=$(this).datepicker('getDate');
getEvents(dater);
$('#seldate').html(dater);
}
     }, 
        beforeShowDay: highlightDays,
        onChangeMonthYear: fetchFreeDays

    });

var now = new Date();
if(now.getHours()<15){
getEvents(now);
$('#seldate').html(now);
}else{
now.setDate(now.getDate() + 1);
getEvents(now);
$('#seldate').html(now);
}

$('#fromdate').datepicker();
	$('#todate').datepicker();
	});
	$(document).ready(function(){

	$(document).on('click','#aehead > span',function(){
	$('#addnewevent').css('display','none');
	});

	$(document).keypress(function(e) {
	  if (e.keyCode == 27) { 
	$('#addnewevent').css('display','none');
	}   
	});
	$('#addevent').click(function(){
	$('#addnewevent').css('display','block');
	if($('#seldate').html()==""){
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = (month)+"/"+(day)+"/"+now.getFullYear();
	$('#fromdate').val(today);
	$('#todate').val(today);
	var hours = now.getHours() % 12;
	if (hours == 0) {
	    hours += 12;
	}
	if(now.getHours()<12){
	mer="am";
	}else{
	mer="pm";
	}
	$('#fromhours').val(hours);
	$('#frommin').val(now.getMinutes());
	$('#fromampm').val(mer);
	$('#tohours').val(hours);
	$('#tomin').val(now.getMinutes());
	$('#toampm').val(mer);

	}else{
	var now = new Date($('#seldate').html());
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = (month)+"/"+(day)+"/"+now.getFullYear();
	$('#fromdate').val(today);
	$('#todate').val(today);

	var hours = now.getHours() % 12;
	if (hours == 0) {
	    hours += 12;
	}
	if(now.getHours()<12){
	mer="am";
	}else{
	mer="pm";
	}
	$('#fromhours').val(hours);
	$('#frommin').val(now.getMinutes());
	$('#fromampm').val(mer);
	$('#tohours').val(hours);
	$('#tomin').val(now.getMinutes());
	$('#toampm').val(mer);
	}
	});

	$('#evesubmit').click(function(){
	if($('#astitle').val()!="" && $('#evedescp').val()!=""){
	var ajaxurl = '/index.php/class/calender/createevents';
	data={'action':'createEvents','title': $('#astitle').val(),'description':$('#evedescp').val(),'fromdate':$('#fromdate').val(),'todate':$('#todate').val(),'fromhours':$('#fromhours').val(),'frommin':$('#frommin').val(),'fromampm':$('#fromampm').val(),'tohours':$('#tohours').val(),'tomin':$('#tomin').val(),'toampm':$('#toampm').val(),'repeat':$('#everepeat').val(),'grpnum':$('#grp_num').html(),'wtshare':$("input[name=wtshare]:checked").val(), 'eventid':$(this).attr('class')};
	$.post(ajaxurl, data, function(response){
	if(response=="s"){
	alert("Created");
	$('#addnewevent').css('display','none');
	getEvents($('#seldate').html());
	}else{
	alert(response);
	}
	});

	}
	});

	$(document).on('click','#sevent',function(){

	var serial = $(this).attr('class');
	var serials = serial.split("_");
	$('#astitle').val($('#etitle_'+serials[1]).html());
	$('#evedescp').val($('#edesc_'+serials[1]).html());
	var rep = $('#erepeat_'+serials[1]).html();
	$('#everepeat').val(rep);
	var t = $('#efromdate_'+serials[1]).html().split(/[- :]/);
	var fdate = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
	var fday = ("0" + fdate.getDate()).slice(-2);
	var fmonth = ("0" + (fdate.getMonth() + 1)).slice(-2);
	var from = (fmonth)+"/"+(fday)+"/"+fdate.getFullYear();
	$('#fromdate').val(from);
	var fhours = fdate.getHours() % 12;
	if (fhours == 0) {
	    fhours += 12;
	}
	if(fdate.getHours()<12){
	fmer="am";
	}else{
	fmer="pm";
	}
	$('#fromhours').val(fhours);
	$('#frommin').val(fdate.getMinutes());
	$('#fromampm').val(fmer);


	var n = $('#etodate_'+serials[1]).html().split(/[- :]/);
	var tdate = new Date(n[0], n[1]-1, n[2], n[3], n[4], n[5]);
	var tday = ("0" + tdate.getDate()).slice(-2);
	var tmonth = ("0" + (tdate.getMonth() + 1)).slice(-2);
	var to = (tmonth)+"/"+(tday)+"/"+tdate.getFullYear();
	$('#todate').val(to);
	var thours = tdate.getHours() % 12;
	if (thours == 0) {
	    thours += 12;
	}
	if(tdate.getHours()<12){
	tmer="am";
	}else{
	tmer="pm";
	}
	$('#tohours').val(thours);
	$('#tomin').val(tdate.getMinutes());
	$('#toampm').val(tmer);
	var wtshare=$('#ewtshare_'+serials[1]).html();
	$('input:radio[name="wtshare"]').filter('[value="'+wtshare+'"]').attr('checked', true);

	$('#evesubmit').attr('class',serials[1]);
$('#addnewevent').css('display','block');

});

});
