
function groupUpdate(){
var ajaxurl = '/index.php/groupupdate';
if($('.classy-value').text()=='Select Class'){
var classname = "All";
}else{
var classname = $('.classy-value').text();
}
if($('.sectiony-value').text() == 'Select Section' || $('.sectiony-value').text() == 'All'){
var sectionname = "Students";
}else{
var sectionname = $('.sectiony-value').text();
}
var groupname = classname+' '+sectionname;
data={'action': 'updateGroup','sStudents': selectedStudents,'groupName': groupname,'classu':$('.classy-value').data('classy'),'sectionu':$('.sectiony-value').data('sectiony'),'subjectu': $('.subjecty-value').data('subjecty')};
$.post(ajaxurl, data, function(response){
if(response=='s'){
$('#addstudentcontent').remove();
loadTMC();
}else{
alert(response);
}
});
}

function dispform(){
$('#asForm div').fadeIn();
//$('#select-classy div').css('display','none');
//$('#select-sectiony div').css('display','none');
//$('#select-subjecty div').css('display','none');
}

function fixAll(){
var classy = $('.classy-value').attr('data-classy');
var sectiony = $('.sectiony-value').attr('data-sectiony');
var subjecty = $('.subjecty-value').attr('data-subjecty');
var ajaxurl='/index.php/fixall';
data={'action':'getStudents','classy':classy,'sectiony':sectiony,'subjecty':subjecty};
$.post(ajaxurl, data, function(response){
var obj = $.parseJSON(response);
visible = obj.students;
if(obj){
$('#studentslist').html("<div id='all'>All</div>");
for(var i=0;i<obj.students.length;i++){
var myString=obj.students[i].split(',');
if($.inArray(myString[0],selectedStudents) > -1){
$('#studentslist').append("<div id='"+myString[0]+"' class='selected'>"+myString[1]+"</div>");
}else{
$('#studentslist').append("<div id='"+myString[0]+"'>"+myString[1]+"</div>");
}
}
}
});
}
function sectionfix(){
var mclass=$('.classy-value').attr('data-classy');
var ajaxurl='/index.php/fixsection';
data={'action':"fixSection",'mclass':mclass};
$.post(ajaxurl, data, function(response){
if(response){
$('#select-sectiony').html(response);
$('.subjecty-value').attr('data-subjecty',"");
$('.subjecty-value').html("Select Subject");
}
});
}

function subjectfix(){
//var mclass=$('.classy-value').attr('data-classy');
//var msection=$('.sectiony-value').attr('data-sectiony');

if($('.classy-value').text()=='Select Class'){
var classname = "All";
}else{
var classname = $('.classy-value').text();
}
if($('.sectiony-value').text() == 'Select Section' || $('.sectiony-value').text() == 'All'){
var sectionname = "Students";
}else{
var sectionname = $('.sectiony-value').text();
}
var groupname = classname+' '+sectionname;


var ajaxurl='/index.php/fixsubject';
//data={'action':"fixSubject"};//,'mclass':mclass,'msection':msection};
data={'action':"fixSubject",'groupname':groupname};
$.post(ajaxurl,data, function(response){
if(response){
$('#select-subjecty').html(response);
//alert(response);
}
});
}

$(document).ready(function() {
window.visible = [];
window.selectedStudents = [];
fixAll();
$(document).keypress(function(e) {
  if (e.keyCode == 27) { 
$('#addstudentcontent').remove();
}   
});
$(document).on('click','#ashead span',function(){
$('#addstudentcontent').remove();
});

$(document).on('click','#selectbox',function() {
var jj=$(this).attr('class');
 $("#select-"+jj+" div").toggle( "drop",{direction: "up"}, 200 );
});
$('#select-classy div').click(function() {
$('.classy-value').html($(this).html());
//$("#select-classy div").toggle( "drop",{direction: "up"}, 200 );
$('.classy-value').attr('data-classy',$(this).attr('id'));
sectionfix();
fixAll();
});

$(document).on('click','#select-sectiony div',function() {
$('.sectiony-value').html($(this).html());
//$("#select-sectiony div").toggle( "drop",{direction: "up"}, 200 );
$('.sectiony-value').attr('data-sectiony',$(this).attr('id'));
subjectfix();
fixAll();
});

$(document).on('click','#select-subjecty div',function() {
$('.subjecty-value').html($(this).html());
//$("#select-subjecty div").toggle( "drop",{direction: "up"}, 200 );
$('.subjecty-value').attr('data-subjecty',$(this).attr('id'));
//fixAll();
});

$(document).on('click','#studentslist > div',function(){
if($(this).attr('id')=="all"){
if($(this).attr('class')=="selected"){
$('#studentslist > div').removeClass( "selected" );
}else{
for(var i=0;i<visible.length;i++){
var myString = visible[i].split(',');
if($.inArray(myString[0],selectedStudents) == -1){
selectedStudents.push(myString[0]);
}
}
$('#studentslist > div').addClass( "selected" );
}
}else{
var ind = $.inArray($(this).attr('id'),selectedStudents);
if(ind > -1){
selectedStudents.splice(ind,1);
$('#studentslist > #all').removeClass('selected');
$(this).removeClass( "selected" );
}else{
selectedStudents.push($(this).attr('id'));
$(this).addClass( "selected" );
}
}
});

$('#goimage').click(function() {
if($('.subjecty-value').text() == "Select Subject"){
$('#smessage').html('Please Select the Subject!');
}else{
groupUpdate();
}
});

dispform();
});
