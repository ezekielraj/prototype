var onelinetform = "</div>Number of teachers code to generate: <input type='text' name='teacher-name'></div>";
function sectionchanger(std){
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection','code':std};
$.post(ajaxurl,data, function(response){
if(response){
$('#sections').html(response);
}
});
}

function sectionchanger1(std){
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection1','code':std};
$.post(ajaxurl,data, function(response){
if(response){
$('.full-section-list').html(response);
$('#as-section-head').html($('#as-'+std).html());
$('#as-selected-class').html(std);
}
});
}

function sectionchanger2(std){
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection1','code':std};
$.post(ajaxurl,data, function(response){
if(response){
$('.ds-full-section-list').html(response);
$('#as-section-head').html($('#as-'+std).html());
$('#as-selected-class').html(std);
}
});
}

function newgeneratecode(){
var std=$('#standards').val();
var sec=$('#sections').val();
var ajaxurl = '/index.php/admin/gettkey';
data={'action':'getkey','std':std,'sec':sec};
$.post(ajaxurl,data, function(response){
if(response){
$('#code-generator-content').html(response);
}
});

}

$(document).ready(function(){

$('#as-add-section-done').click(function(){
$('#cgwhole').css('display','none');
//$('#standards').val("");
//$('#sections').html("<option value=''>Select Section</option>");
sectionchanger($('#standards').val());
});

$('#ds-del-section-done').click(function(){
$('#dswhole').css('display','none');
//$('#standards').val("");
//$('#sections').html("<option value=''>Select Section</option>");
sectionchanger($('#standards').val());
});

$(document).keypress(function(e) {
  if (e.keyCode == 27) { 
$('#cgwhole').css('display','none');
//$('#standards').val("");
//$('#sections').html("<option value=''>Select Section</option>");
sectionchanger($('#standards').val());
}   
});

$(document).on('click','#cghead > span',function(){
$('#cgwhole').css('display','none');
//$('#standards').val("");
//$('#sections').html("<option value=''>Select Section</option>");
sectionchanger($('#standards').val());
});

$(document).on('click','#dshead > span',function(){
$('#dswhole').css('display','none');
//$('#standards').val("");
//$('#sections').html("<option value=''>Select Section</option>");
sectionchanger($('#standards').val());
});


$('#as-add-section').click(function(){
var std = $('#as-selected-class').html();
var ajaxurl = '/index.php/admin/createsection';
data={'action':'createsection','code':std};
$.post(ajaxurl,data, function(response){
if(response=="s"){
sectionchanger1(std);
}
});
});

$('#ds-del-section').click(function() {
var std = $('#as-selected-class').html();
var ajaxurl = '/index.php/admin/deletesection';
data={'action':'deletesection','code':std};
$.post(ajaxurl,data, function(response){
if(response=="s"){
sectionchanger2(std);
}
});
});

$('#add-section').click(function(){
if($('#standards').val().length > 0 ){
$('#cgwhole').css('display','block');
sectionchanger1($('#standards').val());
}else{
$('.tip').css('display','block');
}


/*var std=$('#standards').val();
var ajaxurl = '/index.php/admin/createsection';
data={'action':'createsection','code':std};
$.post(ajaxurl,data, function(response){
if(response=="s"){
//alert(response);
sectionchanger(std);
}
});*/
});

$('#del-section').click(function(){
if($('#standards').val().length > 0 ){
$('#dswhole').css('display','block');
sectionchanger2($('#standards').val());
}else{
$('.tip').css('display','block');
}
});

$('#code-generate').click(function(){
var std=$('#standards').val();
var sec=$('#sections').val();
var ajaxurl = '/index.php/admin/gettkey';
data={'action':'getkey','std':std,'sec':sec};
$.post(ajaxurl,data, function(response){
if(response){
$('#code-generator-content').html(response);
}
});

});

$('#code-clear').click(function(){
var std=$('#standards').val();
var sec=$('#sections').val();
var ajaxurl='/index.php/admin/clearkey'
data={'action':'clearkey','std':std,'sec':sec};
$.post(ajaxurl,data, function(response){
if(response=="s"){
//alert("Cleared Successfully!!");
$('#code-generate').css('display','block');
$('#code-generator-content').html("Cleared Successfully!!");
$('#code-generate').css('display','block');
}else{
alert(response);
}
});
});

$('#sections').change(function(){
var std=$('#standards').val();
var sec=$('#sections').val();
var ajaxurl='/index.php/admin/checkkey';
data={'action':'checkkey','std':std,'sec':sec};
$.post(ajaxurl,data,function(response){
if(response=="s"){
$('#code-generate').css('display','block');
$('#code-generator-content').html("");
}else{
$('#code-generate').css('display','none');
newgeneratecode();
}
});
});

/*$('.as_classes').click(function(){
var std = $(this).attr('id');
var stdd=std.split('-');
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection1','code':stdd[1]};
$.post(ajaxurl,data, function(response){
if(response){
$('.full-section-list').html(response);
$('#as-section-head').html($('#'+std).html());
$('#as-selected-class').html(stdd[1]);
}
});
});
*/

$('#standards').change(function(){
$('.tip').css('display','none');
var std = $(this).val();
if(std==""){
$('#sections').html("<option value=''>Select Section</option>");
}else{
sectionchanger(std);
}
//var ajaxurl = '/index.php/admin/gensection';
//data={'action':'gensection','code':std};
//$.post(ajaxurl,data, function(response){
//if(response){
//$('#sections').html(response);
//}
//});
});


/*$('#addct').click(function(){
var value=$("input[type='radio']:checked").val();
if(value=='teacher'){
$('#addct-form').html(onelinetform);
}
if(value=='class'){
$('#addct-form').append(onelineform);
}
});
$('.gencode').click(function(){
var id=$(this).attr('id');
var ajaxurl = '/index.php/admin/gencode';
data={'action':'gencode','code':id};
$.post(ajaxurl,data,function(response){
if(response){
$('#code_'+id).html(response);
}
});
});*/
});
