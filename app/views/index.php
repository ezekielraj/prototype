<?php echo HTML::style('css/login.css'); ?>
<?php echo HTML::script('js/login.js'); ?>
<?php
if($result=="student"){
$result1="Students Login";
}elseif($result=="parent"){
$result1="Parents Login";
}elseif($result=="teacher"){
$result1="Teachers Login";
}
?>
<style>
.colxs-centered {
float: none;
margin: 0 auto;
}
#head {
text-align: center;
font-size:22px;
font-weight:bold;
color:#B4BCC2;
}

#head label {
margin-top:auto;
margin-bottom: auto;
height:100%;
}

</style>
<div class="col-lg-12 colxs-centered">
<div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label><?php echo $result1; ?> | <span id='new-registe'>Register</span></label></div>
<br>
<div class='whoselogin' style='display:none;'><?php echo $result; ?></div>
<div id='loginform'>
<div class="colxs-centered col-lg-3">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='user' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<div class="form-group">
<label for="pass">Password</label>
<input id='pass' type='password' class="form-control" placeholder='Password'/>
</div>
<center><button type="button" class="btn btn-default" id='login'>Login</button></center>
<div id='Message'></div>
<div id='new-register' style='float:right;'>Register</div>
</div>
</div>
<div id='register-whole' style='display:none;'>

<div id='register-Form' class="col-lg-4 colxs-centered">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='reg-key' name='username' value='' required>
<br>
<button class="btn btn-default" type="button" id="reg-submitFile" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>

</div>
</div>
