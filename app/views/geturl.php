<script>
$(document).ready(function(){
$('#ajax-img-close').click(function(){
$('.url_load_img').remove();
});
$('#ajax-title-close').click(function(){
$('#url_load_title').remove();
$('.url_load_title').remove();
});
$('#ajax-description-close').click(function(){
$('#url_load_description').remove();
$('.url_load_description').remove();
});
});
</script>
<?php
function make_absolute($urll, $basee)
{
    // Return base if no url
    if( ! $urll) return $basee;
    // Return if already absolute URL
    if(parse_url($urll, PHP_URL_SCHEME) != '') return $urll;
	if(substr( $urll, 0, 2 ) === "//") return $urll;
    // Urls only containing query or anchor
    if($urll[0] == '#' || $urll[0] == '?') return $basee.$urll;
    // Parse base URL and convert to local variables: $scheme, $host, $path
    extract(parse_url($basee));
    // If no path, use /
    if( ! isset($path)) $path = '/';
    // Remove non-directory element from path
    $path = preg_replace('#/[^/]*$#', '', $path);
    // Destroy path if relative url points to root
    if($urll[0] == '/') $path = '';
    // Dirty absolute URL
    $abs = "$host$path/$urll";
    // Replace '//' or '/./' or '/foo/../' with '/'
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {}
    // Absolute URL is ready!
    return $scheme.'://'.$abs;
}
$contents="";
$contents=file_get_contents($url);
$row = get_meta_tags($url);
//image loading part
$image ="";
if(array_key_exists('twitter:image',$row)){
$image=$row['twitter:image'];
}elseif(array_key_exists('og:image',$row)){
$image=$row['og:image'];
}else{
$image=array();
preg_match_all('/<img[^>]+>/i',$contents, $result); 
$img = array();
$i=0;
foreach( $result[0] as $img_tag)
{
    preg_match_all('@src="([^"]+)"@',$img_tag, $img[$i]);
$src = array_pop($img[$i]);
if(isset($src[0]) && $src[0]!=""){
$image[$i]=make_absolute($src[0],$url);
$i=$i+1;
}
}
}
//title loading part
$title="";
if(array_key_exists('twitter:title',$row)){
$title=$row['twitter:title'];
}elseif(array_key_exists('og:title',$row)){
$title=$row['og:title'];
}else{
preg_match("/\<title\>(.*)\<\/title\>/i",$contents,$title);
$title = $title[1];
}
//description loading part
$description="";
if(array_key_exists('twitter:description',$row)){
$description=$row['twitter:description'];
}elseif(array_key_exists('og:description',$row)){
$description=$row['og:description'];
}else{
if(isset($row['description']) && $row['description']!=""){
$description=$row['description'];
}else{
preg_match("/\<p\>(.*)\<\/p\>/i",$contents,$descrip);
$description=$descrip[0];
}
}


echo "<div id='url_load_full' style='overflow:hidden;'>";
if((is_array($image) && count($image)!=0) || $image!=""){
echo "<div class='url_load_img col-md-4 pull-left'>";
echo "<button class='pull-right btn btn-default btn-xs' id='ajax-img-close'>x</button>";

if(is_array($image)){
echo "<script>";
echo "var limit=".count($image).";\n";
?>
$(document).ready(function(){
var disp=1;
$('#uli-go-right').click(function(){
if(disp==limit){
disp=1;
}else{
disp=parseInt(disp)+1;
}
for(i=1;i<=limit;i++){
$(".uli_"+i).css('display','none');
}
$('.uli_'+disp).css('display','block');
});

$('#uli-go-left').click(function(){
if(disp==1){
disp=limit;
}else{
disp=parseInt(disp)-1;
}
for(i=1;i<=limit;i++){
$(".uli_"+i).css('display','none');
}
$('.uli_'+disp).css('display','block');


});
});
<?php
echo "</script>";
for($i=0;$i<count($image);$i++){//each($image as $img){
if($i==0){
echo "<img id='url_load_img' class='uli_".($i+1)."' src='".$image[$i]."' width='158' height='158' style='float:left;display: block;'>";
}else{
echo "<img id='url_load_img' class='uli_".($i+1)."' src='".$image[$i]."' width='158' height='158' style='float:left;display:none;'>";
}
}
echo "<div class=\"btn-group btn-group-xs pull-right\" role=\"group\" aria-label=\"...\">
  <button id='uli-go-left' type=\"button\" class=\"btn btn-default\"><</button>
  <button id='uli-go-right' type=\"button\" class=\"btn btn-default\">></button></div>";
}else{
//$size=getimagesize($image);
//if($size['bits']!=0){
echo "<img id='url_load_img' src='".$image."' width='158' height='158' style='float:left;display: block;'>";
//}
}
echo "</div>";
}
?>
<script>
$(document).ready(function(){
$('#url_load_description').click(function(){
$('.url_load_description').css('display','block');
$('#url_load_description').css('display','none');
});
$('#url_load_title').click(function(){
$('.url_load_title').css('display','block');
$('#url_load_title').css('display','none');
});
});
</script>
<?php
echo "<div id='url_load_title' class='col-md-8 pull-left'><h3>";
echo "<button class='pull-right btn btn-default btn-xs' id='ajax-title-close'>x</button>";
if($title!=""){
echo $title;
}else{
echo "<span style='color:#A4A4A4;'>TITLE</span>";
}
echo "</h3></div>";
echo "<textarea class='url_load_title' rows='1' cols='45' style='display:none;'>";
if($title!=""){
echo $title;
}else{
echo "TITLE";
}
echo "</textarea>";
echo "<div id='url_load_description' class='col-md-8 pull-left'><p>";
echo "<button class='pull-right btn btn-default btn-xs' id='ajax-description-close'>x</button>";
if(trim($description)!=""){
echo strip_tags($description);
}else{
echo "<span style='color:#A4A4A4;'>DESCRIPTION</span>";
}
echo "</p></div>";
echo "<textarea class='url_load_description' style='display:none;' cols='45'>";
if($description!=""){
echo strip_tags($description);
}else{
echo "DESCRIPTION";
}
echo "</textarea>";
$host=parse_url($url);
echo "<div class='col-md-8 pull-left' style='color:#A4A4A4;'>".strtoupper($host['host'])."</div>";
echo "</div>";

?>
