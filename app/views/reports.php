<?php
echo HTML::style('css/reports.css');
echo HTML::style('css/loadreport.css');
echo HTML::style('css/reportview.css');
echo HTML::script('js/reports.js');
echo HTML::script('js/highcharts.js');
echo HTML::script('js/exporting.js');
if(count($exams)){
?>

<div class='col-lg-12' id='for-afterload' style='overflow:hidden;'>
<div class='form-group' style='float:left;'>
<label for='exam_dropdown'>Select Exams</label>
<select id='exam_dropdown' class='form-control'>
<option value="">Select Exams</option>
<?php
for($i=0;$i<count($exams);$i++){
echo "<option value='".$exams[$i]->clasubkey."'>".stripslashes(urldecode($exams[$i]->clasubname))."</option>";
}
?>
</select>
</div>
</div>
<!--div id='init_exams'>
<div id='exam-value'>Select Exams</div>
<img src='/images/down.png' width='25px' height='25px'/>
</div>
<div id='exam_dropdown'>
<?php
for($i=0;$i<count($exams);$i++){
echo "<div  style='display:none;' id='".$exams[$i]->clasubkey."'>".stripslashes(urldecode($exams[$i]->clasubname))."</div>";
}
?>
</div-->


<div id='reportcontent'>
</div>

<?php
}else{
?>
<div id='message'>No Exams Initiated...</div>
<?php
}
?>
