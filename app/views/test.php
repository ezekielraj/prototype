<?php
function make_absolute($urll, $basee)
{
    // Return base if no url
    if( ! $urll) return $basee;
    // Return if already absolute URL
    if(parse_url($urll, PHP_URL_SCHEME) != '') return $urll;
    // Urls only containing query or anchor
    if($urll[0] == '#' || $urll[0] == '?') return $basee.$urll;
    // Parse base URL and convert to local variables: $scheme, $host, $path
    extract(parse_url($basee));
    // If no path, use /
    if( ! isset($path)) $path = '/';
    // Remove non-directory element from path
    $path = preg_replace('#/[^/]*$#', '', $path);
    // Destroy path if relative url points to root
    if($urll[0] == '/') $path = '';
    // Dirty absolute URL
    $abs = "$host$path/$urll";
    // Replace '//' or '/./' or '/foo/../' with '/'
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {}
    // Absolute URL is ready!
    return $scheme.'://'.$abs;
}
//loading part
$request = curl_init();
curl_setopt_array($request, array
(
    CURLOPT_URL => "http://stantonys-littlemount.in/",
   
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HEADER => FALSE,
   
//    CURLOPT_SSL_VERIFYPEER => TRUE,
//    CURLOPT_CAINFO => 'cacert.pem',

    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_MAXREDIRS => 10,
));
$response = curl_exec($request);
curl_close($request);
$document = new DOMDocument();
if($response)
{
    libxml_use_internal_errors(true);
    $document->loadHTML($response);
    libxml_clear_errors();
}
$images = array();
$base ="http://stantonys-littlemount.in/";
foreach($document->getElementsByTagName('img') as $img)
{
    // Extract what we want
    $image = array
    (
        'src' => make_absolute($img->getAttribute('src'), $base),
    );
   
    // Skip images without src
    if( ! $image['src'])
        continue;

    // Add to collection. Use src as key to prevent duplicates.
    $images[$image['src']] = $image;
}
$images = array_values($images);
print_r($images);
?>
