<?php echo HTML::style('css/Activities.css');
echo HTML::script('js/Activities.js'); 
?>
<link rel="stylesheet" href="http://css-spinners.com/css/spinner/spinner.css" type="text/css">
<style>
.groups_names{
color: #ffffff;
background-color:#848484;
}
#browse-file {
float:left;
display:none;
}
</style>
<div id='textboxshare' class='col-md-12'>
<form class="form-inline">
<div class="form-group" style='margin-right:10px;'>
<button id='status' class='btn btn-info btn-sm' type="button">Status</button>
</div>

<div class="form-group" >
<button id='Announcements' class='btn btn-normal btn-sm' type="button">Announcements</button>
</div>
</form>
<?php
if(Session::has('privilege') && (Session::get('privilege')=="student" || Session::get('privilege')=="parent")){
if(count($groups)>0){
for($i=0;$i<count($groups);$i++){
//echo "<div id='".$groups[$i]->slno."' style='float:right;border:0px;' class='groups_names'>".stripslashes(urldecode($groups[$i]->group_name))."</div>";
}
}
}
?>
<div class='form-group status-upload' style='margin-top:10px;'>
<form>
<textarea id='sharecont' style='white-space: pre-wrap;' class='form-control' placeholder='share something...' rows='1' cols='75'>
</textarea>
<div class="spinner-loader pull-right" id='spinner-loader' style='display:none;'>
  Loading…
</div>
<div id='url_load' style='border:1px solid white;'></div>
<ul><li>
<div id='browse-file' class='form-group'>
<input id='cf-browse-file' type='file' name='cf-file'/></div>
</li><!--li>
<div id='form-group'>
<button id='sharesubmit' class='btn btn-primary btn-sm pull-right' style='display:none;' type='button'>Share</button>
</div>
</li-->
<?php
if(Session::has('privilege') && (Session::get('privilege')=="student" || Session::get('privilege')=="parent")){
if(count($groups)>0){
?>
<!--form class='form-inline'-->
<li>
<div id='head-h3' class='form-group pull-right' style='display:none;'><label for="select-group">Select Subject:</label>
<select id='select-group' class='form-control'>

<?php
for($i=0;$i<count($groups);$i++){

if($groups[$i]->teacher_subject=="all" && $groups[$i]->group_section=="all" && $groups[$i]->group_class=="all"){
continue;
}

$subjecter=explode('_',$groups[$i]->teacher_subject);
if($groups[$i]->teacher_subject=="all"){
$subjecter[1]=urldecode(stripslashes($groups[$i]->group_name))." All Subjects";
}
echo "<option value='".$groups[$i]->slno."'>".strtoupper($subjecter[1])."</option>";
}

?>
</select>
</div>
</li>
<li>
<?php
}
}
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<div class='form-group cf_announce pull-right' style='margin-right:10px;display:none;'><label class='checkbox-inline'><input type='checkbox' id='cf_announce' value='1'>Announce</label></div>";
}
?>
</li>
</ul>
<button id='sharesubmit' class="btn btn-success green" style='display:none;'><i class="fa fa-share"></i> Share it!</button>
</form>
</div>

<div id='shrdcont' class='col-md-12'>

</div>
<div class='col-lg-12'>
<center>
<div class="spinner-loader" id='spinner-loader1' style='display:block;'>
  Loading…
</div></center></div>
