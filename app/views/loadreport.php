<?php
echo HTML::script('js/loadreport.js');
//echo HTML::script('js/highcharts.js');
//echo HTML::script('js/exporting.js');
?>
<script>
$(document).ready(function(){
$('#for-afterload').append("<div style='float:right;' id='nav-addedit'>Add/Edit</div>");
});
</script>
<div id='nav-container'><!--div id='nav-report'>Markwise Report</div><div class='pull-right' id='nav-addedit'>Add/Edit</div--></div>
<div id='loadreportcontainer'>
<style>
.getstureport {
text-decoration: underline;
}
.getstureport:hover {
color:#18BC9C;
cursor: pointer;
}
</style>
<?php
function profpictureload($usr){
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if($usr!=""){
if (strpos(strtolower($entry),strtolower($usr)) !== false) {
$img=$entry;
}
}
        }
    }
    closedir($handle);
}
return "/images/profile/".$img;
}

if($act=="addedit"){
?>
<script>
$(document).ready(function(){
loadAddedit();
});
</script>
<table class='table table-striped table-bordered'>
<thead>
<tr><th colspan='2'>Name</th><th>Mark</th><th width="10px">/</th><th>Out of Mark</th></tr>
</thead>
<tbody>

<!--div id='gp_stuhead'><div class='gp_stuname'>Name</div><div id='gp_mark' style='font-weight:bold;'>Mark</div><div class='outofmark' style='font-weight:bold;text-align:center;width:125px;'>Out of Mark</div></div-->
<?php
for($i=0;$i<count($students);$i++){
//echo "<div id='gp_student'><div class='gp_stuname'><div id='gp_photo'><img src='".profpictureload($students[$i]['stu_username'])."' width='40px' height='40px'></div>".$students[$i]['stu_username']."</div><div id='gp_mark' class='".$students[$i]['stu_username']."'>".$students[$i]['stu_mark']."</div><div id='gp_slash'>/</div><div id='gp_outofmark' class='".$students[$i]['stu_username']."'>".$students[$i]['stu_outofmark']."</div></div>";
echo "<tr id='gp_student'><td style='vertical-align:middle;width:12px;border-right:0px;'><div class='gp_stuname'><div id='gp_photo' style='width:40px;'><img class='img-circle' src='".profpictureload($students[$i])."' width='40px' height='40px'></div></div></td><td style='border-left:0px;vertical-align:middle;font-weight:bold;'>".$students[$i]."</td><td><div id='gp_mark' class='".$students[$i]."'>0</div></td><td><div id='gp_slash'>/</div></td><td><div id='gp_outofmark' class='".$students[$i]."'>100</div></td></tr>";
}
?>
</tbody>
</table>
<?php
//for($i=0;$i<count($students);$i++){
//echo "<div id='gp_student'><div class='gp_stuname'><div id='gp_photo'><img src='".profpictureload($students[$i])."' width='40px' height='40px'></div>".$students[$i]."</div><div id='gp_mark' class='".$students[$i]."'></div><div id='gp_slash'>/</div><div id='gp_outofmark' class='".$students[$i]."'>100</div></div>";
//}
}else{
$stumark=array();
$pstu=array();
$fstu=array();
$n=array();
$e=array();
$s=array();
$si=array();
$f=array();
$fl=array();

for($i=0;$i<count($act);$i++){
$mark=(($act[$i]['stu_mark'])/($act[$i]['stu_outofmark']))*100;
if($mark>=35){
array_push($pstu,$act[$i]['stu_username']);
}else{
array_push($fstu,$act[$i]['stu_username']);
}
$stumark[$act[$i]['stu_username']]=$act[$i]['stu_mark'];
$stuoutofmark[$act[$i]['stu_username']]=$act[$i]['stu_outofmark'];
$stuper[$act[$i]['stu_username']]=(($act[$i]['stu_mark'])/($act[$i]['stu_outofmark']))*100;
if($act[$i]['stu_mark']>=90){
array_push($n,$act[$i]['stu_username']);
}elseif($act[$i]['stu_mark']>=80 && $act[$i]['stu_mark']<90){
array_push($e,$act[$i]['stu_username']);
}elseif($act[$i]['stu_mark']>=70 && $act[$i]['stu_mark']<80){
array_push($s,$act[$i]['stu_username']);
}elseif($act[$i]['stu_mark']>=60 && $act[$i]['stu_mark']<70){
array_push($si,$act[$i]['stu_username']);
}elseif($act[$i]['stu_mark']>=50 && $act[$i]['stu_mark']<60){
array_push($f,$act[$i]['stu_username']);
}else{
array_push($fl,$act[$i]['stu_username']);
}
}
$pp = (count($pstu)/count($act)*100);
echo "<div id='lrcontent'>
<div id='lr-first'>
<span>Total Number of Students: ".count($act)."</span>
<span>Total No. of Passed Students: ".count($pstu)."</span>
<span>Total No. of Failed Students: ".count($fstu)."</span>
<span>Pass Percentage: ".round($pp,1)." %</span>
</div>
<div id='lr-second'>

</div>
<div id='lr-third'>
<!--span>>90 ".count($n)."</span>
<span>90-80 ".count($e)."</span>
<span>80-70 ".count($s)."</span>
<span>70-60 ".count($si)."</span>
<span>60-50 ".count($f)."</span>
<span><50 ".count($fl)."</span-->
<div id=\"container\" style=\"min-width: 310px; height: 400px; margin: 0 auto\"></div>

</div>

<table class='table table-bordered' id='lr-forth'>
</table>

</div>";

//$values = count($n).",".count($e).",".count($s).",".count($si).",".count($f).",".count($fl);
$values = "{ y : ".count($n).",mydata:'";
foreach($n as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($e).",mydata:'";
foreach($e as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($s).",mydata:'";
foreach($s as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($si).",mydata:'";
foreach($si as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($f).",mydata:'";
foreach($f as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($fl).",mydata:'";
foreach($fl as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td><td>".$stuoutofmark[$val]."</td><td>".$stuper[$val]."%</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' }";

?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Exam Results'
        },
        subtitle: {
            text: 'Source: AGrade'
        },
        xAxis: {
            categories: [
                '90',
                '90-80',
                '80-70',
                '70-60',
                '60-50',
                '<50'
            ],
            crosshair: true

        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. Of Students'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>' +
		'<tr><td colspan="2"></td></tr>',
		//'<tr><td colspan="2">{point.mydata}</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
			$('#lr-forth').html("<thead><tr><th>Student Name</th><th>Mark</th><th>Out of Mark</th><th>Percentage</th></tr></thead>");
			$('#lr-forth').append(this.mydata);
//                            alert('Category: ' + this.category + ', value: ' + this.y);
                        }
                    }
                }
            }
        },
        series: [{
            name: 'No. of Students',
            data: [<?php echo $values; ?>]

        }]/*, {
            name: 'New York',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]*/
    });
});
</script>
<script>
$(document).ready(function(){
$(document).on("click",".getstureport",function(){
var stuname = $(this).attr("id");
var ajaxurl="/index.php/class/reports/getstureport";
data={'action': 'getstureport', 'studentname': stuname};
$.post(ajaxurl,data,function(response){
reportoldcontent=$('#lrcontent').html();
$('#nav-addedit').remove();
$('#lrcontent').html("<div class='form-group col-lg-2'><button type='button' class='btn btn-primary' id='reportback'>Back to "+$('#exam_dropdown').find('option:selected').text()+"</button></div>"+response);
$('#for-afterload').css('display','none');
});
});
});
</script>
<?php
}
?>
</div>
