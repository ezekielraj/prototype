<?php echo HTML::style('css/login.css'); ?>
<?php echo HTML::script('js/login.js'); ?>
<style>
.loginoption1{
margin: auto;
}
.loginoption:not(p) {
height: 330px;
opacity: 0.5;
}
.loginoption:hover{
opacity:1;
}
</style>
<script>
window.lclick="no";
window.prev="";
$(document).ready(function(){
$('.loginoption1').click(function(){
if(prev!=""){
var newprev=prev.split('-');
$('#'+prev).css('background-image','url(images/'+newprev[1]+'.png)');
}
prev=$(this).children('.loginoption').attr('id');
$(this).children('.loginoption').css('background-image','none');

$('.loginoption1').find('#login-form').each(function(){
$(this).css('display','none');
});
$(this).find('#login-form').css('display','block');
if(lclick=="no"){
lclick="yes";
$('.loginoption1').css('opacity','0.85');
$(this).css('opacity','1');
var f = $(this).width() / $(this).parent().width() * 100;
if(parseInt(f)<40){
var val = 40-parseFloat(f);
var value = parseFloat(val) / 100 * $(this).parent().width(); 
$(this).animate({
width: "+="+value,
});
$('.loginoption1').not(this).each(function(){
var f = $(this).width() / $(this).parent().width() * 100;
if(parseFloat(f)<15){
var val = 15-parseFloat(f);
var value = parseFloat(val) / 100 * $(this).parent().width(); 
$(this).animate({
width: "+="+value,
});
}else{
var val = parseFloat(f)-15;
var value = parseFloat(val) / 100 * $(this).parent().width(); 
$(this).animate({
width: "-="+value,
});

}
});
}
var h = $(this).children('.loginoption').height();
if(parseInt(h)<400){
var heigh=400 - parseInt(h);
$(this).children('.loginoption').animate({
height: "+="+heigh,
});

$('.loginoption').each(function(){
var h=$(this).height();
if(parseInt(h)>330){
var heigh=parseInt(h)-330;
if($(this).attr('id')=="login-teacher"){
$(this).animate({
height: "-="+heigh,
});
}else{
doubl = parseInt(heigh)*2;
$(this).animate({

height: "-="+doubl,
});
}
}
});
}
if($(this).children('.loginoption').attr('id')=="login-student"){
$('#login-teacher').parent().css('position','absolute');
$('#login-teacher').parent().css('left','');
$('#login-teacher').parent().css('top','8%');
$('#login-parent').parent().css('top','8%');
$('#login-student').parent().css('left','10%');
$('#login-parent').parent().css('right','');
$('#login-student').parent().css('z-index','1000');
$('#login-teacher').parent().css('z-index','500');
$('#login-parent').parent().css('z-index','0');
}else{
if($(this).children('.loginoption').attr('id')=="login-parent"){
$('#login-teacher').parent().css('position','absolute');
var theigh = $('#login-teacher').parent().height();
var space = (400 - parseInt(theigh))/2;
$('#login-teacher').parent().css('left','10%');
$('#login-parent').parent().css('right','10%');
$('#login-student').parent().css('z-index','0');
$('#login-teacher').parent().css('z-index','500');
$('#login-parent').parent().css('z-index','1000');
$('#login-student').parent().css('left','');
$('#login-teacher').parent().css('top','8%');
$('#login-student').parent().css('top','8%');
}else{
$('#login-teacher').parent().css('position','');
$('#login-teacher').parent().css('left','');
$('#login-student').parent().css('left','10%');
$('#login-parent').parent().css('right','10%');
$('#login-student').parent().css('z-index','0');
$('#login-teacher').parent().css('z-index','1000');
$('#login-parent').parent().css('z-index','0');
$('#login-teacher').parent().css('top','');
$('#login-student').parent().css('top','');
$('#login-parent').parent().css('top','');


}
}
/*$('#login-student').css('transform','perspective(750px) scaleZ(-1) rotateY(30deg)');
$('#login-parent').css('transform','perspective(750px) scaleZ(1) rotateY(30deg)');
if($(this).children('.loginoption').attr('id')=="login-student"){
$('#login-teacher').css('transform','perspective(750px) scaleZ(1) rotateY(30deg)');
}else{
if($(this).children('.loginoption').attr('id')=="login-parent"){
$('#login-teacher').css('transform','perspective(750px) scaleZ(-1) rotateY(30deg)');
}
}

$(this).children('.loginoption').css('transform','');
*/
lclick="no";
}
});


});
</script>
<div class='ui three column doubling stackable grid container' style='margin-top:75px;position:relative;'>
<div class='column loginoption1' style='width:27%;'><div class='ui segment loginoption' id='login-student' style="background-image: url(images/student.png); background-size: 100% 100%;">
<center><h2><p style='opacity:1;'>STUDENT</p></h2></center>
<div id='login-form' style='display:none;margin-top:30px;'>
<div class="col-lg-6 col-lg-offset-3 colxs-centered" style=''>
<!--div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Students Login<span id='new-registe'></span></label></div-->
<br>
<div class='whoselogin' style='display:none;'>student</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='studentuser' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<div class="form-group">
<label for="pass">Password</label>
<input id='studentpass' type='password' class="form-control" placeholder='Password'/>
</div>
<center><button type="button" class="btn btn-default login" id='student'>Login</button></center>
<div id='Message'></div>
<div id='student' class='new-register' style='float:right;'>Register</div>
</div>
</div>
<div class='register-whole' id='studentregister-whole' style='display:none; color:#2C3E50;'>

<div id='register-Form' class="col-lg-4 colxs-centered col-lg-offset-4">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='studentreg-key' name='username' value='' required>
<br>
<button class="btn btn-default reg-submitFile" type="button" id="student" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>
</div>
</div>
</div>

</div></div>
<div class='column loginoption1' style='width:27%;'><div class='ui segment loginoption' id='login-teacher' style="background-image: url(images/teacher.png); background-size: 100% 100%;">
<center><h2><p style='opacity:1;'>TEACHER</p></h2></center>
<div id='login-form' style='display:none; margin-top:30px;'>
<div class="col-lg-8 col-lg-offset-2 colxs-centered" style=''>
<!--div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Teachers Login <span id='new-registe'></span></label></div-->
<br>
<div class='whoselogin' style='display:none;'>teacher</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='teacheruser' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<br>
<div class="form-group">
<label for="pass">Password</label>
<input id='teacherpass' type='password' class="form-control" placeholder='Password'/>
</div>
<br>
<center><button type="button" class="btn btn-default login" id='teacher'>Login</button></center>
<div id='Message'></div>
<div id='teacher' class='new-register' style='float:right;'>Register</div>
</div>
</div>
<div class='register-whole' id='teacherregister-whole' style='display:none;color:#2C3E50;'>

<div id='register-Form' class="col-lg-4 colxs-centered col-lg-offset-4">
<div id='register-head'>Register<span><button class='btn btn-default btn-xs'>X</button></span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='teacherreg-key' name='username' value='' required>
<br>
<center><button class="btn btn-default reg-submitFile" type="button" id="teacher" value="Submit" style='cursor:pointer;'>Submit</button></center>
</div>
</div>
</div>
</div>
</div>



</div></div>
<div class='column loginoption1' style='width:27%;'><div class='ui segment loginoption' id='login-parent' style="background-image: url(images/parent.png); background-size: 100% 100%;">

<center><h2><p style='opacity:1;'>PARENT</p></h2></center>
<div id='login-form' style='display:none;margin-top:30px;'>
<div class="col-lg-8 col-lg-offset-2 colxs-centered" style=''>
<!--div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Parents Login<span id='new-registe'></span></label></div-->
<br>
<div class='whoselogin' style='display:none;'>parent</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='parentuser' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<br>
<div class="form-group">
<label for="pass">Password</label>
<input id='parentpass' type='password' class="form-control" placeholder='Password'/>
</div>
<br>
<center><button type="button" class="btn btn-default login" id='parent'>Login</button></center>
<div id='Message'></div>
<div class='new-register' id='parent' style='float:right;'>Register</div>
</div>
</div>
<div class='register-whole' id='parentregister-whole' style='display:none;color:#2C3E50;'>

<div id='register-Form' class="col-lg-4 col-lg-offset-4 colxs-centered">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='parentreg-key' name='username' value='' required>
<br>
<button class="btn btn-default reg-submitFile" type="button" id="parent" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>
</div>
</div>
</div>


</div></div>
</div>
