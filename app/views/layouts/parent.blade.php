<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">

  <title>A - Grade</title>

    {{ HTML::style('css1/font-awesome.min.css') }}
 {{ HTML::style('css1/bootstrap.min.css') }}
 {{ HTML::style('css1/common.css') }}
 {{ HTML::style('css1/menu_css_for_mobile.css.css') }}

<!--  
  {{ HTML::style('css/default.css') }}
{{ HTML::style('css/logout.css') }}
{{ HTML::style('css/tmain.css') }}
{{ HTML::style('css/calender.css') }}
{{ HTML::style('css/bootstrap.min.css') }}
{{ HTML::style('semantic/dist/semantic.min.css') }}

-->
{{ HTML::script('js/jquery-1.11.1.min.js'); }}
{{ HTML::script('js/jquery-ui.js'); }}
{{ HTML::script('js/side-calender.js'); }}
{{ HTML::script('js/bootstrap.min.js'); }}
{{ HTML::script('semantic/dist/semantic.min.js'); }}
{{ HTML::script('js/highcharts.js'); }}
{{ HTML::script('js/exporting.js'); }}
 <!--  {{ HTML::style('css1/big_menu_css.css') }}
 --><link id="changecss" href="/css1/big_menu_css.css" rel="stylesheet">
      
      <link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>

<!-- jquery -->
      <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
      <script id="changejs1" src="/js/big_menujs.js"></script>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <script type="text/javascript">
         function createjscssfile(filename, filetype) {
             if (filetype == "js") { //if filename is a external JavaScript file
                 var fileref = document.createElement('script')
                 fileref.setAttribute("type", "text/javascript")
                 fileref.setAttribute("src", filename)
             } else if (filetype == "css") { //if filename is an external CSS file
                 var fileref = document.createElement("link")
                 fileref.setAttribute("rel", "stylesheet")
                 fileref.setAttribute("type", "text/css")
                 fileref.setAttribute("href", filename)
             }
             return fileref
         }
         
         function replacejscssfile(oldfilename, newfilename, filetype) {
             var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist using
             var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for
             var allsuspects = document.getElementsByTagName(targetelement)
             for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
                 if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename) != -1) {
                     var newelement = createjscssfile(newfilename, filetype)
                     allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i])
                 }
             }
         }
         $(document).ready(function() {
          $(function() {
  $('li.dropdown').on('mouseover', function() {

    var $menuItem = $(this),

        $submenuWrapper = $('> .dropdown-container', $menuItem);
    var menuItemPos = $menuItem.position();
    $submenuWrapper.css({
      top: menuItemPos.top-20,
      left: menuItemPos.left + Math.round($menuItem.outerWidth() * 0.75)
      });
    });
});
             (function() {
                 var count = 1;
                 $(".outdentmenu").click(function() {
                     if (count == 3) {
                         count = 0;
                         $("#wrapper").toggleClass("toggled");
                         $('link[href="css/small_menu_css.css"]').attr('href', 'css/big_menu_css.css');
                         /* this is addded for bind the basic event */
                         $(".dropdown-link, .dropdown").bind('click', function(e) {
                             $thisparent = $(this).parents('li.dropdown');
                             $div = $("div.dropdown-container", $thisparent);
                             $div.slideToggle(500);
                             $("div.dropdown-container").not($div).slideUp(500);
                             $div = "";
                             $thisparent = "";
                             return false;
         
                         });
                         $(".div, .div .a").bind("click", function(e) {
         
                             // Remove any old one
                             $(".ripple").remove();
                             var ar = $(this).attr('class');
                             //alert(ar);
                             $('.dropdown-link').not(this).children('i.right').removeClass('fa-minus').addClass('fa-plus');
                             $('.dropdown-link').not(this).removeClass('clickstay')
                                 //alert($(this).children('i.right').hasClass( "fa-plus" ));
                             if ($(this).children('i.right').hasClass("fa-plus")) {
                                 $(this).children('i.right').removeClass('fa-plus').addClass('fa-minus');
                                 $(this).addClass('clickstay');
                             } else {
         
                                 $(this).children('i.right').removeClass('fa-minus').addClass('fa-plus');
                                 $(this).removeClass('clickstay');
                             }
         
         
                             // Setup <i class="fa fa-tasks"></i>
         
                             var posX = $(this).offset().left,
                                 posY = $(this).offset().top,
                                 buttonWidth = $(this).width(),
                                 buttonHeight = $(this).height();
         
                             // Add the element
         
                             $(this).append("<span class='ripple'></span>");
         
                             // Make it round!
                             if (buttonWidth >= buttonHeight) {
                                 buttonHeight = buttonWidth;
                             } else {
                                 buttonWidth = buttonHeight;
                             }
         
                             // Get the center of the element
                             var x = e.pageX - posX - buttonWidth / 2;
                             var y = e.pageY - posY - buttonHeight / 2;
         
         
                             // Add the ripples CSS and start the animation
                             $(".ripple").css({
                                 width: buttonWidth,
                                 height: buttonHeight,
         
                                 top: y + 'px',
                                 left: x + 'px'
                             }).addClass("rippleEffect");
         
         
         
         
                         });
         
         
                     } else if (count == 1) {
         
                         $(".dropdown").unbind("click");
                         $(".dropdown-link").unbind("click");
                         $(".dropdown-container").unbind("slideToggle");
                         $(".dropdown-container").unbind("slideUp");
                         $(".dropdown").bind('click', function(e) {});
                         $(".dropdown-link").bind('click', function(e) {});
                         $(".dropdown-container").bind('click', function(e) {});
         
                         $('link[href="/css1/big_menu_css.css"]').attr('href', '/css1/small_menu_css.css');
                         // replacejscssfile("css/big_menu_css.css", "css/small_menu_css", "css");
         
                         replacejscssfile("/js/big_menujs.js", "/js/small_menujs.js", "js");
         
                     } else if (count == 2) {
                         //alert("df");
                         $("#wrapper").toggleClass("toggled");
                         $('link[href="/css1/small_menu_css.css"]').attr('href', '/css1/big_menu_css.css');
                     }
                     count++;
                 });
             })();
         
         
         
             c = 0;
         
             $(".imguser,#username-top").on("click", function() {
         
                 $(".ag-ct-userpop").slideToggle();
                 $("li.userd a").toggleClass('fontsizetoggler').delay(800);
         
                 /*   if(c==0){
                      $("li.userd a").css("font-size", "1.2em").delay( 800 );
                      c++;
                    
                    }
                    else{
                  $("li.userd a").css("font-size", "1em").delay( 800 );
                    c=0;
                    }
                   */
             }).children().click(function(e) {
                 return false;
             });
             $(".searchme").on("click", function() {
                 $(".searchbar").slideToggle(100);
             });
         });
      </script>


<style>
a, a:visited, a:hover, a:active {
  color: inherit;
text-decoration:none;
}
</style>
<script>
function loadprofileform(){
try{
var ajaxurl='/index.php/loadprofileform';
data={'action':'loadprofileform'};
$.post(ajaxurl,data,function(response){
if(response){
$('body').append(response);
//alert(response);
}
});
}catch(err){
alert(err.message);
}
}
$(window).load(function(){
var pathname = window.location.pathname;
var path = pathname.split("parent/");
var pathh = path[1].split("/");
$('#'+pathh[0]).parent().attr('class','active');


});
$(document).ready(function(){
$(document).on('click','#lpfhead > span',function() {
$('#loadprofileformcontent').remove();
});

$('#fullname').click(function() {
loadprofileform();
});

});
</script>
{{ HTML::script('js/parent.js'); }}
</head>
<body>
    
      <div class="logo">
         <li class="sidebar-brand" style="list-style-type: none;">
            <a href="#">
            Agrade 
            </a><sup class="motto">we make it for you!</sup>
         </li>
      </div>

              <div id="wrapper" class="scrolleffect default-skin">
         <div id="sidebar-wrapper"  >

            <ul class="sidebar-nav" id="boxscroll">
               <!-- remove this for small menubar -->
               <li class="sidebar-brand ">
                  <div class="motto  mp"> <span>NAVIGATION <!--  i class="fa fa-expand"></i>--></span>
                  </div>
                  <div class="navi"> </div>
                  <!-- horizontal line --> 
               </li>

  <li role="presentation" ><a class="a " id='classfeed' href="{{ url('parent/classfeed/'.$sid) }}"><i class="left fa fa-tachometer"></i><span id='grpmenu' class="headtextmenu">Class Feed</span></a></li>
  <li role="presentation" ><a class="a " id='group' href="{{ url('parent/group/'.$sid) }}"><i class="left fa fa-cloud-upload"></i><span id='grpmenu' class="headtextmenu">Classes</span></a></li>
  <li role="presentation" ><a class="a " id='attendance' href="{{ url('parent/attendance/'.$sid) }}"><i class="left fa fa-cogs"></i><span id='grpmenu' class="headtextmenu">Attendance</span></a</li>
  <li role="presentation" ><a class="a " id='assignments' href="{{ url('parent/assignments/'.$sid) }}"><i class="left fa fa-envelope"></i><span id='grpmenu' class="headtextmenu">Assignments</span></a></li>
  <li role="presentation" ><a class="a " id='reports' href="{{ url('parent/reports/'.$sid) }}"><i class="left fa fa-share"></i><span id='grpmenu' class="headtextmenu">Reports</span></a></li>

            </ul>
         </div>
                            <div class="topheader">
            <ul class="head-menu-ul-left">
{{
"";
if(isset($fullname)){

              echo '<li class="outdentmenu"><i class="fa fa-outdent fa-lg menudent"></i>
               </li>
               <li><i class="fa fa-search fa-lg menudent searchme"></i>
               </li>
            </ul>
            <ul class="head-menu-ul">
               <li class="iconmap">
                  <div class="round round-sm hollow green">9</div>
                  <i class="fa  fa-md fa-bell up"></i> 
               </li>
               <li class="iconmap">
                  <div class="round round-sm hollow green">12</div>
                  <i class="fa fa-md fa-user"></i> 
               </li>
               <li class="iconmap">
                  <div class="round round-sm hollow green">6</div>
                  <i class="fa  fa-md fa-envelope"></i>
               </li>';
echo HTML::script('js/logout.js');

$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if (strpos($entry,Session::get('username')) !== false) {
$img=$entry;
}
        }
    }
    closedir($handle);
}
echo                "<li class=\"userd\">

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='/images/profile/".$img."' class=\"img-circle imguser\" alt=\"".$fullname."\" width=\"30px\" height=\"px\">
                  <div class=\"ag-ct-userpop\">
                     <div class=\"buttonlogout\">
                        <div id='logout' class=\"button_base b05_3d_roll\">
                           <div id='logout'>Sure?</div>
                           <div id='logout'>Logout!</div>
                        </div>
                     </div>
                     <ul>
                        <li id='fullname'><i id='fullname' class=\"fa fa-user\"></i> My Profile </li>
                        <li> <i class=\"fa fa-check\"></i> Tasks</li>
                        <li><i class=\"fa fa-calendar-times-o\"></i> Time Line</li>
                        <li><i class=\"fa fa-cog\"></i> Settings</li>
                     </ul>
                  </div>

               </li>
               <li class=\"userd\"><a href=\"#\" id=\"username-top\" title=\"Header\">".$fullname."
                  </a>
               </li>";

}
}}

            </ul>
         </div>
<div id="page-content-wrapper">
<br>
<br>
<br>
<div class="container-fluid contentarea">
              <div class="searchbar">
                  <i>Search me</i>
                  <form>
                     <input type="text" placeholder="Lets SEARCH ThE WeBSitE!" />
                  </form>
               </div>
    
 

<div id="content">

<div id="tcontent">

<div id="tmaincontent" class="row">
{{ HTML::style('css/groupMgr.css') }}
<div id='grp_name' style='display:none;'></div>
<div id='grp_num' style='display:none;'>{{ $sid }}</div>
<div id='sub_name' style='visibility:hidden;'></div>
<div id='grpmcontent' class='col-lg-12'>
{{ $content }}
</div>
<div id='leftcal' class='col-lg-3' style="display:none;">
<style scoped>
    td.highlight {border: none !important;padding: 1px 0 1px 1px !important;background: none !important;overflow:hidden;}
td.highlight a {background: #9F9F9F url(bg.png) 50% 50% repeat-x !important;  border: 1px #88a276 solid !important;}
.ui-datepicker th {
text-align:center;
}
</style>
<div id='side-calender' style='float:right;border:0px solid black;width:234px;'>
</div>
<div id='seldate' style='display:none'></div>
<div id='calevents' style='margin:10px 0px;float:right;width:234px;'>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</div>

</body>
</html>
