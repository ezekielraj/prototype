<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
 
  <title>A - Grade</title>
  <!-- css -->   
 {{ HTML::style('css1/font-awesome.min.css') }}
 {{ HTML::style('css1/bootstrap.min.css') }}
 {{ HTML::style('css1/common.css') }}
 {{ HTML::style('css1/menu_css_for_mobile.css.css') }}
 {{ HTML::style('css1/big_menu_css.css') }}
      <link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>

<!-- jquery -->
      <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
      <script id="changejs1" src="js/big_menujs.js"></script>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <script type="text/javascript">
         function createjscssfile(filename, filetype) {
             if (filetype == "js") { //if filename is a external JavaScript file
                 var fileref = document.createElement('script')
                 fileref.setAttribute("type", "text/javascript")
                 fileref.setAttribute("src", filename)
             } else if (filetype == "css") { //if filename is an external CSS file
                 var fileref = document.createElement("link")
                 fileref.setAttribute("rel", "stylesheet")
                 fileref.setAttribute("type", "text/css")
                 fileref.setAttribute("href", filename)
             }
             return fileref
         }
         
         function replacejscssfile(oldfilename, newfilename, filetype) {
             var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist using
             var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for
             var allsuspects = document.getElementsByTagName(targetelement)
             for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
                 if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename) != -1) {
                     var newelement = createjscssfile(newfilename, filetype)
                     allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i])
                 }
             }
         }
         $(document).ready(function() {
          $(function() {
  $('li.dropdown').on('mouseover', function() {

    var $menuItem = $(this),

        $submenuWrapper = $('> .dropdown-container', $menuItem);
    var menuItemPos = $menuItem.position();
    $submenuWrapper.css({
      top: menuItemPos.top-20,
      left: menuItemPos.left + Math.round($menuItem.outerWidth() * 0.75)
      });
    });
});
             (function() {
                 var count = 1;
                 $(".outdentmenu").click(function() {
                     if (count == 3) {
                         count = 0;
                         $("#wrapper").toggleClass("toggled");
                         $('link[href="css/small_menu_css.css"]').attr('href', 'css/big_menu_css.css');
                         /* this is addded for bind the basic event */
                         $(".dropdown-link, .dropdown").bind('click', function(e) {
                             $thisparent = $(this).parents('li.dropdown');
                             $div = $("div.dropdown-container", $thisparent);
                             $div.slideToggle(500);
                             $("div.dropdown-container").not($div).slideUp(500);
                             $div = "";
                             $thisparent = "";
                             return false;
         
                         });
                         $(".div, .div .a").bind("click", function(e) {
         
                             // Remove any old one
                             $(".ripple").remove();
                             var ar = $(this).attr('class');
                             //alert(ar);
                             $('.dropdown-link').not(this).children('i.right').removeClass('fa-minus').addClass('fa-plus');
                             $('.dropdown-link').not(this).removeClass('clickstay')
                                 //alert($(this).children('i.right').hasClass( "fa-plus" ));
                             if ($(this).children('i.right').hasClass("fa-plus")) {
                                 $(this).children('i.right').removeClass('fa-plus').addClass('fa-minus');
                                 $(this).addClass('clickstay');
                             } else {
         
                                 $(this).children('i.right').removeClass('fa-minus').addClass('fa-plus');
                                 $(this).removeClass('clickstay');
                             }
         
         
                             // Setup <i class="fa fa-tasks"></i>
         
                             var posX = $(this).offset().left,
                                 posY = $(this).offset().top,
                                 buttonWidth = $(this).width(),
                                 buttonHeight = $(this).height();
         
                             // Add the element
         
                             $(this).append("<span class='ripple'></span>");
         
                             // Make it round!
                             if (buttonWidth >= buttonHeight) {
                                 buttonHeight = buttonWidth;
                             } else {
                                 buttonWidth = buttonHeight;
                             }
         
                             // Get the center of the element
                             var x = e.pageX - posX - buttonWidth / 2;
                             var y = e.pageY - posY - buttonHeight / 2;
         
         
                             // Add the ripples CSS and start the animation
                             $(".ripple").css({
                                 width: buttonWidth,
                                 height: buttonHeight,
         
                                 top: y + 'px',
                                 left: x + 'px'
                             }).addClass("rippleEffect");
         
         
         
         
                         });
         
         
                     } else if (count == 1) {
         
                         $(".dropdown").unbind("click");
                         $(".dropdown-link").unbind("click");
                         $(".dropdown-container").unbind("slideToggle");
                         $(".dropdown-container").unbind("slideUp");
                         $(".dropdown").bind('click', function(e) {});
                         $(".dropdown-link").bind('click', function(e) {});
                         $(".dropdown-container").bind('click', function(e) {});
         
                         $('link[href="css/big_menu_css.css"]').attr('href', 'css/small_menu_css.css');
                         // replacejscssfile("css/big_menu_css.css", "css/small_menu_css", "css");
         
                         replacejscssfile("js/big_menujs.js", "js/small_menujs.js", "js");
         
                     } else if (count == 2) {
                         //alert("df");
                         $("#wrapper").toggleClass("toggled");
                         $('link[href="css/small_menu_css.css"]').attr('href', 'css/big_menu_css.css');
                     }
                     count++;
                 });
             })();
         
         
         
             c = 0;
         
             $(".imguser,#username-top").on("click", function() {
         
                 $(".ag-ct-userpop").slideToggle();
                 $("li.userd a").toggleClass('fontsizetoggler').delay(800);
         
                 /*   if(c==0){
                      $("li.userd a").css("font-size", "1.2em").delay( 800 );
                      c++;
                    
                    }
                    else{
                  $("li.userd a").css("font-size", "1em").delay( 800 );
                    c=0;
                    }
                   */
             }).children().click(function(e) {
                 return false;
             });
             $(".searchme").on("click", function() {
                 $(".searchbar").slideToggle(100);
             });
         });
      </script>


 {{ HTML::script('js/jquery-1.11.1.min.js'); }}
{{ HTML::script('js/jquery-ui.js'); }}
{{ HTML::script('js/bootstrap.min.js'); }}
<!--  {{ HTML::style('semantic/dist/semantic.min.css') }}
{{ HTML::script('semantic/dist/semantic.min.js'); }}
-->
</head>
<body>

      <div class="logo">
         <li class="sidebar-brand" style="list-style-type: none;">
            <a href="#">
            Agrade 
            </a><sup class="motto">we make it for you!</sup>
         </li>
      </div>

      <div id="wrapper" class="scrolleffect default-skin toggled">
         <!-- Sidebar -->
         
        
         <div class="topheader">
            <ul class="head-menu-ul-left">
{{
"";
if(isset($fullname)){

              echo '<!--  li class="outdentmenu"><i class="fa fa-outdent fa-lg menudent"></i>
               </li>
               <li><i class="fa fa-search fa-lg menudent searchme"></i>
               </li-->
            </ul>
            <ul class="head-menu-ul">
               <!-- li class="iconmap">
                  <div class="round round-sm hollow green">9</div>
                  <i class="fa  fa-md fa-bell up"></i> 
               </li>
               <li class="iconmap">
                  <div class="round round-sm hollow green">12</div>
                  <i class="fa fa-md fa-user"></i> 
               </li>
               <li class="iconmap">
                  <div class="round round-sm hollow green">6</div>
                  <i class="fa  fa-md fa-envelope"></i>
               </li-->';
echo HTML::script('js/logout.js');

$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if (strpos($entry,Session::get('username')) !== false) {
$img=$entry;
}
        }
    }
    closedir($handle);
}
echo                "<li class=\"userd\">

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='/images/profile/".$img."' class=\"img-circle imguser\" alt=\"".$fullname."\" width=\"30px\" height=\"px\">
                  <div class=\"ag-ct-userpop\">
                     <div class=\"buttonlogout\">
                        <div id='logout' class=\"button_base b05_3d_roll\">
                           <div id='logout'>Sure?</div>
                           <div id='logout'>Logout!</div>
                        </div>
                     </div>
                     <ul>
                        <li id='fullname'><i id='fullname' class=\"fa fa-user\"></i> My Profile </li>
                        <li> <i class=\"fa fa-check\"></i> Tasks</li>
                        <li><i class=\"fa fa-calendar-times-o\"></i> Time Line</li>
                        <li><i class=\"fa fa-cog\"></i> Settings</li>
                     </ul>
                  </div>

               </li>
               <li class=\"userd\"><a href=\"#\" id=\"username-top\" title=\"Header\">".$fullname."
                  </a>
               </li>";

}
}}

            </ul>
         </div>

<div id="page-content-wrapper">

    <!-- nav style='background-color:#616161;' class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
<a class="navbar-brand" href="#" style='color:#FFF;font-weight:bold;'>A - GRADE</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div>
      </div>
    </nav-->

<!--  div class="container-fluid contentarea">

</div-->
<div id="content" class="container-fluid contentarea">
<br><br><br>
 {{ $content }}
</div>
</div>
</div>
</div>
      <script>
         $(".pictureuploadicon a").click(function(e) {
         
             //alert("df");
             $("#mypiccon").slideToggle(100);
         });
         
         $("#menu-toggle").click(function(e) {
             e.preventDefault();
             //alert("df");
             $("#wrapper").toggleClass("toggled");
         });
      </script>
      <script>
         $(document).ready(function() {
             $('[data-toggle="popover"]').popover();
             $("[data-toggle=tooltip]").tooltip();
         
             $('.comment').click(function(e) {
                 $(this).parent().parent().next().slideToggle();
             });
         
         });
      </script>

</body>
</html>
