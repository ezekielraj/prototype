<?php
for($i=0;$i<count($result);$i++){
$style="";
if (time() > strtotime($result[$i]->as_enddate)){
$style = "style='background-color:#E1A7A7;'";
}

?>
<?php echo "<div id='singleassign' ".$style." class='assign_".$result[$i]->as_id."'>"; ?>
<?php echo "<div id='assign_title' class='title_".$result[$i]->as_id."'>".stripslashes(urldecode($result[$i]->as_title)); ?></div>
<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<div id='assign_delete' class='".$result[$i]->as_id."'><button type='button' class='btn btn-primary btn-xs'>X</button></div>"; 
}
?>
<div id='assign_desc' class='col-lg-8'><?php echo implode(' ', array_slice(explode(' ', stripslashes(urldecode($result[$i]->as_description))), 0, 10)); ?>...</div>
<div id='assign_fulldesc' class='col-lg-8'><?php echo stripslashes(urldecode($result[$i]->as_description)); ?></div>
<div id='assign_pdate' class='col-lg-4'><b>Posted on: </b><?php
$datee= explode(' ',$result[$i]->as_currentdate);
 echo $datee[0]; ?></div>
<div id='assign_sdate' class='col-lg-8' style='display:none;'><b>Start Date:</b> <span><?php $startdatee = explode(' ',$result[$i]->as_startdate); echo $startdatee[0]; ?></span></div><div id='assign_edate' class='col-lg-4'><b>Due Date: </b><span><?php $enddatee = explode(' ',$result[$i]->as_enddate); echo $enddatee[0]; ?></span></div>
<?php 
if($result[$i]->as_attachment!=""){
if(File::exists(storage_path()."/assignment/".$result[$i]->as_attachment)){
//echo storage_path()."/assignment/".$result[$i]->as_attachment;

?>
<div id='assign_attach' class='col-lg-12'>
<b>Attachment: </b><?php 
echo HTML::link('/class/assignment/gattach/'.$result[$i]->as_attachment,stripslashes(urldecode($result[$i]->as_attachment))); ?>
</div>
<?php
}
}
?>
<br>
<br>
<br>
<?php 
if (time() > strtotime($result[$i]->as_enddate)){
echo "<div id='assign_mark' class='".$result[$i]->as_id."' data-gnum='".$result[$i]->as_groupnum."'><button type='button' class='btn btn-primary'>View/Mark Awards</button></div>"; 
}
?>

<?php 
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<div id='assign_edit' class='".$result[$i]->as_id."'><button type='button' class='btn btn-primary'>edit</button></div>"; 
}

?>
</div>



<?php
}
?>
