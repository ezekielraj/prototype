<script>
window.oldcontent = null;
window.secselectedStudents = [];
window.secvisible = [];
window.mgrpcontent = null;
function secGroupUpdate(){
var grpnum=$('#grp_num').text();
var ajaxurl='/index.php/class/group/secgroupupdate';
data={'action':'secgroupupdate','grpnum':grpnum,'sstudents':secselectedStudents};
$.post(ajaxurl,data,function(response){
if(response=="s"){
$('#sec-addstudent').css('display','none');
location.reload();
}else{
alert(response);
}
});
}

function secGetStudents(){
var grpnum=$('#grp_num').text();
var ajaxurl='/index.php/class/group/secgetstudents';
data={'action':'secgetstudents','grpnum':grpnum};
$.post(ajaxurl,data,function(response){
if(response){

var obj = $.parseJSON(response);
secvisible = obj.students;
if(obj){
$('#sec-studentlist').html("<div id='all' class='col-lg-3'><img class='img-responsive' width='80px'/><span>All</span></div>");
for(var i=0;i<obj.students.length;i++){
var myString=obj.students[i].split(',');
$('#sec-studentlist').append("<div id='"+myString[0]+"' class='col-lg-3'><img class='img-responsive' width='80px'/><span>"+myString[1]+"</span></div>");
}
}


}
});
}

$(document).ready(function() {
$(document).on('click','#gp_stuname_addi',function(){
var stuname = $(this).attr('id');
var stuuname = stuname.split('-');
//var ajaxurl= '/index.php/student/getawards';
var ajaxurl='/index.php/student/getprofile';
//data={'action': 'getawards','stuuname':$(this).text()};
data={'action': 'getprofile','stuuname':$(this).text()};
$.post(ajaxurl, data, function(response){
if(response){
oldcontent = $('#all_grp_sectionfall').html();
//alert(response);
$('#all_grp_sectionfall').html(response);
}
});
});

$(document).on('click','#aw-back-button',function(){
$('#all_grp_sectionfall').html(oldcontent);
});

$(document).on('click','#gc-addstudent',function() {
$('.sec-asclass-name').text($('.class-name').text()+" "+$('.subject-name').text());
$('#sec-addstudent').css('display','block');
secGetStudents();
});

$(document).on('click','#sec-ashead > span',function(){
$('#sec-addstudent').css('display','none');

});

$(document).on('click','#sec-studentlist > div',function(){
if($(this).attr('id')=="all"){
if($(this).attr('class')=="selected"){
$('#sec-studentlist > div').removeClass( "selected" );
}else{
for(var i=0;i<secvisible.length;i++){
var myString = secvisible[i].split(',');
if($.inArray(myString[0],secselectedStudents) == -1){
secselectedStudents.push(myString[0]);
}
}
$('#sec-studentlist > div').addClass( "selected" );
}
}else{
var ind = $.inArray($(this).attr('id'),secselectedStudents);
if(ind > -1){
secselectedStudents.splice(ind,1);
$('#sec-studentlist > #all').removeClass('selected');
$(this).removeClass( "selected" );
}else{
secselectedStudents.push($(this).attr('id'));
$(this).addClass( "selected" );
}
}
});

$(document).on('click','#sec-goimage',function() {
secGroupUpdate();
});


});
</script>

<?php
echo HTML::style('css/students.css');
echo HTML::script('js/forawards.js');
$k = "";
for($l=0;$l<count($posawards);$l++){
$pa = $posawards[$l]->awardsName;
$k=$k."<div id='".$posawards[$l]->awardskey."'>".$pa[0].$pa[1]."</div>";
}
$m = "";
for($n=0;$n<count($negawards);$n++){
$na = $negawards[$n]->awardsName;
$m=$m."<div id='".$negawards[$n]->awardskey."'>".$na[0].$na[1]."</div>";
}
?>
<style>
#aw-back-button {
margin-bottom:10px;
}
.getstureport{
cursor:pointer;
}
.del-student {
border:1px solid black;
padding:2px;
height:20px;
width:20px;
line-height:1 !important;
text-align:center;
margin:10px;
cursor:pointer;
}
</style>
<div id='all_grp_sectionfall' class='col-lg-12'>
<div id='grp_selection' class='col-lg-12'>
<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>
<!--button type="button" class="btn btn-info btn-sm pull-right" id='grpaward'>Award Multiple Students</button!-->
<?php
}
?>

</div>
<br>
<br>
<div id='awards-value-pos' style='display:none;'><div id='sel_pos'><?php echo $k; ?></div></div>
<div id='awards-value-neg' style='display:none;'><div id='sel_neg'><?php echo $m; ?></div></div>

<div class="table col-md-12">
<table class='table table-bordered'>
    <thead>
      <tr>
        <th colspan='3'>Name</th>
        <th>Rank</th>
        <th>Grades</th>
        <!--th>Reports</th-->
        <!--th>Contact Parent</th-->
        <!--th>Awards</th-->
        <th>Delete</th>
      </tr>
    </thead>
<tbody>
<!--tr id='gp_stuhead' style='font-weight:bold;'><td class='gp_stuname'>Name</td><td id='gp_rank' style='font-weight:bold;'>Rank</td><td id='gp_grade' style='font-weight:bold;'>Grades</td><td>R</td><td>C</td><td class='awards' style='font-weight:bold;text-align:center;width:125px;'>Awards</td><div class='gp_stupos'>Positive</div><div class='gp_stuneg'>Negative</div><td>Delete</td></tr-->
<?php
function profpictureload($usr){
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if($usr!=""){
if (strpos(strtolower($entry),strtolower($usr)) !== false) {
$img=$entry;
}
}
        }
    }
    closedir($handle);
}
return "/images/profile/".$img;
}

$j="";
for($i=0;$i<count($result);$i++){
$students=explode(',',$result[$i]);
$j=$j."<tr id='gp_student' class='".$students[0]."'><td id='gp_stuname_addi' class='gp_stuname' style='vertical-align:middle;width:10px;border-bottom:1px solid white;'>";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$j=$j."<div id='mulselector' style='display:none;'><input type='checkbox' name='student' value='".$students[0]."'></div>";
}
$j=$j."</td><td style='border-bottom:1px solid white;'><div id='gp_photo'><img class='img-circle' src='".profpictureload($students[0])."' width='40px' height='40px'></div></td><td id='gp_stuname_addi' class='gp_stuname' style='vertical-align:middle;text-decoration:underline;border-bottom:1px solid white;border-right:1px solid white;'>".$students[0]."</td><td id='gp_rank' style='text-decoration:underline;border:1px solid white;vertical-align:middle;'><div id=\"".$students[0]."\" class=\"getstureport\" style='text-align:center;'>".$students[3]."</div></td><td id='gp_grade' style='vertical-align:middle;text-align:center;border:1px solid white;'>".$students[1]."</td><!--td style='vertical-align:middle;border:1px solid white;'><div id=\"".$students[0]."\" class=\"getstureport\" style='text-align:center;'>Nil</div></td--><!--td style='vertical-align:middle;text-align:center;border:1px solid white;'>C</td--><!--td class='gp_pos' id='".$students[0]."' style='border:1px solid white;'><div class='btn-group pull-right'><button type='button' class='btn btn-success pull-right dropdown-toggle' data-toggle='dropdown' id='aw-pos'> + <span class='caret'</span></button><ul class='dropdown-menu' role='menu'><li><a href='#' id='pos_ontask' class='pos_awdmulstu'>On task</a></li><li><a href='#' id='pos_participating' class='pos_awdmulstu'>Participating</a></li><li><a href='#' id='pos_workinghard' class='pos_awdmulstu'>Working hard</a></li></ul></div></td--><!--div class='gp_neg' id='".$students[0]."'>-</div--><td class='del-student' style='border:1px solid white;vertical-align:middle;' id='".$students[0]."'>x</td></tr>";
}
$j=$j."</tbody></table></div>";
echo $j;
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<button type='button' class='btn btn-info btn-sm' id='gc-addstudent'>Add Students</button>";
//echo "<div id='gc-addstudent'>Add Students</div>";
}
?>

</div>
<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>
<!--div id='gc-addstudent'>Add Students</div-->
<div id="sec-addstudent" style="display:none;">
<style scoped>
#sec-aswhole {
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    z-index: 10;
    background-color: rgba(0, 0, 0, 0.5);
	overflow:auto;
}
#sec-asform {
border:1px solid white;
background-color:#ffffff;
margin-top:100px;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
   -webkit-animation: fadein 2s; /* Safari and Chrome */
       -moz-animation: fadein 2s; /* Firefox */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera */
            animation: fadein 2s;
}
#sec-ashead {
border-bottom: 1px solid rgb(128, 128, 128);
line-height: 2;
padding-left: 30px;
}
#sec-ashead > span {
font-size: 12px;
float: right;
line-height: 4;
padding-right: 25px;
padding-left: 25px;
border-left: 1px solid rgb(128, 128, 128);
cursor: pointer;
}
#sec-asinput {
padding-left:20px;
height:460px;
}
#sec-studentlist {
border-radius: 3px 3px 3px 3px;
margin: 6px;
float: left;
overflow: auto;
height: 300px;
border: 1px solid rgb(209, 199, 172);
}
#sec-studentlist div{
height: 100px;
cursor: pointer;
float: left;
border: 1px solid black;
margin-left: 20px;
margin-top: 20px;
}
#sec-allGo {
float:right;
bottom: 0px;
right: 0px;
margin: 15px;
}
#sec-goimage{
    background-image: url("/images/go.png");
    background-repeat: no-repeat;
    background-size: 78px 78px;
    cursor: pointer;
    width: 80px;
    height: 80px;
}
#sec-smessage{
float: left;
padding-left: 5px;
padding-right: 15px;
margin: 10px;
line-height: 3;
font-size: 12px;
color: red;
height: 30px;
}
.selected {
background-image:url('/images/accept-icon.png');
background-repeat:no-repeat;
background-size:20px 20px;
background-position: right bottom;
}

</style>
<div id="sec-aswhole">
<div class='container'>
<div id="sec-asform" class='col-lg-offset-2 col-lg-8'>
<h3 id="sec-ashead">
Add Student
<span>X</span>
</h3>
<div id="sec-asinput">
<div class="sec-asclass-name">
</div>

<div id="sec-studentlist" class='col-lg-10'>



</div>
<div id='sec-allGo'><div id='sec-goimage'></div></div>
<div id='sec-smessage'></div>
</div>

</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
$(document).on('click','.del-student',function(){
var stuuname = $(this).attr("id");
var grpnum = $('#grp_num').html();
var ajaxurl = '/index.php/class/group/removestudent';
data = {'action':'removestudent','groupnum': grpnum,'stuuname': stuuname};
$.post(ajaxurl,data,function(response){
if(response=="s"){
location.reload();
}else{
alert(response);
}
});
});

$(document).on("click",".getstureport",function(){
var stuname = $(this).attr("id");
var ajaxurl="/index.php/class/reports/getstureport";
data={'action': 'getstureport', 'studentname': stuname};
$.post(ajaxurl,data,function(response){
oldcontent = $('#all_grp_sectionfall').html();
$('#all_grp_sectionfall').html("<button id='aw-back-button' class='btn btn-primary'>Back</button>"+response);
});
});


});
</script>
<?php
}
?>
