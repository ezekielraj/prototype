<?php
echo HTML::script('js/loadreport.js');
?>
<div id='nav-container'>
<div id='nav-report'>Markwise Report</div>
<div id='nav-swreport'>Subjectwise Report</div>
<?php
if($starting!="all"){
?>
<div id='nav-calrank'>Calculate Rank</div>
<?php
}
?>
</div>
<div id='loadreportcontainer'>
<style>
.getstureport {
text-decoration: underline;
}
.getstureport:hover {
color:#18BC9C;
cursor: pointer;
}
</style>
<?php
function profpictureload($usr){
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if($usr!=""){
if (strpos(strtolower($entry),strtolower($usr)) !== false) {
$img=$entry;
}
}
        }
    }
    closedir($handle);
}
return "/images/profile/".$img;
}
$stumark=array();
$pstu=array();
$fstu=array();
$n=array();
$e=array();
$s=array();
$si=array();
$f=array();
$fl=array();
//print_r($act);
for($i=0;$i<count($act);$i++){
$username="";
for($j=0;$j<count($act[$i]);$j++){
$username=$act[$i][$j]->stu_username;
$mark=(($act[$i][$j]->stu_mark)/($act[$i][$j]->stu_outofmark))*100;
if($mark<35){
array_push($fstu,$act[$i][$j]->stu_username);
break;
}//else{
//array_push($fstu,$act[$i]['stu_username']);
//}
}
$mark=0;
$outofmark=0;
for($j=0;$j<count($act[$i]);$j++){
$mark=$mark+$act[$i][$j]->stu_mark;
$outofmark=$outofmark+$act[$i][$j]->stu_outofmark;
}

if($outofmark!=0){
$avg = (($mark/$outofmark)*100);
}else{
$avg=0;
}
$stumark[$username]=$avg."%";
if($avg>=90){
array_push($n,$username);
}elseif($avg>=80 && $avg<90){
array_push($e,$username);
}elseif($avg>=70 && $avg<80){
array_push($s,$username);
}elseif($avg>=60 && $avg<70){
array_push($si,$username);
}elseif($avg>=50 && $avg<60){
array_push($f,$username);
}else{
if(trim($username)!=""){
array_push($fl,$username);
}
}
}
$pp = ((count($act)-count($fstu))/count($act)*100);
echo "<div id='lrcontent'>
<div id='lr-first'>
<span>Total Number of Students: ".count($act)."</span>
<span>Total No. of Passed Students: ".(count($act) - count($fstu))."</span>
<span>Total No. of Failed Students: ".count($fstu)."</span>
<span>Pass Percentage: ".round($pp,1)." %</span>
</div>
<div id='lr-second'>
</div>
<div id='lr-third'>
<!--span>>90 ".count($n)."</span>
<span>90-80 ".count($e)."</span>
<span>80-70 ".count($s)."</span>
<span>70-60 ".count($si)."</span>
<span>60-50 ".count($f)."</span>
<span><50 ".count($fl)."</span-->
<div id=\"container\" style=\"min-width: 310px; height: 400px; margin: 0 auto\"></div>

</div>

<table id='lr-forth' class='table table-bordered'>

</table>

</div>";

//$values = count($n).",".count($e).",".count($s).",".count($si).",".count($f).",".count($fl);
$values = "{ y : ".count($n).",mydata:'";
foreach($n as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
}
$values=$values."' },{ y: ".count($e).",mydata:'";
foreach($e as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
}
$values=$values."' },{ y: ".count($s).",mydata:'";
foreach($s as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($si).",mydata:'";
foreach($si as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($f).",mydata:'";
foreach($f as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' },{ y: ".count($fl).",mydata:'";
foreach($fl as $val){
$values=$values."<tr><td id=\"".$val."\" class=\"getstureport\">".$val."</td><td>".$stumark[$val]."</td></tr>";
//$values=$values."<div id=\"".$val."\" class=\"getstureport\">".$val."</div>";
}
$values=$values."' }";

?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Exam Results'
        },
        subtitle: {
            text: 'Source: AGrade'
        },
        xAxis: {
            categories: [
                '90',
                '90-80',
                '80-70',
                '70-60',
                '60-50',
                '<50'
            ],
            crosshair: true

        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. Of Students'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>' +
		//'<tr><td colspan="2">{point.mydata}</td></tr>',
		'<tr><td colspan="2"></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
			$('#lr-forth').html("<thead><tr><th>Student Name</th><th>Overall Percentage</th></tr></thead>");
			$('#lr-forth').append(this.mydata);
//                            alert('Category: ' + this.category + ', value: ' + this.y);
                        }
                    }
                }
            }
        },
        series: [{
            name: 'No. of Students',
            data: [<?php echo $values; ?>]

        }]/*, {
            name: 'New York',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]*/
    });
});
</script>
<script>
$(document).ready(function(){
$('#mymodal').hide();
$(document).on("click",".getstureport",function(){
var stuname = $(this).attr("id");
var ajaxurl="/index.php/class/reports/getstureport";
data={'action': 'getstureport', 'studentname': stuname};
$.post(ajaxurl,data,function(response){
$('#lrcontent').html(response);
});
});

$(document).on("click",'#nav-calrank',function(){
var ajaxurl="/index.php/class/reports/calrank";
var grpnum=$('#grp_num').html();
data={'action':'calrank','grpnum':grpnum,'examkey':$('#exam_dropdown').val()};
$.post(ajaxurl,data,function(response){
$('#mymodal').text(response);
$('#mymodal').show();
setTimeout(function() {$('#mymodal').hide();}, 4000);
});
});
});
</script>
</div>
<style>
#mymodal{
border: 1px solid #D8D8D8;
background-color:#F2F5A9;
color:#848484;
height:40px;
position: fixed;
top:100px;
left:50%;
padding-left:10px;
padding-right:10px;
z-index: 10000;
-webkit-border-radius: 7px;
-moz-border-radius: 7px;
border-radius: 7px;
}
</style>
<div id="mymodal">
asdfads
</div>

