<div id='student_overallreport' class='table-responsive col-lg-12'>
<?php

//print_r($subjects);
//print_r($examreport);
?>
<!--div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div-->

<h2 id='sor_head'>Historical Report</h2>
<h4>Name: <b><?php echo $stuname; ?></b></h4>
<table id='sor_examreports' class='table table-bordered'>
<thead><tr id='sor_singlereport'><!--th id='sor_slno'>slno</th--><th id='sor_examname'>Exam Name</th>
<?php
$gseries=array();
for($k=0;$k<count($subjects);$k++){
$sub = explode('_',$subjects[$k]->stu_subjectname);
if($subjects[$k]->stu_subjectname=="all"){
echo "<th id='sor_subjects' class='".$subjects[$k]->stu_subjectname."'>All</th>";
}else{
echo "<th id='sor_subjects' class='".$subjects[$k]->stu_subjectname."'>".strtoupper($sub[1][0].$sub[1][1])."</th>";
}
}
?>
<th id='sor_subjects'>Total</th><th id='sor_rank'>Rank</th></tr></thead><tbody>
<?php
$overall="";
$gexamnames="";
for($i=0;$i<count($examreport);$i++){
if($i==count($examreport)-1){
$gexamnames=$gexamnames."'".stripslashes(urldecode($examreport[$i]['examdetails']->clasubname))."'";
}else{
$gexamnames=$gexamnames."'".stripslashes(urldecode($examreport[$i]['examdetails']->clasubname))."', ";
}
$repo="<tr id='sor_singlereport'><!--td id='sor_slno'>".($i+1)."</td--><td id='sor_examname' class='".$examreport[$i]['examdetails']->clasubkey."'>".stripslashes(urldecode($examreport[$i]['examdetails']->clasubname))."</td>";
$t=0;
$ot=0;
for($j=0;$j<count($examreport[$i]['examresults']);$j++){
$repo=$repo."<td id='sor_subjectmark' class='".$examreport[$i]['examresults'][$j]->stu_subjectname."'>".$examreport[$i]['examresults'][$j]->stu_mark."</td>";
$t=$t+$examreport[$i]['examresults'][$j]->stu_mark;
$ot=$ot+$examreport[$i]['examresults'][$j]->stu_outofmark;

}
$repo=$repo."<td id='sor_subjectmark'>".$t."</td>";
if(isset($examreport[$i]['examrank'][0]->overallrank)){
$repo=$repo."<td>".$examreport[$i]['examrank'][0]->overallrank."</td></tr>";
}
echo $repo;
if($i==count($examreport)-1){
$overall=$overall.(($t/$ot)*100);
}else{
$overall=$overall.(($t/$ot)*100).",";
}
}
?>
</body>
</table>

<script>
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Historical Report',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: Agrade',
            x: -20
        },
        xAxis: {
            categories: [<?php echo $gexamnames; ?>]
        },
        yAxis: {
            title: {
                text: 'Marks'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '%'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
<?php
for($i=0;$i<count($subjects);$i++){
$entry="{ name: '".$subjects[$i]->stu_subjectname."', data: [";
for($j=0;$j<count($examreport);$j++){
for($k=0;$k<count($examreport[$j]['examresults']);$k++){
if($examreport[$j]['examresults'][$k]->stu_subjectname==$subjects[$i]->stu_subjectname){
if($j==count($examreport)-1){
$entry=$entry.$examreport[$j]['examresults'][$k]->stu_mark;
}else{
$entry=$entry.$examreport[$j]['examresults'][$k]->stu_mark.",";
}
}
}
}
$entry=$entry."]";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$entry=$entry.",visible: ( $('#sub_name').text() == \"".$subjects[$i]->stu_subjectname."\" || $('#allsubnav3').text() == \"".$subjects[$i]->stu_subjectname."\" ) ? true : false";
}
if($i==count($subjects)-1){
$entry=$entry."},";
}else{
$entry=$entry."},";
}
echo $entry;
}
echo "{ name: 'Overall', data: [".$overall."]}";
?>
        ]
    });
});
</script>

</div>
