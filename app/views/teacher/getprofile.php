<style>
#aw-back-button {
float:left;
}
</style>
<?php
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if (strpos($entry,$result['studentdetails'][0]->stu_username) !== false) {
$img=$entry;
}
        }
    }
    closedir($handle);
}
?>
<div class='col-lg-12'>
<button id='aw-back-button' class='btn btn-primary'>Back</button>

<div id='saw-profpic' class='col-lg-12'><center>
<img class='img-circle' src='/images/profile/{{ $img; }}' width="140px" height="140px"/></center>
</div>
<center><h2><?php echo stripslashes(urldecode($result['studentdetails'][0]->stu_fullname)); ?></h2></center>
<div class='col-lg-8'>
<p>Class: <?php echo $result['class'][0]->clasubname; ?> </p>
</div>



<div class='col-lg-10'>
<h3>Exam Marks</h3>
<table class='table table-bordered'>
<thead>
<tr>
<th>Exam Name</th>
<th>Subjects/Marks</th>
</tr>
</thead>
<tbody>
<?php
foreach($result['report'] as $roww){
echo "<tr><td>".$roww['name']."</td><td>";
$reports=$roww['result'];
?>
<table class='table table-bordered'>
<thead>
<tr>
<th>Subjects</th>
<th>Marks</th>
<th>Out of Marks</th>
</tr>
</thead>
<tbody>
<?php
for($i=0;$i<count($reports);$i++){
echo "<tr>";
$sub = explode('_',$reports[$i]->stu_subjectname);
echo "<td>".$sub[1]."</td>";
echo "<td>".$reports[$i]->stu_mark."</td>";
echo "<td>".$reports[$i]->stu_outofmark."</td>";
echo "</tr>";
}
?>
</tbody>
</table>
<?php
echo "</td></tr>";
}
?>
</tbody>
</table>
</div>

<div class='col-lg-10'>
<h3>Attendance</h3>
<table class='table table-bordered'>
<thead>
<tr>
<th>Total</th>
<th>Present</th>
<th>Absent</th>
</tr>
</thead>
<tbody>
<?php
$att=$result['attendance'];
echo "<tr><td>".$att['all']."</td><td>".$att['pre']."</td><td>".$att['abs']."</td></tr>";
?>
</tbody>
</table>
</div>



<div class='col-lg-10'>
<h3>Teachers' Comments</h3>
</div>

<div id='stu-contactinfo' class='col-lg-10'>
<h3>Contact Info</h3>
<ul>
<li>Email: <?php echo Session::get('emailid'); ?></li>

<li>Parent Name: <?php if(isset($result['parentdetails'][0]->par_fullname) && $result['parentdetails'][0]->par_fullname!=""){ echo stripslashes(urldecode($result['parentdetails'][0]->par_fullname)); } ?></li>
<li>Parent Contact: <?php if(isset($result['parentdetails'][0]->emailaddress) && $result['parentdetails'][0]->emailaddress!=""){ echo "<a href='mailto:".stripslashes(urldecode($result['parentdetails'][0]->emailaddress))."'>".stripslashes(urldecode($result['parentdetails'][0]->emailaddress))."</a>"; } ?></li>
</ul>
</div>



</div>
