<?php echo HTML::style('css/assignments.css'); ?>
<!--<?php echo HTML::style('css/jquery-ui.css'); ?>-->
<?php echo HTML::script('js/assignments.js'); ?>
<style>
#all_assignments {
margin: 20px 0px;
}
#singleassign{
border-radius: 4px;
cursor:pointer;
background-color:#BADDC9;
padding:10px 10px 10px 10px;
margin-bottom:5px;
overflow:hidden;
}
#assign_title{
font-weight:bold;
font-size:22px;
width:350px;
float:left;
}
#assign_desc {
font-size:15px;
/*width:350px;
float:left;*/
}
#assign_pdate {
/*float:left;
width:150px;*/
font-size:12px;
}
#assign_fulldesc {
/*width:350px;
float:left;*/
display:none;
}
#assign_sdate,#assign_edate {
/*float:left;
width:350px;
margin-top:3px;*/
font-size:12px;
display:none;
}
#assign_attach {
display:none;
/*float:left;*/
font-size:12px;
}
#assign_delete {
float:right;
display:none;
}
#assign_edit {
float:right;
display:none;
}
#assign_mark {
float:right;
display:none;
margin-left:10px;
}
#asedit_whole,#asmark_whole {
display:none;
position:fixed;
top: 0px;
left: 0px;
height: 100%;
width: 100%;
z-index: 10;
background-color: rgba(0, 0, 0, 0.5);
overflow:auto;
}
#eas_create{
border:1px solid white;
background-color:#ffffff;
margin-top:10%;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
   -webkit-animation: fadein 2s; /* Safari and Chrome */
       -moz-animation: fadein 2s; /* Firefox */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera */
            animation: fadein 2s;
}
#mas_create{
border:1px solid white;
background-color:#ffffff;
margin-top:10%;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
   -webkit-animation: fadein 2s; /* Safari and Chrome */
       -moz-animation: fadein 2s; /* Firefox */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera */
            animation: fadein 2s;
}
#eashead,#mashead {
border-bottom:1px solid #808080;
line-height:3;
font-size:18px;
color: #2C3E50;
}
#eashead span {
font-size:12px;
float:right;
line-height:4;
padding-right:25px;
padding-left:25px;
border-left:1px solid #808080;
cursor:pointer;
}
#mashead span {
font-size:12px;
float:right;
line-height:4;
padding-right:25px;
padding-left:25px;
border-left:1px solid #808080;
cursor:pointer;
}
#massign_id,#massign_title,#massgin_header {
margin:10px;
}
#massgin_header {
padding-left:80px;
overflow:hidden;
font-weight:bold;
line-height:3;
}
#massgin_header > div {
float:left;
}
#massign_stugrades {
width:100px;
}
#gp_photo {
    float: left;
    margin-right: 10px;
}
#massign_stuname{
font-weight: bold;
line-height:2;
}
#massign_assignmentmark,#massign_outofmark {
border:1px solid #DDD;
border-radius:2px;
line-height:2;
}
#markbar{
float: left;
margin-top:15px;
padding:5px;
height:18px;
width:3px;
}
#overallstudentsdiv {
overflow:auto;
padding-bottom:30px;
}
</style>
<div id='acontent' class='col-lg-12'>
<h2>Assignments</h2>

<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>


<button id='createassign' type='button' class='btn btn-default'><div id='createassign'>Create an Assignment</div></button>
<div id='as_create' style='display:none;' class='col-lg-12'>
<div id='as_title'>Title: <input type='text' id='astitle_name' name='as_title' placeholder='Untitled Assignment'/></div>
<div id='as_date'>start date: <input type='text' readonly='readonly' id='stdate' class='datepicker'><span>end date: </span><input type='text' id='enddate' class='datepicker' readonly='readonly'><div class='dterr'></div></div>
<div id='wdesp'>
<span>Description</span>
<div id='desp_buttons'><div id='d_bold' onclick='document.execCommand('bold',false,null);' class='chtext'>B</div><div id='d_italics' class='chtext'><i>i</i></div></div>
<div id='as_description' onClick=\"this.contentEditable='true';\"></div>
</div>
<div id='as_attachment'>
<div><span>Attachment:</span><br>
<img src='/images/fupload.png' width='110px'/><input type='file' name='file' id='ffile' style='visibility:hidden;'></input>
<div class='uploadfile'></div>
</div>
</div>
<div id='as_submit'><button type='button' class='btn btn-default'>
Submit</button>
</div>
</div>
<?php
}
?>

 <div id='all_assignments' class='col-lg-12'>

</div>

<div class="btn-group pull-left" role="group" aria-label="...">
  <button type="button" id='ass_prevbutton' class="btn btn-default"><</button>
  <button type="button" id='ass_morebutton' class="btn btn-default">></button>
</div>

</div>

<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>

<div id='asedit_whole'>
<div id='eas_create' style='display:none;' class='col-lg-offset-3 col-lg-6'>
<div id='eashead'>Edit Assignment<span>X</span></div>
<div id='eas_id' style='display:none;'></div>
<div id='eas_title'>Title: <input type='text' id='eastitle_name' name='as_title' placeholder='Untitled Assignment'/></div>
<div id='eas_date'>start date: <input type='text' readonly='readonly' id='estdate' class='edatepicker'><span>end date: </span><input type='text' id='eenddate' class='edatepicker' readonly='readonly'><div class='edterr'></div></div>
<div id='ewdesp'>
<span>Description</span>
<div id='edesp_buttons'><div id='ed_bold' onclick='document.execCommand('bold',false,null);' class='echtext'>B</div><div id='ed_italics' class='echtext'><i>i</i></div></div>
<div id='eas_description' onClick=\"this.contentEditable='true';\"></div>
</div>
<div id='eas_attachment'>
<div><span>Attachment:</span><br>
<img src='/images/fupload.png' width='110px'/><input type='file' name='file' id='effile' style='visibility:hidden;'></input>
<div class='euploadfile'></div>
</div>
</div>
<div id='eas_submit'>
<button type='button' class='btn btn-default'>
Update
</button>
</div>
</div>
</div>

<?php
}
?>

<div id='asmark_whole'>
<div id='mas_create' style='display:none;' class='col-lg-offset-3 col-lg-6'>
<div id='mashead' class='col-lg-12'>Student Submissions<span>X</span></div>
<div id='massign_id' class='col-lg-12'>Assignment ID: <span></span></div>
<div id='massign_title' class='col-lg-12'>Assignment Title: <span></span>
<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>
<button type='button' class='btn btn-default pull-right' id='massign_save'>Save</button>
<?php
}
$ce="";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$ce="contenteditable='true'";
}
?>
</div>

<?php
function profpictureload($usr){
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if($usr!=""){
if (strpos(strtolower($entry),strtolower($usr)) !== false) {
$img=$entry;
}
}
        }
    }
    closedir($handle);
}
return "/images/profile/".$img;
}
?>
<table class='table table-bordered'>
<thead>
<tr>
<th>Students</th>
<th>Awards</th>
<th><div id='massign_stupoints'>Points: <span style='border:1px solid #DDD; padding:5px;' <?php echo $ce; ?>>100</span></div></th>
</tr>
</thead>
<tbody>
<?php
$students = explode(',',$stulist);
for($i=0;$i<count($students);$i++){
echo "<tr id='student_assigngrade'>
<td style='vertical-align:middle;'><div id='gp_photo'><img class='img img-circle' src='".profpictureload($students[$i])."' width='40px' height='40px'></div><div id='massign_stuname'>".$students[$i]."</div></td>
<td><div id='massign_assignmentmark' class='".$students[$i]."' ".$ce.">0</div></td>
<td><div id='massign_outofmark' class='outof_".$students[$i]."' ".$ce.">0</div></td>
</tr>";
}
?>
</tbody>
</table>
<div id='overallstudentsdiv'>
<?php 

/*$students = explode(',',$stulist);
for($i=0;$i<count($students);$i++){
echo "<div id='student_assigngrade'><div id='gp_photo'><img src='".profpictureload($students[$i])."' width='40px' height='40px'></div><div id='massign_stuname'>".$students[$i]."</div><div id='massign_assignmentmark' class='".$students[$i]."' ".$ce.">0</div><div id='markbar'>/</div>
<div id='massign_outofmark' class='outof_".$students[$i]."' ".$ce.">0</div>
</div>";
}*/
?>
</div>

</div>
</div>

