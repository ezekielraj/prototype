<div id='aeuserremove'>
<script>

function sectionchanger(std){
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection','code':std};
$.post(ajaxurl,data, function(response){
if(response){
$('#select-section').html(response);
}
});
}


$(document).ready(function(){
$('#aeuserhead > span').click(function(){
$('#aeuserremove').remove();
});


$('#select-class').change(function(){
$('.tip').css('display','none');
var std = $(this).val();
if(std==""){
$('#select-section').html("<option value=''>Select Section</option>");
}else{
sectionchanger(std);
}
});


$('#submit').click(function(){
var firstname=$('#firstname').val();
var lastname=$('#lastname').val();
var username=$('#username').val();
var email=$('#email').val();
var password=$('#password').val();
var rpassword=$('#rpassword').val();
var role=$('#select-role').val();
var userkey=$('#user-key').val();
var userclass=$('#select-class').val();
var usersection=$('#select-section').val();
var ajaxurl='/index.php/admin/createuser';
data={'action':'createuser','firstname':firstname,'lastname':lastname,'username':username,'email':email,'password':password,'rpassword':rpassword,'role':role,'userkey':userkey,"userclass":userclass,"usersection":usersection};
$.post(ajaxurl,data,function(response){
alert(response);
});

});

});

</script>
<style scoped>
#aeuserwhole {
position:absolute;
top: 0;
left: 0;
height: 1000px;
width: 100%;
z-index: 10;
background-color: rgba(0,0,0,0.5);
overflow:auto;
}
#aeuserform {
border:1px solid white;
background-color:#ffffff;
margin-top:100px;
padding-bottom:30px;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
   -webkit-animation: fadein 2s; /* Safari and Chrome */
       -moz-animation: fadein 2s; /* Firefox */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera */
            animation: fadein 2s;
}
#aeuserhead {
border-bottom:1px solid #808080;
line-height:3;
padding-left:30px;
font-size:18px;
color: #2C3E50;
}
#aeuserhead span {
font-size:12px;
float:right;
line-height:4;
padding-right:25px;
padding-left:25px;
border-left:1px solid #808080;
cursor:pointer;
}
</style>
<div id='aeuserwhole'>
<div id='aeuserform' class='col-lg-8 col-lg-offset-2'>
<div id="aeuserhead" style="">
    Add/ Edit User
    <span>
        X
    </span>
</div>

<div class='col-lg-12'>
<div class='col-lg-6'>
<div class='form-group'>
<label for='firstname'>First Name</label><input id='firstname' class='form-control' type='text' name='firstname'/>
</div>

<div class='form-group'>
<label for='lastname'>Last Name</label><input id='lastname' class='form-control' type='text' name='lastname'/>
</div>

<div class='form-group'>
<label for='username'>Username</label><input id='username' class='form-control' type='text' name='username'/>
</div>

<div class='form-group'>
<label for='email'>Email ID</label><input id='email' class='form-control' type='email' name='email'/>
</div>

<div class='form-group'>
<label for='password'>Password</label><input id='password' type='password' class='form-control' name='password'/>
</div>

<div class='form-group'>
<label for='rpassword'>Retype Password</label><input id='rpassword' type='password' class='form-control' name='rpassword'/>
</div>

<div class='form-group'>
<button type='button' class='btn btn-default' id='submit'>Add User</button>
</div>
</div>

<div class='col-lg-6'>
<div class='form-group'>
<label for='select-role'>Select Role</label>
<select id='select-role' class='form-control'>
<option value='student'>Student</option>
<option value='teacher'>Teacher</option>
<option value='parent'>Parent</option>
<option value='administrator'>Administrator</option>
</select>
</div>
<h3>To Choose Class and Section</h3>
<div class='form-group'>
<label for='user-key'>User Key</label>
<input type='text' id='user-key' class='form-control'/>
</div>
<center><h3>(OR)</h3></center>

<div class='form-group'>
<label for='select-class'>Select Class</label>
<select id='select-class' class='form-control'>
<option value=''>Select Class</option>
<?php
if(is_array($result)){
for($i=0;$i<count($result);$i++){
echo "<option value='".$result[$i]->clasubkey."'>".$result[$i]->clasubname."</option>";
}
}
?>
</select>
</div>

<div class='form-group'>
<label for='select-section'>Select Section</label>
<select id='select-section' class='form-control'>
<option value=''>Select Section</option>
</select>
</div>




</div>
</div>



</div>
</div>
</div>
