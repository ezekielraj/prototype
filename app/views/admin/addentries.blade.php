<style>
#acwhole,#aswhole,#aewhole {
    position: fixed;
    top: 0px;
    left: 0px;
    height: 1000px;
    width: 100%;
    z-index: 100;
    background-color: rgba(0, 0, 0, 0.5);
    overflow: auto;
}
#acwholeform,#aswholeform,#aewholeform {
border: 1px solid #FFF;
background-color: #FFF;
margin-top: 100px;
padding-bottom: 30px;
border-radius: 4px;
animation: 2s ease 0s normal none 1 running fadein;
}
#achead,#ashead,#aehead {
border-bottom: 1px solid #808080;
line-height: 3;
padding-left: 30px;
font-size: 18px;
color: #2C3E50;
}
#achead > span,
#ashead > span,
#aehead > span {
font-size: 12px;
float: right;
line-height: 4;
padding-right: 25px;
padding-left: 25px;
border-left: 1px solid #808080;
cursor: pointer;
}
</style>
<script>
jQuery(document).ready(function(){
jQuery('#add-class').click(function(){
var ajaxurl='/index.php/admin/addclass';
data={'action':'addclass'};
jQuery.post(ajaxurl,data, function(response){
jQuery('body').append(response);
});
});

jQuery('#add-subject').click(function(){
var ajaxurl='/index.php/admin/addsubject';
data={'action':'addsubject'};
jQuery.post(ajaxurl,data, function(response){
jQuery('body').append(response);
});
});

jQuery('#add-exam').click(function(){
var ajaxurl='/index.php/admin/addexam';
data={'action':'addexam'};
jQuery.post(ajaxurl,data, function(response){
jQuery('body').append(response);
});
});

jQuery(document).on('click','#achead span',function(){
jQuery('#acwhole').remove();
window.location.href='/index.php/admin/addentries';
});

jQuery(document).on('click','#ashead span',function(){
jQuery('#aswhole').remove();
window.location.href='/index.php/admin/addentries';
});

jQuery(document).on('click','#aehead span',function(){
jQuery('#aewhole').remove();
window.location.href='/index.php/admin/addentries';
});

jQuery('.classdelete').click(function(){
var ajaxurl='/index.php/admin/removeclass';
data={'action':'removeclass','id':jQuery(this).data('value')};
jQuery.post(ajaxurl,data,function(response){
if(response=="s"){
window.location.href="/index.php/admin/addentries";
}else{
alert("Cannot be deleted");
}
});
});

});
</script>
<h4>Classes</h4>
<button id='add-class' class='btn btn-default pull-right' type='button'>Add Class</button>
<table class='table table-bordered table-striped'>
<thead>
<tr>
<th>Class Name</th>
<th>Class Key</th>
<th>Class Remove</th>
</tr>
</thead>
<tbody>
@foreach($class as $val)
<tr><td>{{ stripslashes(urldecode($val->clasubname)) }} </td><td>{{ stripslashes(urldecode($val->clasubkey)) }}</td>
<td><button class='btn btn-default classdelete' id='classdelete' data-value='{{ $val->slno }}' type='button'>Delete</button></td>
</tr>
@endforeach
</tbody>
</table>
<h4>Subjects</h4>
<button id='add-subject' class='btn btn-default pull-right' type='button'>Add Subject</button>
<table class='table table-bordered table-striped'>
<thead>
<tr>
<th>Subject Name</th>
<th>Subject Key</th>
<th>Subject Remove</th>
</tr>
</thead>
<tbody>
@foreach($subject as $val)
<tr><td>{{ stripslashes(urldecode($val->clasubname)) }} </td><td>{{ stripslashes(urldecode($val->clasubkey)) }}</td>
<td><button class='btn btn-default classdelete' id='classdelete' data-value='{{ $val->slno }}' type='button'>Delete</button></td></tr>
@endforeach
</tbody>
</table>
<h4>Exams</h4>
<button id='add-exam' class='btn btn-default pull-right' type='button'>Add Exam</button>
<table class='table table-bordered table-striped'>
<thead>
<tr>
<th>Exam Name</th>
<th>Exam Key</th>
<th>Exam Remove</th>
</tr>
</thead>
<tbody>
@foreach($exam as $val)
<tr><td>{{ stripslashes(urldecode($val->clasubname)) }} </td><td>{{ stripslashes(urldecode($val->clasubkey)) }}</td>
<td><button class='btn btn-default classdelete' id='classdelete' data-value='{{ $val->slno }}' type='button'>Delete</button></td>

</tr>
@endforeach
</tbody>
</table>
