<script>
window.sactive=0;

function sectionchanger(std){
var ajaxurl = '/index.php/admin/gensection';
data={'action':'gensection','code':std};
$.post(ajaxurl,data, function(response){
if(response){
$('#schsection').html(response);
}
});
}


function getuser(){
var ajaxurl='/index.php/admin/getuser';
data={'action':'privilege','priv':"all",'schclass':$('#schclass').val(),'schsection':$('#schsection').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#userdetails').html(response);
}
});
}

$(document).ready(function(){
getuser();
$('#user-filter').click(function(){
var priv=$('#privilege').val();
var ajaxurl='/index.php/admin/getuser';
data={'action':'privilege','priv':priv,'schclass':$('#schclass').val(),'schsection':$('#schsection').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#userdetails').html(response);
}
});
});
$('#user-clear').click(function(){
var ajaxurl='/index.php/admin/getuser';
data={'action':'privilege','priv':'all','schclass':'%','schsection':'%'};
$.post(ajaxurl,data,function(response){
if(response){
$('#userdetails').html(response);
}
});
});
$('#funame').keyup(function(){
if(sactive==0){
sactive=1;
var priv=$('#privilege').val();
var funame=$('#funame').val();
var ajaxurl='/index.php/admin/getuser1';
data={'action':'privilege','priv':priv,'funame':funame,'schclass':$('#schclass').val(),'schsection':$('#schsection').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#userdetails').html(response);
sactive=0;
}else{
sactive=0;
}
});

}
});

$('#privilege').change(function(){
var priv=$(this).val();
if(priv=="student"){
$('#schclass').css('display','block');
$('#schsection').css('display','block');
}else{
$('#schclass').css('display','none');
$('#schsection').css('display','none');


}
});

$('#schclass').change(function(){
var std = $(this).val();   
if(std==""){
$('#schsection').html("<option value='%'>Select Section</option>");
}else{
sectionchanger(std);
}
});

$('#adduser').click(function(){
var ajaxurl='/index.php/admin/addedituser';
data={'action':'addedituser'};
$.post(ajaxurl,data,function(response){
if(response){
$('body').append(response);
}
});
});
});
</script>
<h2>User Management</h2>
<div class='col-lg-12' style='margin-top:10px;margin-bottom:10px;'><button type='button' id='adduser' class='btn btn-default pull-right'>Add User</button></div>
<div class='col-lg-12'>
<div class='form col-lg-7'>
<div class='form-group'>
<label for='privilege'>Select Privilege</label>
<select id='privilege' class='form-control'>
<option value='all'>All</option>
<option value='teacher'>Teacher</option>
<option value='student'>Student</option>
<option value='parent'>Parent</option>
<option value='administrator'>Admin</option>
</select>
</div>

<div class='form-group'>
<select id='schclass' class='form-control' style='display:none;'>
<option value='%'>Select Class</option>
<?php
if(is_array($result)){
for($i=0;$i<count($result);$i++){
echo "<option value='".$result[$i]->clasubkey."'>".$result[$i]->clasubname."</option>";
}
}
?>
</select>
</div>

<div class='form-group'>
<select id='schsection' class='form-control' style='display:none;'>
<option value='%'>Select Section</option>
</select>
</div>

<div class='form-group'>
<button id='user-filter' class='btn btn-default'>Filter</button>
</div>
<div class='form-group'>
<label for='funame'>Search For:</label><input id='funame' type='text' class='form-control' placeholder='Type Name or Email'/>
</div>
<div class='form-group'>
<button id='user-clear' type='button' class='btn btn-default'>Clear</button>
</div>
</div>
</div>
<div class='col-lg-12' id='userdetails'>
<table class='table table-bordered'>
<thead>
<tr>
<th>Full Name</th>
<th>username</th>
<th>Email</th>
<th>Privilege</th>
</tr>
</thead>
<tbody>
<?php

//for($i=0;$i<count($result);$i++){
//echo "<tr><td>".stripslashes(urldecode($result[$i]->fullname))."</td><td>".stripslashes(urldecode($result[$i]->username))."</td><td>".stripslashes(urldecode($result[$i]->emailaddress))."</td><td>".stripslashes(urldecode($result[$i]->privilege))."</td></tr>";
//}
?>
</tbody>
</table>
</div>
