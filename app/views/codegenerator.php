<?php
echo HTML::style('css/codegenerator.css').HTML::script('js/codegenerator.js');
echo "<div class='form-inline'>";
?>
<style scoped>
/* base CSS element */
.tip {

	background: #eee;
	border: 1px solid #ccc;
	padding: 10px;
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	position: absolute;
	width: 200px;
	top:-50px;
}

/* arrows - :before and :after */
.tip:before {
	position: absolute;
	display: inline-block;
	border-left: 7px solid transparent;
	border-top: 7px solid #eee;
	border-right: 7px solid transparent;
	border-top-color: rgba(0, 0, 0, 0.2);
	left: 20px;
	top: 40px;
	content: '';
}

.tip:after {
	position: absolute;
	display: inline-block;
	border-left: 6px solid transparent;
	border-top: 6px solid #eee;
	border-right: 6px solid transparent;
	left: 20px;
	top: 40px;
	content: '';
}
</style>
<?php
echo "<div class='form-group'><select id=\"standards\" class='form-control'>";
echo "<option value=''>Select Class</option>";
for($i=0;$i<count($result);$i++){
echo "<option value='".$result[$i]->feature_class."'>".$result[$i]->clasubname."</option>";
}

echo "</select>
<div class='tip' style='display:none;'>Please select the Class</div>
</div><div class='form-group col-lg-offset-1'>";
echo "<select id='sections' class='form-control'>";
echo "<option value=''>Select Section</option>";


echo "</select></div>";

echo "<div class='form-group col-lg-offset-1'><input id='code-generate' style='display:none;' class='btn btn-primary' type='button' value='Generate'></div></div>";
//echo "<input id='code-clear' type='button' value='Clear'>";
echo "<div class='form-inline' style='padding:20px;'><div id='add-section' class='form-group col-lg-3 col-lg-offset-1'><input id='add-section' type='button' class='btn btn-success' value='Add Section'></div>";
echo "<div class='form-group col-lg-3 col-lg-offset-1' id='del-section'><input id='del-section' type='button' class='btn btn-danger' value='Delete Section'></div></div>";
echo "<div class='col-lg-12' id='code-generator-content' style='margin-top:20px;'></div>";

?>

<div id='cgwhole' style='display:none;'>
<div id='cgform' class='col-lg-offset-4 col-lg-4'>

<div id='cghead'>Add Section<span>X</span></div>

<div style='overflow:hidden;' class='col-lg-12'>
<div class='col-lg-6'>
<div class='full-section-list'>
</div>
</div>
<div class='pull-right col-lg-6'>
<div class='col-lg-12' style='margin-top:10px;margin-bottom:10px;'>
<input type='button' class='btn btn-primary' id='as-add-section' value='New Section'>
</div>
<div class='col-lg-12' style='margin-top:10px;margin-bottom:10px;'>
<input class='btn btn-primary' type='button' id='as-add-section-done' value='Done'></div>

</div>
</div>

</div>
</div>


<div id='dswhole' style='display:none;'>
<div id='dsform' class='col-lg-offset-4 col-lg-4' >

<div id='dshead'>Delete Section<span>X</span></div>

<div style='overflow:hidden;' class='col-lg-12'>
<div class='col-lg-6'>
<div class='ds-full-section-list'>
</div>
</div>
<div class='pull-right col-lg-6'>
<div class='col-lg-12' style='margin-top:10px;margin-bottom:10px;'>
<input class='btn btn-primary' type='button' id='ds-del-section' value='Delete Section'>
</div>
<div class='col-lg-12' style='margin-top:10px;margin-bottom:10px;'>
<input class='btn btn-primary' type='button' id='ds-del-section-done' value='Done'></div>

</div>
</div>

</div>
</div>

