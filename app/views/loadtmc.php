<style>
#caddstu {
font-size:50px;
text-align:center;
cursor: pointer;
color: #B7B5B5;
line-height:1.9;
border:4px solid #B7B5B5;
width:100px;
height:100px;
float:left;
margin:30px;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
   -webkit-animation: fadein 2s; /* Safari and Chrome */
       -moz-animation: fadein 2s; /* Firefox */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera */
            animation: fadein 2s;
}

</style>
<script>
window.pnselectedStudents = [];
window.pnvisible = [];
window.pnregisterkey="";
$(document).ready(function(){
$('#caddstu').click(function(){
$('#pnregister-whole').css('display','block');
});
$(document).on('click','#pnregister-head > span',function() {
$('#pnregister-whole').css('display','none');
});
$('#pnreg-submitFile').click(function(){
pnregisterkey=$('#pnreg-key').val();
var ajaxurl = '/index.php/pngetstudents';
data = { 'action':'pngetstudents','regcode':$('#pnreg-key').val() };
$.post(ajaxurl,data,function(response){
if(response){
var obj = $.parseJSON(response);
pnvisible = obj.students;
if(obj.students!=""){
if(obj){
$('#pnregister-input').html("<div>Please Select Your Kid</div>");
for(var i=0;i<obj.students.length;i++){
var myString=obj.students[i].split(',');
$('#pnregister-input').append("<input type='radio' id='pnstuDents' name='pnstudents' value='"+myString[0]+"'>"+myString[1]+"</input><br>");
}
$('#pnregister-input').append("<input id='pnselect-student' type='button' style='cursor:pointer;' value='Submit'></input>");
}

}else{
$('#pnregister-input').html("Please ask your kid to register!!!");
}
//$('#pnregister-input').html(response);
}else{
$('#pnregister-input').html("Please ask your kid to register!!!");
}
});
});

$(document).on('click','#pnselect-student',function(){
var selectedval = $('input[name=pnstudents]:checked').val();
if(selectedval){
var ajaxurl='/index.php/pnaddkid';
data = {'action':'pnaddkid','selstudent': selectedval,'regcode':pnregisterkey};
$.post(ajaxurl,data,function(response){
alert(response);
});
}
});

});
</script>

<?php
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
if(count($result)>0){
//$result = $this->groups;
for($i=0;$i<count($result);$i++){

echo "<div class='col-md-2'><div id='selectedBlock' class='".$result[$i]->slno."'>";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<span style='display:none;' id='".$result[$i]->slno."'>x</span>";
}
echo "<div id='selectedBlock1' class='".$result[$i]->slno."'><img src='".stripslashes(urldecode($result[$i]->group_icon))."' width='100px' height='100px'><div class='navygrpname'>".stripslashes(urldecode($result[$i]->group_name))."</div><div class='navysubname' data-subkey='".$result[$i]->subkey."'>".stripslashes(urldecode($result[$i]->clasubname))."</div></div></div></div>";

}
}//else{
//echo "s";
//}
echo "<div id='cblock'>+</div>";
}
if(Session::has('privilege') && Session::get('privilege')=="parent"){
if(count($result)>0){
for($a=0;$a<count($result);$a++){

$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if (strpos($entry,$result[$a]->stu_username) !== false) {
$img=$entry;
}
        }
    }
    closedir($handle);
}


echo "<div class='selectedBlock2' id='".$result[$a]->slno."'>";
echo "<div id='selectedBlock3' class='".$result[$a]->slno."'><img src='/images/profile/".$img."' width='100px' height='100px'><div class='navygrpname' id='".stripslashes(urldecode($result[$a]->stu_username))."'>".stripslashes(urldecode($result[$a]->fullname))."</div></div></div>";

}
}
echo "<div id='caddstu'>+</div>";
?>
<div>
<style scoped>
#pnregister-whole {
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    z-index: 10;
    background-color: rgba(0, 0, 0, 0.5);
}
#pnregister-Form {
text-align:left;
    border: 1px solid white;
    background-color: rgb(255, 255, 255);
    margin-top: 100px;
    margin-right: auto;
    margin-left: auto;
    width: 700px;
    border-radius: 4px 4px 4px 4px;
    animation: 2s ease 0s normal none 1 fadein;
}
#pnregister-head {
    border-bottom: 1px solid rgb(128, 128, 128);
    line-height: 3;
    padding-left: 30px;
    font-size: 18px;
    color: rgb(44, 62, 80);
}
#pnregister-head span {
    font-size: 12px;
    float: right;
    line-height: 4;
    padding-right: 25px;
    padding-left: 25px;
    border-left: 1px solid rgb(128, 128, 128);
    cursor: pointer;
}
#pnregister-input {
    padding-left: 20px;
    font-size: 12px;
}
#pnregister-input input {
    line-height: 2;
    margin: 10px 4px 8px 10px;
    background-repeat: repeat-x;
    border: 1px solid rgb(209, 199, 172);
    height: 30px;
    width: 230px;
    color: rgb(51, 51, 51);
    padding: 3px;
    font-family: tahoma,arial,sans-serif;
    font-size: 13px;
}
#pnregister-input input {
    line-height: 2;
    margin: 10px 4px 8px 10px;
    background-repeat: repeat-x;
    border: 1px solid rgb(209, 199, 172);
    height: 30px;
    width: 230px;
    color: rgb(51, 51, 51);
    padding: 3px;
    font-family: tahoma,arial,sans-serif;
    font-size: 13px;
}
</style>
<div id="pnregister-whole" style="display: none;"><div id="pnregister-Form"><div id="pnregister-head">
      Register New Student
    <span>
      X
    </span></div><div id="pnregister-input">
      
Key: 
    <input id="pnreg-key" type="text" required value="" name="username"></input><br></br><input id="pnreg-submitFile" type="button" style="cursor:pointer;" value="Submit"></input></div></div></div>
</div>
<?php
}
?>
<script>
$(document).ready(function(){
$('.selectedBlock2').click(function(){
window.location.href= '/index.php/parent/group/'+$(this).attr('id');
});
});
</script>
