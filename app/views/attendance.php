<style>
#grp_att_head {
border:0px solid black;
overflow:hidden;
height:30px;
font-weight:bold;
}
.atthead_sub{
height:30px;
float:left;
width:250px;
border:0px solid black;
}
#grp_attvalue {
height:60px;
overflow:hidden;
font-size:14px;
background-color:#E1E1E1;
border-bottom:1px solid #fff;
padding-left:10px;
}
#grp_attname{
float:left;
height:60px;
width:200px;
font-weight:bold;
line-height:3;
}
.grp_attstatusclass{
float:left;
height:60px;
line-height:3;
font-weight:bold;
}
#att_mark_single{
border:0px solid black;
float:right;
height:60px;
padding-left:10px;
overflow:hidden;
line-height:3;
}
#att_mark_single > div {
display:block;
border:1px solid #FFFFFF;
cursor:pointer;
width:40px;
text-align:center;
padding:5px;
margin:10px;
float:right;
line-height:1;
}
#gp_photo{
-webkit-border-radius: 20px;
-moz-border-radius: 20px;
border-radius: 20px;
width:40px;
height:40px;
margin-top:10px;
background-color:white;
float:left;
margin-right:10px;
}
/*#attendance_save{
cursor:pointer;
margin-top:20px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
border:1px solid black;
width:60px;
line-height:1.8;
text-align:center;
font-size:14px;
}*/
</style>
<?php
echo HTML::style('css/Attendance.css');
echo HTML::script('js/Attendance.js');
$out="
<div id='atcontent' class='col-lg-12'>
<div id='at_headtext' class='col-lg-12'>Attendance for the week of <span></span></div>
<div id='at_changeweek' class='col-lg-12'>
<div class='btn-group pull-right' role='group' aria-label='...'>
  <button type='button' class='btn btn-default' id='at_left'><</button>
  <button type='button' class='btn btn-default' id='at_weekcount'>This week</button>
  <button type='button' class='btn btn-default' id='at_right'>></button>
</div>

<!--div id='at_right'>></div><div id='at_weekcount' style='cursor:pointer;'>This week</div><div id='at_left'><</div--></div>
<div id='at_thisweekdates' class='col-lg-12'>

<div id='at_datedisp' class='col-lg-12'>";
for($i=0;$i<7;$i++){
$sun = date('M d',strtotime('last sunday +'.($i+$count).' days'));
$out=$out."<div id='at_day'>".$sun."<br><span>".date('l', strtotime( $sun ))."</span></div>";
}
$out=$out."</div>
<div id='at_attendisp' class='col-lg-12'>";
for($i=0;$i<7;$i++){
$suny = date('d/m/Y',strtotime('last sunday +'.($i+$count).' days'));
$out=$out."<div id='at_atten'><div class='AM_".$suny."' id='at_fn'>F N</div><div class='PM_".$suny."' id='at_an'>A N</div></div>";
}
echo $out."</div>

</div>

<div id='at_group_students' style=''>
Date: <span id='atten-dater'>".date('d/m/Y')."</span>-<span id='atten-noon'>".date('A')."</span><br>";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){

echo "
<div class='btn-group pull-right' role='group' aria-label='...'>
<p>Mark All:</p>
  <button type='button' class='btn btn-default' id='markallpresent'>Present</button>
  <button type='button' class='btn btn-default' id='markallabsent'>Absent</button>
</div>
";
echo "<!--div id='att_mark' style='overflow:hidden;float:right;'>Mark all: <div style='border:1px solid black;padding:3px;float:right;cursor:pointer;' id='markallabsent'>Absent</div><div id='markallpresent' style='float:right;border:1px solid black;padding:3px;cursor:pointer;'>Present</div></div!-->";
}
echo "<br><br><div id='grp_att_head'><div class='atthead_sub'>Name</div><div class='atthead_sub'>Attendance</div></div>
";
function profpictureload($usr){
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if($usr!=""){
if (strpos(strtolower($entry),strtolower($usr)) !== false) {
$img=$entry;
}
}
        }
    }
    closedir($handle);
}
return "/images/profile/".$img;
}

for($i=0;$i<count($attdet);$i++){
if($attdet[$i][1]=='2'){
$status="Not Marked";
}elseif($attdet[$i][1]=='1'){
$status="Present";
}elseif($attdet[$i][1]=='0'){
$status="Absent";
}
echo "<div id='grp_attvalue'><div id='gp_photo'><img class='img-circle' src='".profpictureload($attdet[$i][0])."' width='40px' height='40px'></div><div id='grp_attname'>".$attdet[$i][0]."</div><div id='atten_".$attdet[$i][0]."' class='grp_attstatusclass' data-atvalue='".$attdet[$i][1]."'>".$status."</div>";
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "<div id='att_mark_single'>Mark: <div id='marksingleabsent' class='".$attdet[$i][0]."'>A</div> <div id='marksinglepresent' class='".$attdet[$i][0]."'>P</div></div>";
}
echo "</div>";
}
//$attdet;
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
echo "
<button class='btn btn-primary' id='attendance_save' >Save</button>
<!--div id='attendance_save'>Save</div-->";
}
echo "</div>
</div>";
?>
