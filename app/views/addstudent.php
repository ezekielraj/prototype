<div id='addstudentcontent'>
<?php echo HTML::style('css/addstudent.css'); ?>
<?php echo HTML::script('js/addstudent.js'); ?>
<div id='aswhole'>
<div class='container'>
<div id='asForm' class='col-lg-offset-2 col-lg-8'>
<div id='ashead' style='display:none;'>Select Students<span>X</span></div>
<div id='asfother' style='display:none;'>
<label for='selectbox-value'>Class:</label>
<div id='selectbox' class='btn-group classy' role='group'>
<button type="button" id='selectbox-value' class="btn btn-default dropdown-toggle classy-value" data-classy='all' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Class</button>
<ul id='select-classy' class="dropdown-menu">
      <li><a href="#"><div id='all'>All</div></a></li>
<?php
for($i=0;$i<count($result);$i++){
  echo "<li><a href='#'><div id='".$result[$i]->clasubkey."'>".$result[$i]->clasubname."</div></a></li>";
}
?>
    </ul>
</div>
<label for='selectbox-value'>Section:</label>
<div id='selectbox' class='btn-group sectiony' role='group'>
<button type="button" id='selectbox-value' class="btn btn-default dropdown-toggle sectiony-value" data-sectiony='all' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Section</button>
<ul id='select-sectiony' class='dropdown-menu'>

</ul>
</div>

<label for='selectbox-value'>Subject:</label>
<div id='selectbox' class='btn-group subjecty' role='group'>
<button type="button" id='selectbox-value' class="btn btn-default dropdown-toggle subjecty-value" data-subjecty='all' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Subject</button>
<ul id='select-subjecty' class='dropdown-menu'>

</ul>
</div>

<div id='studentslist'>
<link rel='stylesheet' href='css/floader.css'></link>
<div class='spinner'>
</div>
</div>
<button type="button" id='goimage' class='btn btn-success'>Create</button>
<!--div id='allGo'><div id='goimage'></div></div-->
<div id='smessage'></div>
</div>
</div>
</div>
</div>
</div>

