<div id='loadprofileformcontent'>
<?php echo HTML::style('css/loadprofileform.css'); ?>
<?php echo HTML::script('js/loadprofileform.js'); ?>

<?php
$name=explode(" ",Session::get('fullname'));
$firstname=$name[0];
$lastname="";
for($i=1;$i<count($name);$i++){
$lastname=$lastname." ".$name[$i];
}
?>
<div id='lpfwhole'>
<div id='lpfForm' class='col-lg-offset-2 col-lg-8'>
<div id='lpfhead' style='display:none;'>Basic Profile<span>X</span></div>
<div id='lpfinput' style='display:none;'>
<?php
$img="";
if ($handle = opendir($_SERVER["DOCUMENT_ROOT"].'/images/profile/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
if (strpos($entry,Session::get('username')) !== false) {
$img=$entry;
}
        }
    }
    closedir($handle);
}

if(Session::has('privilege') && Session::get('privilege')=="teacher"){
?>
<script>
$(document).ready(function(){
$('.lpfedit').css('display','none');
$('.lpfresetpassword').css('display','none');
$('#lpfnav li').click(function(){
var id=$(this).attr('id');
$('#lpfbasicprofile').removeClass('active');
$('#lpfedit').removeClass('active');
$('#lpfresetpassword').removeClass('active');
$(this).addClass('active');

$('.lpfbasicprofile').css('display','none');
$('.lpfedit').css('display','none');
$('.lpfresetpassword').css('display','none');
$('.'+id).css('display','block');

});
});
</script>
<div class='col-lg-12' style='padding-top:10px;padding-bottom:10px;'>
<ul id='lpfnav' class="nav nav-tabs">
  <li role="presentation" id='lpfbasicprofile' class="active"><a href="#">Profile</a></li>
  <li role="presentation" id='lpfedit'><a href="#">Edit</a></li>
  <li role="presentation" id='lpfresetpassword'><a href="#">Reset Password</a></li>
</ul>
</div>
<div>

<div class='lpfbasicprofile'>

<div id='teach-picandname' class='col-lg-12'>
<div id='teach-profpic' class='col-lg-3'><img class='img-responsive' src='/images/profile/<?php echo $img; ?>' width='150px'/></div>
<div id='teach-name' class='col-lg-9'>

<p> Teacher Name: <?php echo Session::get('fullname'); ?></p>
<p> Teacher Subjects: 
<?php
$subjects=$result['subject'];
for($i=0;$i<count($subjects);$i++){
echo $subjects[$i]->teacher_subject.", ";
}
?>
</p>
</div>
</div>
<div id='teach-classhand' class='col-lg-12'>
<h4>Classes Handling</h4>
<ul>
<?php
$classes=$result['class'];
for($j=0;$j<count($classes);$j++){
echo "<li>".$classes[$j]['class']." ".$classes[$j]['section']."</li>";
}
?>
</ul>
</div>
<div id='teach-contactinfo' class='col-lg-12'>
<h4>Contact Info</h4>
<ul>
<li>Email: <?php echo Session::get('emailid'); ?></li>
<li>Phone: </li>
</ul>
</div>
</div>



<div class='lpfedit'>
<div class='col-lg-6'>
<div class='form-group'>
<label for='prof_username'>Username: </label>
<input type='text' class='form-control' id='prof_username' name='username' value='<?php echo Session::get('username'); ?>' required>
</div>
<div class='form-group'>
<label for='prof_firstname'>First Name: </label><input type='text' class='form-control' id='prof_firstname' name='firstname' value='<?php echo $firstname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_lastname'>Last Name: </label><input type='text' id='prof_lastname' class='form-control' name='lastname' value='<?php echo $lastname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_emailid'>Email Address: </label><input type='text' id='prof_emailid' class='form-control' name='emailid' value='<?php echo Session::get('emailid'); ?>' required>
</div>
<?php
//echo HTML::script('js/loadprofileform-teacher.js');
?>
<!--Class: <select id='teacher-standard'>
<option value=''>Select Class</option>
</select><br>
Section: <select id='teacher-section'>
<option value=''>Select Section</option>
</select><br-->
<div class='form-group'>
<label for='profpicture'>Profile Picture:</label> <input type="file" id="profpicture" />
</div>

     <input type="button" class='btn btn-default' id="submitFile" value="Submit" style='cursor:pointer;'/>
</div>
</div>

<div class='lpfresetpassword'>
<script>
$(document).ready(function(){
$('#retypenewpassword').keyup(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
$('#pwdmsg').html("Passwords Match");
}else{
$('#pwdmsg').html("Passwords Do not Match");
}
});
$('#resetpassword').click(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
var ajaxurl='/index.php/resetpassword';
data={'action':'resetpassword','newpassword':$('#newpassword').val(),'oldpassword':$('#currentpassword').val(),'rnewpassword':$('#retypenewpassword').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#passwdmsg').html(response);
}
});
}
});
});
</script>
<div class='col-lg-6'>
<div class='form-group'>
<label>Current Password</label><input id='currentpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>New Password</label><input id='newpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>Retype New Password</label><input id='retypenewpassword' class='form-control' type='password' />
<div id='pwdmsg'></div>
</div>
<div id='passwdmsg' class='form-group'>
</div>
<input type='button' id='resetpassword' class='btn btn-default' value='Submit'/>
</div>
</div>
<?php
}elseif(Session::has('privilege') && Session::get('privilege')=="student"){
?>
<script>
$(document).ready(function(){
$('.lpfedit').css('display','none');
$('.lpfresetpassword').css('display','none');
$('#lpfnav li').click(function(){
var id=$(this).attr('id');
$('#lpfbasicprofile').removeClass('active');
$('#lpfedit').removeClass('active');
$('#lpfresetpassword').removeClass('active');
$(this).addClass('active');

$('.lpfbasicprofile').css('display','none');
$('.lpfedit').css('display','none');
$('.lpfresetpassword').css('display','none');
$('.'+id).css('display','block');

});
});
</script>
<div class='col-lg-12' style='padding-top:10px;padding-bottom:10px;'>
<ul id='lpfnav' class="nav nav-tabs">
  <li role="presentation" id='lpfbasicprofile' class="active"><a href="#">Profile</a></li>
  <li role="presentation" id='lpfedit'><a href="#">Edit</a></li>
  <li role="presentation" id='lpfresetpassword'><a href="#">Reset Password</a></li>
</ul>
</div>


<div>

<div class='lpfbasicprofile'>
<div class='col-lg-12' style='padding-top:10px;'>
<div class='col-lg-4'>
<img class='img img-responsive' src='/images/profile/<?php echo $img; ?>'/>
</div>
<div class='col-lg-8'>
<h4>Student Name: <?php echo Session::get('fullname'); ?> </h4>
<p>Class: <?php echo $result['class'][0]->clasubname; ?></p>
</div>
</div>


<div class='col-lg-10'>
<h3>Exam Marks</h3>
<table class='table table-bordered'>
<thead>
<tr>
<th>Exam Name</th>
<th>Subjects/Marks</th>
</tr>
</thead>
<tbody>
<?php
foreach($result['report'] as $roww){
echo "<tr><td>".$roww['name']."</td><td>";
$reports=$roww['result'];
?>
<table class='table table-bordered'>
<thead>
<tr>
<th>Subjects</th><th>Marks</th>
</tr>
</thead>
<tbody>
<?php
for($i=0;$i<count($reports);$i++){
echo "<tr>";
$sub = explode('_',$reports[$i]->stu_subjectname);
echo "<td>".$sub[1]."</td>";
echo "<td>".$reports[$i]->stu_mark."</td>";
echo "</tr>";
}
?>
</tbody>
</table>
<?php
echo "</td></tr>";
}
?>
</tbody>
</table>
</div>

<!--div id='stu-examresults'>
<h3>Exam Marks</h3>
<table>
<tr><td>Exam Name</td><td></td></tr>
<?php
foreach($result['report'] as $roww){
echo "<tr border='1'><td>".$roww['name']."</td>";
echo "<td>";
echo "<div style='overflow:hidden;'>";
$reports = $roww['result'];
for($i=0;$i<count($reports);$i++){
echo "<div style='border:1px solid black;float:left; width:50px;'>";
$sub = explode('_',$reports[$i]->stu_subjectname);
echo $sub[1]."<br>";
echo $reports[$i]->stu_mark;
echo "</div>";
}
echo "</div>";
echo "</td></tr>";
}
?>
</table>
</div-->


<div class='col-lg-10'>
<h3>Attendance</h3>
<table class='table table-bordered'>
<thead>
<tr>
<th>Total</th>
<th>Present</th>
<th>Absent</th>
</tr>
</thead>
<tbody>
<?php
$att=$result['attendance'];
echo "<tr><td>".$att['all']."</td><td>".$att['pre']."</td><td>".$att['abs']."</td></tr>";
?>
</tbody>
</table>
</div>

<!--div id='stu-attendance'>
<h3>Attendance</h3>
<table>
<tr><td>Total</td><td>Present</td><td>Absent</td></tr>
<?php 
$att=$result['attendance'];
echo "<tr><td>".$att['all']."</td><td>".$att['pre']."</td><td>".$att['abs']."</td></tr>";
?>
</table>
</div-->

<div class='col-lg-10'>
<h3>Teachers' Comments</h3>
</div>

<div id='stu-contactinfo' class='col-lg-10'>
<h3>Contact Info</h3>
<ul>
<li>Email: <?php echo Session::get('emailid'); ?></li>

<li>Parent Name: <?php if(isset($result['parentdetails'][0]->par_fullname) && $result['parentdetails'][0]->par_fullname!=""){ echo stripslashes(urldecode($result['parentdetails'][0]->par_fullname)); } ?></li>
<li>Parent Contact: </li>
</ul>
</div>


</div>
</div>

<div class='lpfedit'>
<div class='col-lg-6'>
<div class='form-group'>
<label for='prof_username'>Username: </label>
<input type='text' class='form-control' id='prof_username' name='username' value='<?php echo Session::get('username'); ?>' required>
</div>
<div class='form-group'>
<label for='prof_firstname'>First Name: </label><input type='text' class='form-control' id='prof_firstname' name='firstname' value='<?php echo $firstname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_lastname'>Last Name: </label><input type='text' id='prof_lastname' class='form-control' name='lastname' value='<?php echo $lastname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_emailid'>Email Address: </label><input type='text' id='prof_emailid' class='form-control' name='emailid' value='<?php echo Session::get('emailid'); ?>' required>
</div>
<?php
//echo HTML::script('js/loadprofileform-teacher.js');
?>
<!--Class: <select id='teacher-standard'>
<option value=''>Select Class</option>
</select><br>
Section: <select id='teacher-section'>
<option value=''>Select Section</option>
</select><br-->
<div class='form-group'>
<label for='profpicture'>Profile Picture:</label> <input type="file" id="profpicture" />
</div>

     <input type="button" class='btn btn-default' id="submitFile" value="Submit" style='cursor:pointer;'/>
</div>
</div>

<div class='lpfresetpassword'>

<script>
$(document).ready(function(){
$('#retypenewpassword').keyup(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
$('#pwdmsg').html("Passwords Match");
}else{
$('#pwdmsg').html("Passwords Do not Match");
}
});
$('#resetpassword').click(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
var ajaxurl='/index.php/resetpassword';
data={'action':'resetpassword','newpassword':$('#newpassword').val(),'oldpassword':$('#currentpassword').val(),'rnewpassword':$('#retypenewpassword').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#passwdmsg').html(response);
}
});
}
});
});
</script>
<div class='col-lg-6'>
<div class='form-group'>
<label>Current Password</label><input id='currentpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>New Password</label><input id='newpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>Retype New Password</label><input id='retypenewpassword' class='form-control' type='password' />
<div id='pwdmsg'></div>
</div>
<div id='passwdmsg' class='form-group'>
</div>
<input type='button' id='resetpassword' class='btn btn-default' value='Submit'/>
</div>

</div>
<?php
}else{
?>
<script>
$(document).ready(function(){
$('.lpfresetpassword').css('display','none');
$('#lpfnav li').click(function(){
var id=$(this).attr('id');
$('#lpfedit').removeClass('active');
$('#lpfresetpassword').removeClass('active');
$(this).addClass('active');

$('.lpfedit').css('display','none');
$('.lpfresetpassword').css('display','none');
$('.'+id).css('display','block');

});
});
</script>
<div class='col-lg-12' style='padding-top:10px;padding-bottom:10px;'>
<ul id='lpfnav' class="nav nav-tabs">
  <li role="presentation" id='lpfedit' class='active'><a href="#">Edit</a></li>
  <li role="presentation" id='lpfresetpassword'><a href="#">Reset Password</a></li>
</ul>
</div>


<div class='lpfedit'>
<div class='col-lg-6'>
<div class='form-group'>
<label for='prof_username'>Username: </label>
<input type='text' class='form-control' id='prof_username' name='username' value='<?php echo Session::get('username'); ?>' required>
</div>
<div class='form-group'>
<label for='prof_firstname'>First Name: </label><input type='text' class='form-control' id='prof_firstname' name='firstname' value='<?php echo $firstname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_lastname'>Last Name: </label><input type='text' id='prof_lastname' class='form-control' name='lastname' value='<?php echo $lastname; ?>' required>
</div>
<div class='form-group'>
<label for='prof_emailid'>Email Address: </label><input type='text' id='prof_emailid' class='form-control' name='emailid' value='<?php echo Session::get('emailid'); ?>' required>
</div>
<div class='form-group'>
<label for='profpicture'>Profile Picture:</label> <input type="file" id="profpicture" />
</div>

     <input type="button" class='btn btn-default' id="submitFile" value="Submit" style='cursor:pointer;'/>
</div>
</div>

<div class='lpfresetpassword'>

<script>
$(document).ready(function(){
$('#retypenewpassword').keyup(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
$('#pwdmsg').html("Passwords Match");
}else{
$('#pwdmsg').html("Passwords Do not Match");
}
});
$('#resetpassword').click(function(){
if($('#newpassword').val().toString() === $('#retypenewpassword').val().toString()){
var ajaxurl='/index.php/resetpassword';
data={'action':'resetpassword','newpassword':$('#newpassword').val(),'oldpassword':$('#currentpassword').val(),'rnewpassword':$('#retypenewpassword').val()};
$.post(ajaxurl,data,function(response){
if(response){
$('#passwdmsg').html(response);
}
});
}
});
});
</script>

<div class='col-lg-6'>
<div class='form-group'>
<label>Current Password</label><input id='currentpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>New Password</label><input id='newpassword' class='form-control' type='password' />
</div>

<div class='form-group'>
<label>Retype New Password</label><input id='retypenewpassword' class='form-control' type='password' />
<div id='pwdmsg'></div>
</div>
<div id='passwdmsg' class='form-group'>
</div>
<input type='button' id='resetpassword' class='btn btn-default' value='Submit'/>
</div>

</div>
<?php
}
?>
</div>

</div>
</div>
</div>
