<style>
.colxs-centered{
float:none;
margin: 0 auto;
}
.loginoption {
border:0px solid black;
height:370px;
padding-top:20px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
margin:45px;
}

#head {
text-align: center;
font-size:18px;
font-weight:bold;
color:#FFF;
}

#head label {
margin-top:auto;
margin-bottom: auto;
height:100%;
}

</style>
<script>
$(document).ready(function(){
$('.loginoption').mouseenter(function(){
$(this).children('div#login-form')
  .transition('slide up')
;
$(this).css('background-image',"");
});
$('.loginoption').mouseleave(function(){
$(this).children('div#login-form')
  .transition('slide down')
;
});




$('.loginoption:not(.loginoption-inner)').click(function(){
$('.loginoption').not(this).dimmer('show');
$(this).children('div#login-form').removeAttr('id');
$(this).find('div.loginoption-inner').css('visibility','visible');
$('.loginoption').not(this).switchClass("col-lg-3","col-lg-2",300);
$(this).switchClass("col-lg-3","col-lg-5",300);
});

/*
$(document).on('click','.loginoption-inner',function(){
var idd = $(this).attr('id');
$('#loginoption-'+idd).children('div').attr("id","login-form");
$(this).css('visibility','hidden');
$('.loginoption').css('height','370px');
$('.loginoption').css('margin','45px');
$('#loginoption-'+idd).children('div').transition('slide down');
$('#loginoption-'+idd).children('div').attr("id","login-form");
});
*/
});
</script>
<div class="col-lg-12 colxs-centered">
<center><h3>Choose A Login</h3>
<!--form class="form-inline">
<div class="form-group">
<select id='loginoption' class="form-control">
<option value='student'>Student Login</option>
<option value='teacher'>Teacher Login</option>
<option value='parent'>Parent Login</option>
</select>
</div>
<div class="form-group">
<input id='go-button' class="btn btn-default" type='button' Value='Go'>
</div>
</form-->
</center>
<div class='col-lg-12'>
<div class="col-lg-3 loginoption" style="background-color:#FF3C3C;" id='loginoption-student'>


<div id='login-form' style='display:none;'>
<div class="ui red basic icon button loginoption-inner" id='student' style='visibility:hidden;'><i class='remove icon'></i></div>
<div class="col-lg-12 colxs-centered" style='color:#FFF;'>
<div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Students Login | <span id='new-registe'>Register</span></label></div>
<br>
<div class='whoselogin' style='display:none;'>student</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='user' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<div class="form-group">
<label for="pass">Password</label>
<input id='pass' type='password' class="form-control" placeholder='Password'/>
</div>
<center><button type="button" class="btn btn-default" id='login'>Login</button></center>
<div id='Message'></div>
<div id='new-register' style='float:right;'>Register</div>
</div>
</div>
<div id='register-whole' style='display:none;'>

<div id='register-Form' class="col-lg-4 colxs-centered">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='reg-key' name='username' value='' required>
<br>
<button class="btn btn-default" type="button" id="reg-submitFile" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>
</div>
</div>
</div>



</div>
<div class="col-lg-3 loginoption" style='background-color:#4CDC72;' id='loginoption-teacher'>

<div id='login-form' style='display:none;'>
<div class="ui red basic icon button right loginoption-inner" id='teacher' style='visibility:hidden;'><i class='remove icon'></i></div>
<div class="col-lg-12 colxs-centered" style='color:#FFF;'>
<div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Teachers Login | <span id='new-registe'>Register</span></label></div>
<br>
<div class='whoselogin' style='display:none;'>teacher</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='user' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<div class="form-group">
<label for="pass">Password</label>
<input id='pass' type='password' class="form-control" placeholder='Password'/>
</div>
<center><button type="button" class="btn btn-default" id='login'>Login</button></center>
<div id='Message'></div>
<div id='new-register' style='float:right;'>Register</div>
</div>
</div>
<div id='register-whole' style='display:none;'>

<div id='register-Form' class="col-lg-4 colxs-centered">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='reg-key' name='username' value='' required>
<br>
<button class="btn btn-default" type="button" id="reg-submitFile" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>
</div>
</div>
</div>

</div>
<div class="col-lg-3 loginoption" style='background-color:#1678C2' id='loginoption-parent'>

<div id='login-form' style='display:none;'>
<div class="ui red basic icon button right loginoption-inner" id='parent' style='visibility:hidden;'><i class='remove icon'></i></div>
<div class="col-lg-12 colxs-centered" style='color:#FFF;'>
<div id='head'><img src='iconns/lock.png' width='50px' height='50px'><label>Parents Login | <span id='new-registe'>Register</span></label></div>
<br>
<div class='whoselogin' style='display:none;'>parent</div>
<div id='loginform'>
<div class="colxs-centered">
<div class="form-group">
<label for="user">Username or Email Address</label>
<input id='user' type='text' class="form-control" placeholder='Username or Email Address'/>
</div>
<div class="form-group">
<label for="pass">Password</label>
<input id='pass' type='password' class="form-control" placeholder='Password'/>
</div>
<center><button type="button" class="btn btn-default" id='login'>Login</button></center>
<div id='Message'></div>
<div id='new-register' style='float:right;'>Register</div>
</div>
</div>
<div id='register-whole' style='display:none;'>

<div id='register-Form' class="col-lg-4 colxs-centered">
<div id='register-head'>Register<span>X</span></div>
<div id='register-input' class="form-group">
<br>
<label for="reg-key">Key: </label>
<input type='text' class="form-control" id='reg-key' name='username' value='' required>
<br>
<button class="btn btn-default" type="button" id="reg-submitFile" value="Submit" style='cursor:pointer;'>Submit</button>
</div>
</div>
</div>
</div>
</div>

</div>
</div>


</div>
