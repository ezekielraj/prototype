<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//admin routes
Route::get('/admin', 'adminController@adminhome');
Route::get('/admin/codegenerator', 'adminController@codegenerator');
Route::get('/admin/permissions', 'adminController@permissions');
Route::get('/admin/teacherkey', 'adminController@teacherkey');
//Route::post('/admin/gencode', 'adminController@gencode');
Route::post('/admin/gensection', 'adminController@gensection');
Route::post('/admin/createsection', 'adminController@createsection');
Route::post('/admin/deletesection', 'adminController@deletesection');
Route::post('/admin/gettkey', 'adminController@gettkey');
Route::post('/admin/clearkey', 'adminController@clearkey');
Route::post('/admin/getuser', 'adminController@getuser');
Route::post('/admin/getuser1', 'adminController@getuser1');
Route::post('/admin/addedituser', 'adminController@addedituser');
Route::get('/admin/people', 'adminController@people');
Route::get('/admin/addentries', 'adminController@addentries');
Route::post('/admin/addclass', 'adminController@addclass');
Route::post('/admin/addsubject', 'adminController@addsubject');
Route::post('/admin/addexam', 'adminController@addexam');
Route::post('/admin/acsubmit', 'adminController@acsubmit');
Route::post('/admin/assubmit', 'adminController@assubmit');
Route::post('/admin/aesubmit', 'adminController@aesubmit');
Route::post('/admin/removeclass', 'adminController@removeclass');
Route::post('/admin/genteachkey', 'adminController@genteachkey');
Route::post('/admin/udeactivate', 'adminController@udeactivate');
Route::post('/admin/uactivate', 'adminController@uactivate');
Route::post('/admin/createuser', 'adminController@createuser');
Route::post('/admin/checkkey', 'adminController@checkkey');

Route::get('/', 'HomeController@showLogin');
Route::post('/index','HomeController@showchooselogin');
Route::post('/login', 'HomeController@valLogin');
Route::post('/login/register', 'HomeController@register');
Route::post('/login/fregister', 'HomeController@fregister');
Route::post('/logout', 'HomeController@Logout');
Route::get('/classes', 'classController@classes');

Route::post('/loadtmc', 'classController@loadtmc');
Route::post('/loadprofileform', 'classController@loadprofileform');
Route::post('/checkprofile', 'classController@checkprofile');
Route::post('/profileupdate', 'classController@profileupdate');
Route::post('/addstudent', 'classController@addstudent');
Route::post('/grpdelete', 'classController@grpdelete');
Route::post('/checkStudentfp', 'classController@checkStudentfp');
Route::post('/updateKidfp', 'classController@updateKidfp');
Route::post('/pnaddkid', 'classController@pnaddkid');
Route::post('/pngetstudents', 'classController@pngetstudents');
Route::post('/loadstd', 'classController@loadstd');
Route::post('/resetpassword', 'classController@resetpassword');
//addstudents.js
Route::post('/fixall', 'classController@fixall');
Route::post('/fixsection', 'classController@fixsection');
Route::post('/fixsubject', 'classController@fixsubject');
Route::post('/groupupdate', 'classController@groupupdate');
Route::post('/getallsection', 'classController@getallsection');

Route::get('/class/group/{groupname}/{subname}','groupController@students');
	Route::post('/class/group/awardupdate','groupController@awardupdate');
	Route::post('/class/group/secgetstudents','groupController@secgetstudents');
	Route::post('/class/group/secgroupupdate','groupController@secgroupupdate');
	Route::post('/class/group/removestudent','groupController@removestudent');
Route::get('/class/classfeed/{groupname}/{subname}','classfeedController@classfeed');
	Route::post('/class/classfeed/loadsharedcontent','classfeedController@loadsharedcontent');
	Route::post('/class/classfeed/loadannouncecontent','classfeedController@loadannouncecontent');
	Route::post('/class/classfeed/feedupdate','classfeedController@feedupdate');
	Route::post('/class/classfeed/updatelike','classfeedController@updatelike');
	Route::post('/class/classfeed/secthank','classfeedController@secthank');
	Route::post('/class/classfeed/secdelete','classfeedController@secdelete');
	Route::post('/class/classfeed/updatecomment','classfeedController@updatecomment');
	Route::post('/class/classfeed/deletefeed','classfeedController@deletefeed');
	Route::post('/class/classfeed/loadcomments','classfeedController@loadcomment');
	Route::post('/class/classfeed/geturl','classfeedController@geturl');
	Route::get('/class/attach/classfeed/gcfattach/{fname}','classfeedController@gcfattach');

Route::get('/class/attendance/{groupname}/{subname}','attendanceController@attendance');
	Route::post('/class/attendance/loaddays','attendanceController@loaddays');
	Route::post('/class/attendance/loadweek','attendanceController@loadweek');
	Route::post('/class/attendance/updateattendance','attendanceController@updateattendance');
	Route::post('/class/attendance/getattendancevalue','attendanceController@getattendancevalue');
	
Route::get('/class/assignments/{groupname}/{subname}','assignmentsController@assignments');
	Route::post('/class/assignments/createassignment','assignmentsController@createassignment');	
	Route::post('/class/assignments/getassignments','assignmentsController@getassignments');
	Route::post('/class/assignments/delassignment','assignmentsController@delassignment');
	Route::post('/class/assignments/updateassignment','assignmentsController@updateassignment');
	Route::post('/class/assignments/assignmark','assignmentsController@assignmark');
	Route::post('/class/assignments/getassignmarks','assignmentsController@getassignmarks');
	Route::get('/class/assignment/gattach/{fname}','assignmentsController@gattach');
Route::get('/class/reports/{groupname}/{subname}','reportsController@reports');
	Route::post('/class/reports/loadreport','reportsController@loadreport');
	Route::post('/class/reports/loadswreport','reportsController@loadswreport');
	Route::post('/class/reports/loadaddedit','reportsController@loadaddedit');
	Route::post('/class/reports/markupdate','reportsController@markupdate');
	Route::post('/class/reports/outofmarkupdate','reportsController@outofmarkupdate');
	Route::post('/class/reports/getstureport','reportsController@getstureport');
	Route::post('/class/reports/calrank','reportsController@calrank');
Route::get('/class/announcements/{groupname}/{subname}','announcementsController@announcements');

Route::get('/class/calender/{groupname}/{subname}','calenderController@calender');
	Route::post('/class/calender/getevents','calenderController@getevents');
	Route::post('/class/calender/createevents','calenderController@createevents');
	Route::post('/class/calender/getdates','calenderController@getdates');


//For students
Route::get('/student','studentController@index');
	Route::post('/student/getawards','studentController@getawards');
	Route::post('/student/getprofile','studentController@getprofile');
Route::get('/student/classfeed','studentController@classfeed');
Route::get('/student/attendance','studentController@attendance');
Route::get('/student/assignments','studentController@assignments');
Route::get('/student/reports','studentController@reports');
//For parents
Route::get('/parent','parentController@index');
Route::get('/parent/group/{sid}','parentController@stugrp');
Route::get('/parent/classfeed/{sid}','parentController@classfeed');
Route::get('/parent/attendance/{sid}','parentController@attendance');
Route::get('/parent/assignments/{sid}','parentController@assignments');
Route::get('/parent/reports/{sid}','parentController@reports');
