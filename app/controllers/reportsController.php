<?php
class reportsController extends BaseController {
public $layout = 'layouts.wrapper';



public function reports($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$exams = with(new Reports)->getExams();
$this->layout->with('title', 'A - Grade')->nest('content', 'reports',array('exams' => $exams));
}

public function loadreport(){
$this->layout = null;
if(Input::has('action') && Input::get('action')=="loadReport"){
if(Input::has('classname') && Input::get('classname')!="" && Input::has('examname') && Input::get('examname')!="" && Input::has('subjectname') && Input::get('subjectname')!=""){
if(strtolower(trim(Input::get('subjectname')))=="all"){
$examreport = with(new Reports)->getallResults(Input::get('classname'),Input::get('examname'),strtolower(Input::get('subjectname')),"report");
//print_r($examreport);
if(count($examreport)){
if(substr(Input::get('classname'),0,3)==="All"){
return View::make('loadswreport')->with('act',$examreport)->with('starting',"all");
}else{
return View::make('loadswreport')->with('act',$examreport)->with('starting',"");
}
}else{
return "No Result Entries Made!!!!";
}

}else{
$examreport = with(new Reports)->getResults(Input::get('classname'),Input::get('examname'),strtolower(Input::get('subjectname')),"report");
if(count($examreport)){
if(Input::has('drill') && Input::get('drill')=="1"){
return View::make('loaddrillreport')->with('act',$examreport)->with('subject',Input::get('subjectname'));
}else{
return View::make('loadreport')->with('act',$examreport);
}
}else{
$students = with(new Reports)->getStudents(Input::get('classname'),strtolower(Input::get('subjectname')));
return View::make('loadreport')->with('act','addedit')->with('students',$students);
}
}
}
}else{
return Redirect::to('/classes');
}
}
public function loadswreport(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="loadswReport"){
if(Input::has('classname') && Input::get('classname')!="" && Input::has('examname') && Input::get('examname')!=""){
$examreport = with(new Reports)->getallResults(Input::get('classname'),Input::get('examname'),"all","report");
if(count($examreport)){
return View::make('loadswreport')->with('act',$examreport);
//print_r($examreport);
}else{
return "No Result Entries Made!!!!";
}
}
}
}

public function loadaddedit(){
$this->layout = null;
if(Input::has('action') && Input::get('action')=="loadAddedit"){
if(Input::has('classname') && Input::get('classname')!="" && Input::has('examname') && Input::get('examname')!="" && Input::has('subjectname') && Input::get('subjectname')!=""){
$examreport = with(new Reports)->getResults(Input::get('classname'),Input::get('examname'),Input::get('subjectname'),"addedit");
if(count($examreport)){
return View::make('loadaddedit')->with('students',$examreport);
}else{
return null;
}
}
}else{
return Redirect::to('/classes');
}


}

public function markupdate(){
$this->layout = null;
if(Input::has('action') && Input::get('action')=="markUpdate"){
if(Input::has('studentname') && Input::get('studentname')!="" && Input::has('examname') && Input::get('examname')!="" && Input::has('subjectname') && Input::get('subjectname')!="" && Input::has('mark') && Input::get('mark')!=""){
$markupdate = with(new Reports)->updatemark(Input::get('studentname'),Input::get('examname'),Input::get('subjectname'),trim(Input::get('mark')));
if($markupdate){
return View::make('markupdate')->with('row',"s");
}else{
return View::make('markupdate')->with('row',"no");
}
}
}else{
return Redirect::to('/classes');
}
}

public function outofmarkupdate(){
$this->layout = null;
if(Input::has('action') && Input::get('action')=="outofmarkUpdate"){
if(Input::has('studentname') && Input::get('studentname')!="" && Input::has('examname') && Input::get('examname')!="" && Input::has('subjectname') && Input::get('subjectname')!="" && Input::has('mark') && Input::get('mark')!=""){
$markupdate = with(new Reports)->updateoutofmark(Input::get('studentname'),Input::get('examname'),Input::get('subjectname'),Input::get('mark'));
if($markupdate){
return View::make('markupdate')->with('row',"s");
}else{
return View::make('markupdate')->with('row',"no");
}
}
}else{
return Redirect::to('/classes');
}
}

public function getstureport(){
$this->layout = null;
if(Input::has('action') && Input::get('action')=="getstureport"){
if(Input::has('studentname') && Input::get('studentname')!=""){
$exams = with(new Reports)->getExams();
$getReport = array();
$getSubjects=with(new Reports)->getSubjects(Input::get('studentname'));
$studentname=Input::get('studentname');
for($i=0;$i<count($exams);$i++) {
$getReport[$i]['examdetails'] = $exams[$i];
$getReport[$i]['examresults'] = with(new Reports)->getResultsall($exams[$i]->clasubkey,Input::get('studentname'));
$getReport[$i]['examrank']= with(new Reports)->getrank($exams[$i]->clasubkey,Input::get('studentname'));
}
return View::make('student.reports')->with('examreport',$getReport)->with('subjects',$getSubjects)->with('stuname',$studentname);
}
}
}


public function calrank(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="calrank"){
if(Input::has('grpnum') && Input::get('grpnum')!="" && Input::has('examkey') && Input::get('examkey')!=""){
$result = with(new Students)->getgrpStudents(Input::get('grpnum'),Session::get('username'));
$students=explode(',',$result);
$maxsub=0;
for($i=0;$i<count($students);$i++){
$getSubjects=with(new Reports)->getSubjects($students[$i]);
if(count($getSubjects)>$maxsub){
$maxsub=count($getSubjects);
}
}
if($maxsub>0){
$markperc=array();
$passper=with(new Reports)->getpassper(Input::get('examkey'));
for($i=0;$i<count($students);$i++){
$report = with(new Reports)->getResultsall(Input::get('examkey'),$students[$i]);
$markper[$students[$i]]=0;
for($j=0;$j<count($report);$j++){
$per=((($report[$j]->stu_mark)/($report[$j]->stu_outofmark))*100);
$markper[$students[$i]]=$markper[$students[$i]]+$per;
}
$insrank=with(new Reports)->updaterank(Input::get('examkey'),$students[$i],($markper[$students[$i]]/$maxsub));
if($markper[$students[$i]]>=$passper[0]->exam_passmark){
$markperc[$markper[$students[$i]]]=$students[$i];
}
}
if(count($markperc)>0){
krsort($markperc);
$i=1;
foreach($markperc as $mar){
$updaterank=with(new Reports)->updateorank(Input::get('examkey'),$mar,$i);
$i=$i+1;
}
}
echo "Successfully Updated";

}
}
}
}

}
