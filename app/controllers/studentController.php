<?php
class studentController extends BaseController {
public $layout = 'layouts.student';
public function index(){
if(Session::has('privilege') && Session::get('privilege')!="student"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
/*$grps = with(new sepStudent)->getGroups();
if(count($grps)>0){
$select = with(new sepStudent)->getGroupid();
$grpid=rtrim($select[0]->stu_gid,",");
$ids=explode(',',$grpid);
$gp=array();
$j=0;
for($i=0;$i<count($ids);$i++){
$gpname=with(new sepStudent)->getGrpname($ids[$i]);
if($gpname){
$gp[$j]['id'] = $ids[$i];
$gp[$j]['grpname']=with(new sepStudent)->getGrpname($ids[$i]);
$gp[$j]['posdetails']=with(new sepStudent)->getPos($ids[$i]);
$gp[$j]['negdetails']=with(new sepStudent)->getNeg($ids[$i]);
$j++;
}
}
$this->layout->with('title', 'A - Grade')->nest('content', 'student.groups',array('posresult' => $gp));
}else{

$result1=with(new sepStudent)->getstudents();
$this->layout->with('title', 'A - Grade')->nest('content', 'student.index',array('result' => $result1));
}*/
$gp=array();
$students=with(new sepStudent)->getsimStudents();
$clasec=with(new sepStudent)->getclasec();
for($i=0;$i<count($students);$i++){
$gp[$i]['stuuname']=$students[$i]->username;
$gp[$i]['stuname']=$students[$i]->fullname;
$gp[$i]['posdetails']=array();
$pd=with(new sepStudent)->getPos($students[$i]->username);
if(count($pd)>0){
$gp[$i]['posdetails']=$pd;
}
$gp[$i]['negdetails']=array();
$nd=with(new sepStudent)->getNeg($students[$i]->username);
if(count($nd)>0){
$gp[$i]['posdetails']=$nd;
}
}
$this->layout->with('classsection',$clasec);
$this->layout->with('title', 'A - Grade')->nest('content', 'student.groups',array('posresult' => $gp));
}


public function getawards(){

$this -> layout = null;
if(Input::has('action') && Input::get('action')=='getawards' && Session::has('username') && Session::get('username')!=""){
if(Input::has('stuuname') && Input::get('stuuname')!=""){
$pd=with(new sepStudent)->getPos(Input::get('stuuname'));
$studetails=array();
$studetails['userdetails']=with(new sepStudent)->getuserdetails(Input::get('stuuname'));
$studetails['username']=Input::get('stuuname');
if(count($pd)>0){
$studetails['awarddetails']=$pd;
}
return View::make('student.singleawards')->with('studetails',$studetails);
}
}//else{
//return Redirect::to('/');
//}


}



public function classfeed(){
if(Session::has('privilege') && Session::get('privilege')!="student"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$clasec=with(new sepStudent)->getclasec();
$grps = with(new sepStudent)->getGroups();
$this->layout->with('classsection',$clasec);
$this->layout->with('title', 'A - Grade')->nest('content', 'classfeed',array('groups'=>$grps));

}

public function attendance(){
if(Session::has('privilege') && Session::get('privilege')!="student"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$clasec=with(new sepStudent)->getclasec();
$attdet = with(new Attendance)->getstuAtten(Session::get('username'),date('Y-m-d'),date('A'));
$this->layout->with('classsection',$clasec);
$this->layout->with('title', 'A - Grade')->nest('content', 'attendance',array('count' => 0,'attdet'=>$attdet));//,'atfn'=>$atfn,'atan'=>$atan));
//$this->layout->with('title', 'A - Grade')->nest('content', 'attendance');

}

public function assignments(){
if(Session::has('privilege') && Session::get('privilege')!="student"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$clasec=with(new sepStudent)->getclasec();
$this->layout->with('classsection',$clasec);
$this->layout->with('title', 'A - Grade')->nest('content', 'assignments',array('stulist'=>Session::get('username')));
}

public function reports(){
if(Session::has('privilege') && Session::get('privilege')!="student"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$exams = with(new Reports)->getExams();
$getReport = array();
$getSubjects=with(new Reports)->getSubjects();
$studentname=Session::get('username');
for($i=0;$i<count($exams);$i++) {
$getReport[$i]['examdetails'] = $exams[$i];
$getReport[$i]['examresults'] = with(new Reports)->getResultsall($exams[$i]->clasubkey);
$getReport[$i]['examrank']= with(new Reports)->getrank($exams[$i]->clasubkey,$studentname);
}
$clasec=with(new sepStudent)->getclasec();
$this->layout->with('classsection',$clasec);
$this->layout->with('title', 'A - Grade')->nest('content', 'student.reports',array('examreport'=>$getReport,'subjects'=>$getSubjects,'stuname'=>$studentname));
}


public function getprofile(){

$this -> layout = null;
if(Input::has('action') && Input::get('action')=='getprofile' && Session::has('username') && Session::get('username')!=""){
if(Input::has('stuuname') && Input::get('stuuname')!=""){

$classes['class']=with(new User)->getsClass(Input::get('stuuname'));
$exam = with(new Reports)->getExams();
for($i=0;$i<count($exam);$i++){
$classes['report'][$exam[$i]->clasubkey]['name']=$exam[$i]->clasubname;
$classes['report'][$exam[$i]->clasubkey]['result']=with(new Reports)->getResultsall($exam[$i]->clasubkey,Input::get('stuuname'));
}
$classes['attendance'] = with(new User)->getsAttendance(Input::get('stuuname'));
$classes['parentdetails']=with(new User)->getsParent(Input::get('stuuname'));
$classes['studentdetails']=with(new User)->checkUser(Input::get('stuuname'),'student');
return View::make('teacher.getprofile')->with('result',$classes);

}
}

}

}
