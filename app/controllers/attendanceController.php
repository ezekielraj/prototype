<?php
class attendanceController extends BaseController {
public $layout = 'layouts.wrapper';



public function attendance($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$atfn = array();
$atan = array();
//for($i=0;$i<7;$i++){
//$sun = date('Y-m-d',strtotime('last sunday +'.($i+0).' days'));
//$atfn[$i] = with(new Attendance)->checkAttendance($sun,"fn");//->firstOrFail();
//$atan[$i] = with(new Attendance)->checkAttendance($sun,"an");//->firstOrFail();
//}
$stulist = with(new Students)->getgrpStudents($groupname,Session::get('username'));//->firstOrFail();
$attdet = with(new Attendance)->getstuAtten($stulist,date('Y-m-d'),date('A'));
$this->layout->with('title', 'A - Grade')->nest('content', 'attendance',array('count' => 0,'attdet'=>$attdet));//,'atfn'=>$atfn,'atan'=>$atan));
}

public function loaddays(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="LoadDays"){
if(Input::has('count') && Input::get('count')!=""){
//$atfn = array();
//$atan = array();
//for($i=0;$i<7;$i++){
//$sun = date('Y-m-d',strtotime('last sunday +'.($i+Input::get('count')).' days'));
//$atfn[$i] = with(new Attendance)->checkAttendance($sun,"fn");//->firstOrFail();
//$atan[$i] = with(new Attendance)->checkAttendance($sun,"an");//->firstOrFail();
//}
return View::make('atloaddays')->with('lsc',array('count' => Input::get('count')));//,'atfn'=>$atfn,'atan'=>$atan));
}
}else{
return Redirect::to('/classes');
}
}

public function loadweek(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="LoadWeek"){
if(Input::has('count') && Input::get('count')!=""){
$count=Input::get('count');
return date('F d',strtotime('last sunday +'.($count).' days'))." - ".date('F d',strtotime('last sunday +'.($count+6).' days'));//date('F d',strtotime('next saturday +'.($count).' days'));
}
}else{
return Redirect::to('/classes');
}
}

public function updateattendance(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="updateattendance" && Session::has('privilege') && Session::get('privilege')=="teacher" && Input::has('stuatt') && Input::has('attendate') && Input::get('attendate')!="" && Input::has('attennoon') && Input::get('attennoon')!=""){
$attdet = with(new Attendance)->updateattendance(Input::get('stuatt'),Input::get('attendate'),Input::get('attennoon'));
//print_r(Input::get('stuatt'));
if($attdet){
echo "s";
}else{
echo "no";
}

}else{
return Redirect::to('/classes');
}
}

public function getattendancevalue(){

$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="getattendancevalue" && Session::has('privilege') && Input::has('groupnum') && Input::has('attendate') && Input::get('attendate')!="" && Input::has('attennoon') && Input::get('attennoon')!=""){
if(Session::get('privilege')=="teacher"){

$groupname = with(new Students)->getGroupName(Input::get('groupnum'));
$stulist = with(new Students)->getgrpStudents($groupname,Session::get('username'));//->firstOrFail();
}
if(Session::get('privilege')=="student"){
$stulist=Session::get('username');
}
if(Session::get('privilege')=="parent"){
$username=with(new sepStudent)->getStuunamefid(Input::get('groupnum'));
$stulist=$username[0]->stu_username;
}
$da=explode('/',Input::get('attendate'));
$attdet = with(new Attendance)->getstuAtten($stulist,$da[2]."-".$da[1]."-".$da[0],Input::get('attennoon'));
echo json_encode($attdet);

}else{
return Redirect::to('/classes');
}


}

}
