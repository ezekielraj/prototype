<?php
class classController extends BaseController {
public $layout = 'layouts.default';
public function classes(){
if(Session::has('privilege') && Session::get('privilege')=="administrator"){

return Redirect::to('/admin');
}elseif(Session::has('privilege') && Session::get('privilege')=="student"){
return Redirect::to('/student');
#$this->layout = View::make('layouts.student');

#$this->layout->with('title', 'A - Grade')->nest('content', 'student.index');
}//elseif(Session::has('privilege') && Session::get('privilege')=="parent"){
//return Redirect::to('/parent');
#$this->layout = View::make('layouts.student');
#$this->layout->with('title', 'A - Grade')->nest('content', 'student.index');
//}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('title', 'A - Grade')->nest('content', 'classes');
}


public function profileupdate(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='profileUpdate' && Session::has('username') && Session::get('username')!=""){
if(Input::has('profusername') && Input::get('profusername')!="" && Input::has('proffullname') && Input::get('proffullname')!="" && Input::has('profemailid') && Input::get('profemailid')!=""){
if(Input::file('profpicture')!=""){
$file=Input::file('profpicture');
$filename= $file->getClientOriginalName();
$extension = Input::file('profpicture')->getClientOriginalExtension();
//$dpath = "C:\\inetpub\\wwwroot\\laravel\\public\\images\\profile\\";
//$dpath = "D:\\laravelnew1\\public\\images\\profile\\";
$dpath = $_SERVER["DOCUMENT_ROOT"]."/images/profile/";
$upload = $file->move($dpath,$file->getClientOriginalName());
//if($upload){
File::move($dpath.$filename, $dpath.Input::get('profusername').".".$extension);
}
$result=with(new User)->profileUpdate(Input::get('profusername'),Input::get('proffullname'),Input::get('profemailid'));
if($result){
Session::put('fullname',Input::get('proffullname'));
Session::put('username',Input::get('profusername'));
return "s";
}else{
return "no";
}
//}else{
//return "no";
//}
//}else{
//return "no";
//}
}else{
return "no";
}
}else{
return Redirect::to('/');
}
}
public function getallsection(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='getallsection' && Input::has('code') && Input::get('code')!="" && Session::has('username') && Session::get('username')!=""){
$result=with(new Admin)->getsections(Input::get('code'));
return View::make('gensection')->with('result',$result);
}else{
return Redirect::to('/');
}
}
public function loadprofileform(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='loadprofileform' && Session::has('username') && Session::get('username')!=""){
//$result=with(new User)->checkUser(Session::get('username'),Session::get('privilege'));
$classes=array();
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$classes['subject']=with(new User)->gettSubjects();
$classes['class']=with(new User)->gettClasses();
}elseif(Session::has('privilege') && Session::get('privilege')=="student"){
$classes['class']=with(new User)->getsClass();
$exam = with(new Reports)->getExams();
for($i=0;$i<count($exam);$i++){
$classes['report'][$exam[$i]->clasubkey]['name']=$exam[$i]->clasubname;
$classes['report'][$exam[$i]->clasubkey]['result']=with(new Reports)->getResultsall($exam[$i]->clasubkey);
}
$classes['attendance'] = with(new User)->getsAttendance();
$classes['parentdetails']=with(new User)->getsParent();
}
return View::make('loadprofileform')->with('result',$classes);//array('0'=>$result,'1'=>$classes));
}else{
return Redirect::to('/');
}
}

public function checkprofile(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='loadprofile' && Session::has('username') && Session::get('username')!=""){
$result=with(new User)->checkUser(Session::get('username'),Session::get('privilege'));
if(count($result)!='0'){
return View::make('profcheck')->with('result',"s");
}else{
return View::make('profcheck')->with('result',"no");
}
}else{
return Redirect::to('/');
}
}
public function loadtmc(){
$this -> layout = null;
$result=array();
if(Input::has('action') && Input::get('action')=='loadtmc' && Session::has('username') && Session::get('username')!=""){
//if (Session::has('token') && Session::get('token')=="1"){
if(Session::get('privilege')=="teacher"){
$result = with(new User)->getGroups(Session::get('username'));//->firstOrFail();
return View::make('loadtmc')->with('result',$result);
//print_r($result);
}elseif(Session::get('privilege')=="parent"){
$result=with(new User)->GetParstudents();
return View::make('loadtmc')->with('result',$result);
}else{
echo "";
}
}else{
return Redirect::to('/');
}
}

public function grpdelete(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='delGroup' && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="teacher"){
if(Input::has('groupId') && Input::get('groupId')!=""){
$result = with(new User)->delGroup(Input::get('groupId'));//->firstOrFail();
if($result){
echo "s";
}
}
}else{
return Redirect::to('/classes');
}
}

public function addstudent(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='addstudent' && Session::has('username') && Session::get('username')!=""){
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$result = with(new User)->getClasses();//->firstOrFail();
return View::make('addstudent')->with('result',$result);
}
}else{
return Redirect::to('/classes');
}
}

public function fixall(){
$this -> layout = null;
$class="";
$section="";
$subject="";
if(Input::has('action') && Input::get('action')=='getStudents' && Session::has('username') && Session::get('username')!=""){
$class=Input::get('classy');
$section=Input::get('sectiony');
$subject=Input::get('subjecty');
//if(Input::has('classy') && Input::has('sectiony') && Input::has('subjecty')){
$row = with(new User)->getStudents($class,$section,$subject);
return View::make('fixall')->with('row',$row);
//}
}else{
return Redirect::to('/classes');
}
}

public function fixsection(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='fixSection' && Session::has('username') && Session::get('username')!=""){
if(Input::has('mclass') && Input::get('mclass')!=""){
$result= with(new User)->getSection(Input::get('mclass'));
return View::make('fixsection')->with('row',$result);
}
}else{
return Redirect::to('/classes');
}
}

public function fixsubject(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='fixSubject' && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="teacher"){
//if(Input::has('mclass') && Input::get('mclass')!="" && Input::has('msection') && Input::get('msection')!=""){
if(Input::has('groupname') && Input::get('groupname')!=""){
$result= with(new User)->getSubject(Input::get('groupname'));//,Input::get('msection'));
//echo print_r($result);
return View::make('fixsubject')->with('row',$result);
}
}else{
return Redirect::to('/classes');
}
}

public function groupupdate(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='updateGroup' && Session::has('username') && Session::get('username')!=""){
if(Input::has('sStudents') && Input::get('sStudents')!="" && Input::has('groupName') && Input::get('groupName')!="" && Input::has('subjectu') && Input::get('subjectu')!=""){
$result = with(new User)->groupUpdate(Input::get('sStudents'),Input::get('groupName'),Session::get('username'),Input::get('subjectu'),Input::get('classu'),Input::get('sectionu'));
if($result){
return View::make('groupupdate')->with('row',"s");
}else{
return View::make('groupupdate')->with('row',"Group cannot be Created!!!");
}
}
}else{
return Redirect::to('/classes');
}
}

public function checkStudentfp(){
$this -> layout = null;
$row=array();
if(Input::has('action') && Input::get('action')=='checkStudentfp' && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="parent"){
$result=with(new User)->checkStudentfp();
if($result){
$row = with(new User)->getkeyStudentsfp();
}
if(count($row)>0){
return View::make('selStudentsfp')->with('row',$row);
}else{
echo "After your Kid selected in a Group.. It will be visible here";
}
}
}

public function updateKidfp(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='updateKidfp' && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="parent" && Input::has('stuvalue') && Input::get('stuvalue')!=""){
$result=with(new User)->updateKidfp(Input::get('stuvalue'));
if($result){
echo "s";
}else{
echo "no";
}
}
}

public function pngetstudents(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="pngetstudents" && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="parent" && Input::has('regcode') && Input::get('regcode')!="" ){
$result=with(new User)->pngetstudents(Input::get('regcode'));
return View::make('fixall')->with('row',$result);
}
}

public function pnaddkid(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="pnaddkid" && Session::has('username') && Session::get('username')!="" && Session::has('privilege') && Session::get('privilege')=="parent" && Input::has('selstudent') && Input::get('selstudent')!="" && Input::has('regcode') && Input::get('regcode')!=""){
$result=with(new User)->pnaddkid(Input::get('selstudent'),Input::get('regcode'));
if($result){
echo "s";
}else{
echo "no";
}
}

}

public function loadstd(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="loadstd"){
$result=with(new User)->getClasses();
if(count($result)>0){
return view::make('loadstd')->with('result',$result);
}
}
}

public function resetpassword(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="resetpassword" && Input::has('oldpassword') && Input::get('oldpassword')!="" && Input::has('newpassword') && Input::get('newpassword')!=""){
if(Input::get('rnewpassword')==Input::get('newpassword')){
$result = with(new User)->resetpassword(Input::get('oldpassword'),Input::get('newpassword'));
if($result){
echo "Password Changed Successfully";
}else{
echo "Password is not changed";
}
}else{
echo "Password is not changed";
}
}
}

}

?>
