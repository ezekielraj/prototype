<?php
class groupController extends BaseController {
public $layout = 'layouts.wrapper';


public function students($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$result = with(new Students)->getgrpStudents($groupnum,Session::get('username'));//->firstOrFail();
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$students=explode(',',$result);
$new = array();
for($i=0;$i<count($students);$i++){
$poscount = with(new Students)->getstuCount($students[$i]);//->firstOrFail();
$negcount = with(new Students)->getstunegCount($students[$i]);//->firstOrFail();
$rank=with(new Reports)->getlatestrank($students[$i]);
$r="Nil";
if(count($rank)>0){
$r=$rank[0]->overallrank;
}
$new[$i] = $students[$i].",".$poscount.",".$negcount.",".$r;
}
$posawards = with(new Students)->getposawards();
$negawards = with(new Students)->getnegawards();
$this->layout->with('title', 'A - Grade')->nest('content', 'students',array('result' => $new,'posawards'=>$posawards,'negawards'=>$negawards,'res'=>$result,'stu'=>$students));
}


public function awardupdate(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='awardUpdate' && Session::has('privilege') && Session::get('privilege')=="teacher"){
if(Input::has('user') && Input::get('user')!="" && Input::has('award') && Input::get('award')!="" && Input::has('subjectname') && Input::get('subjectname')!="" && Input::has('group_num') && Input::get('group_num')!=""){
$result = with(new Students)->awardUpdate(Input::get('user'),Input::get('award'),Input::get('subjectname'),Input::get('group_num'));
if((is_bool($result) === true) && $result === true){
return View::make('awardupdate')->with('row',"s");
}else{
return View::make('awardupdate')->with('row',$result);
}
}
}else{
return Redirect::to('/');
}
}

public function secgetstudents(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="secgetstudents" && Session::has('privilege') && Session::get('privilege')=="teacher"){
if(Input::has('grpnum') && Input::get('grpnum')!=""){
$row = with(new Students)->secGetStudents(Input::get('grpnum'));
return View::make('fixall')->with('row',$row);
//print_r($row);
}
}else{
return Redirect::to('/');
}
}

public function secgroupupdate(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="secgroupupdate" && Session::has('privilege') && Session::get('privilege')=="teacher"){
if(Input::has('grpnum') && Input::get('grpnum')!="" && Input::has('sstudents') && Input::get('sstudents')!=""){
$result=with(new Students)->secGroupUpdate(Input::get('grpnum'),Input::get('sstudents'));
if($result){
echo "s";
}else{
echo "no";
}
}
}else{
return Redirect::to('/');
}
}

public function removestudent(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="removestudent" && Session::has('privilege') && Session::get('privilege')=="teacher"){
if(Input::has('groupnum') && Input::get('groupnum')!="" && Input::has('stuuname') && Input::get('stuuname')!=""){
$result = with(new Students)->removestudent(Input::get('groupnum'),Input::get('stuuname'));
//print_r($result);
if($result){
echo "s";
}else{
echo "Cannot be deleted";
}
}else{
echo "Cannot be Removed";
}
}else{
echo "Cannot Be Deleted";
}
}

}
?>
