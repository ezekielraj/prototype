<?php
class announcementsController extends BaseController {
public $layout = 'layouts.wrapper';
public function announcements($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$this->layout->with('title', 'A - Grade')->nest('content', 'announcements');
}
}
