<?php
class assignmentsController extends BaseController {
public $layout = 'layouts.wrapper';


public function assignments($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$stulist = with(new Students)->getgrpStudents($groupname,Session::get('username'));//->firstOrFail();
$this->layout->with('title', 'A - Grade')->nest('content', 'assignments',array('stulist'=>$stulist));
}

public function createassignment(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Session::get('privilege')=="teacher" && Input::has('action') && Input::get('action')=="createassignment"){
if(Input::has('astitle') && Input::get('astitle')!="" && Input::has('startdate') && Input::get('startdate')!="" && Input::has('enddate') && Input::get('enddate')!="" && Input::has('description') && Input::get('description')!="" && Input::has('groupnum') && Input::get('groupnum')!=""){
$filename="";
if(Input::file('attachment')!=""){
$file=Input::file('attachment');
$filename= $file->getClientOriginalName();
$extension = Input::file('attachment')->getClientOriginalExtension();
if(!File::exists(storage_path()."/assignment")){
//File::makeDirectory(storage_path()."/assignment", $mode = 0755);
}
if(!File::exists(public_path()."/assignment")){
//File::makeDirectory(public_path()."/assignment", $mode = 0755);
}
$dpath = storage_path()."/assignment/";
$upload = $file->move($dpath,$file->getClientOriginalName());
File::move($dpath.$filename, $dpath.urlencode(addslashes($filename)));
}
$result=with(new Assignment)->CreateAssign(Input::get('astitle'),Input::get('startdate'),Input::get('enddate'),Input::get('description'),$filename,Input::get('groupnum'));
if($result){
echo "s";
}else{
echo "no";
}

}else{
echo "Please fill the mandatory fields";
}
//echo storage_path();
}else{
return Redirect::to('/');
}
}

public function getassignments(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Input::has('action') && Input::get('action')=="getassignments" && Input::has('offset') && Input::get('offset')!="" && Input::has('groupnum') && Input::get('groupnum')!=""){
if(Session::get('privilege')=="student"){
$getGroupid = with(new sepStudent)->getGroupid();
$result=with(new Assignment)->getassignments(Input::get('offset'),rtrim($getGroupid[0]->stu_gid,","));
}elseif(Session::get('privilege')=="parent"){
$getGroupid = with(new sepStudent)->getGroupid(Input::get('groupnum'));
$result=with(new Assignment)->getassignments(Input::get('offset'),rtrim($getGroupid[0]->stu_gid,","));
}else{
$result=with(new Assignment)->getassignments(Input::get('offset'),Input::get('groupnum'));
}
return View::make('availassignments')->with('result',$result);

}else{
return Redirect::to('/');
}
}

public function delassignment(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Session::get('privilege')=="teacher" && Input::has('action') && Input::get('action')=="delassignment" && Input::has('assignid') && Input::get('assignid')!="" && Input::has('groupnum') && Input::get('groupnum')!=""){
$result=with(new Assignment)->delassignment(Input::get('assignid'),Input::get('groupnum'));
if($result){
echo "s";
}else{
echo "no";
}
}else{
return Redirect::to('/');
}
}

public function gattach($fname){
$this->layout = null;
if(Session::has('username') && Session::has('privilege')){
if(File::exists(storage_path()."/assignment/".$fname)){
return Response::download(storage_path()."/assignment/".$fname,$fname);
}else{
return null;
}
}
}

public function updateassignment(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Session::get('privilege')=="teacher" && Input::has('action') && Input::get('action')=="updateassignment"){
if(Input::has('asid') && Input::get('asid')!="" && Input::has('astitle') && Input::get('astitle')!="" && Input::has('startdate') && Input::get('startdate')!="" && Input::has('enddate') && Input::get('enddate')!="" && Input::has('description') && Input::get('description')!="" && Input::has('groupnum') && Input::get('groupnum')!=""){
$filename="";
if(Input::file('attachment')!=""){
$file=Input::file('attachment');
$filename= $file->getClientOriginalName();
$extension = Input::file('attachment')->getClientOriginalExtension();
if(!File::exists(storage_path()."/assignment")){
//File::makeDirectory(storage_path()."/assignment", $mode = 0755);
}
if(!File::exists(public_path()."/assignment")){
//File::makeDirectory(public_path()."/assignment", $mode = 0755);
}
$dpath = storage_path()."/assignment/";
$upload = $file->move($dpath,$file->getClientOriginalName());
File::move($dpath.$filename, $dpath.urlencode(addslashes($filename)));
}
$result=with(new Assignment)->UpdateAssign(Input::get('asid'),Input::get('astitle'),Input::get('startdate'),Input::get('enddate'),Input::get('description'),$filename,Input::get('groupnum'));
if($result){
echo "s";
}else{
echo "no";
}

}else{
echo "Please fill the mandatory fields";
}
//echo storage_path();
}else{
return Redirect::to('/');
}
}

public function assignmark(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Session::get('privilege')=="teacher" && Input::has('action') && Input::get('action')=="assignmark" && Input::has('students') && Input::get('students')!="" && Input::has('groupnum') && Input::get('groupnum')!="" && Input::has('assignid') && Input::get('assignid')!=""){

$chd = with(new Assignment)->getassignwithid(Input::get('assignid'),Input::get('groupnum'));
if(time()>strtotime($chd[0]->as_enddate)){
$result=with(new Assignment)->assignmark(Input::get('students'),Input::get('groupnum'),Input::get('assignid'));
print_r($result);
}else{
echo "no";
}

}else{
return Redirect::to('/');
}
}

public function getassignmarks(){
$this->layout = null;
if(Session::has('username') && Session::has('privilege') && Input::has('action') && Input::get('action')=="getassignmarks" && Input::has('aid') && Input::get('aid')!="" && Input::has('groupnum') && Input::get('groupnum')!=""){
$result=with(new Assignment)->getassignmarks(Input::get('aid'),Input::get('groupnum'));
echo json_encode($result);
}
}

}
