<?php
class classfeedController extends BaseController {
public $layout = 'layouts.wrapper';

public function classfeed($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$this->layout->with('title', 'A - Grade')->nest('content', 'classfeed');
}

public function loadsharedcontent(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="LSContent"){
if(Session::has('privilege') && Session::get('privilege')=="student"){
$getGroupid = with(new sepStudent)->getGroupid();
$lsc = with(new Classfeed)->shdcontentLoad(rtrim($getGroupid[0]->stu_gid,","),Session::get('username'),Input::get('gscontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}elseif(Session::has('privilege') && Session::get('privilege')=="parent"){
if(Input::has('grpnum') && Input::get('grpnum')!=""){
$getGroupid = with(new sepStudent)->getGroupid(Input::get('grpnum'));
$lsc = with(new Classfeed)->shdcontentLoad(rtrim($getGroupid[0]->stu_gid,","),Input::get('grpnum'),Input::get('gscontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}
}else{
if(Input::has('grpnum') && Input::get('grpnum')!=""){
$lsc = with(new Classfeed)->shdcontentLoad(Input::get('grpnum'),Session::get('username'),Input::get('gscontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}

}
}else{
return Redirect::to('/classes');
}
}

public function loadannouncecontent(){

$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="LAnnounceContent"){
if(Session::has('privilege') && Session::get('privilege')=="student"){
$getGroupid = with(new sepStudent)->getGroupid();
$lsc = with(new Classfeed)->anncontentLoad(rtrim($getGroupid[0]->stu_gid,","),Session::get('username'),Input::get('gacontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}elseif(Session::has('privilege') && Session::get('privilege')=="parent"){
if(Input::has('grpnum') && Input::get('grpnum')!=""){
$getGroupid = with(new sepStudent)->getGroupid(Input::get('grpnum'));
$lsc = with(new Classfeed)->anncontentLoad(rtrim($getGroupid[0]->stu_gid,","),Input::get('grpnum'),Input::get('gacontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}
}else{
if(Input::has('grpnum') && Input::get('grpnum')!=""){
$lsc = with(new Classfeed)->anncontentLoad(Input::get('grpnum'),Session::get('username'),Input::get('gacontnum'));
if(count($lsc)){
return View::make('loadsharedcontent')->with('lsc',$lsc);
}
}

}
}else{
return Redirect::to('/classes');
}


}

public function feedupdate(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="FeedUpdate"){
if(Input::has('grpnum') && Input::get('grpnum')!="" && Input::has('content') && Input::get('content')!="" && Input::has('announce') && Input::get('announce')!=""){

$filename="";
if(Input::file('attachment')!=""){
$file=Input::file('attachment');
$filename= $file->getClientOriginalName();
$extension = Input::file('attachment')->getClientOriginalExtension();
if(!File::exists(storage_path()."/classfeed")){
//File::makeDirectory(storage_path()."/assignment", $mode = 0755);
}
if(!File::exists(public_path()."/classfeed")){
//File::makeDirectory(public_path()."/assignment", $mode = 0755);
}
$dpath = storage_path()."/classfeed/";
$upload = $file->move($dpath,$file->getClientOriginalName());
File::move($dpath.$filename, $dpath.urlencode(addslashes($filename)));
}
$image=$title=$desc="";
if(Input::has('image')){
$image=Input::get("image");
}
if(Input::has('title')){
$title=Input::get("title");
}
if(Input::has('description')){
$desc=Input::get("description");
}

$update=with(new Classfeed)->feedupdate(Input::get('content'),Input::get('grpnum'),Session::get('username'),Input::get('announce'),$filename,$image,$title,$desc);
if($update){
$students=with(new Students)->getgrpStudents(Input::get('grpnum'),Session::get('username'));
if($students){
$stu=explode(',',$students);
for($i=0;$i<count($stu);$i++){
$msgcount = with(new msgCount)->addmc("s_".$update,$stu[$i],Input::get('grpnum'));
}
}
return "s";
}
}
}else{
return Redirect::to('/classes');
}
}

public function deletefeed(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="deleteFeed"){
if(Input::has('id') && Input::get('id')!=""){
$students=with(new Classfeed)->deletefeed(Input::get('id'),Session::get('username'));
print_r($students);
}
}else{
return Redirect::to('/classes');
}
}


public function updatelike(){

$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="likeUpdate"){
if(Input::has('slno') && Input::get('slno')!=""){
$update=with(new Classfeed)->likeUpdate(Input::get('slno'),Session::get('username'));
//if($update){
return $update;
//}
}
}else{
return Redirect::to('/classes');
}
}

public function updatecomment(){

$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="commentUpdate"){
if(Input::has('slno') && Input::get('slno')!="" && Input::has('textcont') && Input::get('textcont')!="" && Input::has('grpnum') && Input::get('grpnum')!=""){
$update=with(new Classfeed)->commentUpdate(Input::get('slno'),Input::get('textcont'),Session::get('username'));
if($update){
$students=with(new Students)->getgrpStudents(Input::get('grpnum'),Session::get('username'));
$stu=explode(',',$students);
for($i=0;$i<count($stu);$i++){
$msgcount = with(new msgCount)->addmc("c_".$update,$stu[$i],Input::get('grpnum'));
}
return "s";
}
}
}else{
return Redirect::to('/classes');
}
}

public function loadcomment(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="loadcomments"){
if(Input::has('slno') && Input::get('slno')!=""){
$ent=explode('_',Input::get('slno'));
$row=with(new Classfeed)->loadcomments($ent[2],Session::get('username'));
return View::make('loadcomments')->with('row',$row);
}
}else{
return Redirect::to('/classes');
}
}

public function secthank(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="secThank"){
if(Input::has('slno') && Input::get('slno')!=""){
$ent=explode('_',Input::get('slno'));
$row=with(new Classfeed)->secthank($ent[1],Session::get('username'));
return $row;
}
}else{
return Redirect::to('/classes');
}
}

public function secdelete(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="secDelete"){
if(Input::has('slno') && Input::get('slno')!=""){
$ent=explode('_',Input::get('slno'));
$row=with(new Classfeed)->secdelete($ent[1],Session::get('username'));
return $row;
}
}else{
return Redirect::to('/classes');
}
}


public function geturl(){
$this->layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=="geturl"){
if(Input::has('url') && Input::get('url')!=""){
//$variablee = file_get_contents(Input::get('url'));
return View::make('geturl')->with('url',Input::get('url'));
}
}else{
return Redirect::to('/classes');
}
}


public function gcfattach($fname){
$this->layout = null;
if(Session::has('username') && Session::has('privilege')){
if(File::exists(storage_path()."/classfeed/".$fname)){
return Response::download(storage_path()."/classfeed/".$fname,$fname);
}else{
return null;
}
}
}

}
