<?php
class parentController extends BaseController {
public $layout = 'layouts.parent';
public function index(){
if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$row=array();
//$result1=with(new sepStudent)->getstudents();
$result=with(new User)->checkStudentfp();
if($result){
$row = with(new User)->getkeyStudentsfp();
}

$this->layout->with('title', 'A - Grade')->nest('content', 'parent.index',array('row' => $row));
}


public function stugrp($sid){
if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('sid',$sid);

$grps = with(new sepStudent)->getGroups($sid);
//if(count($grps)>0){
$select = with(new sepStudent)->getGroupid($sid);
$grpid=rtrim($select[0]->stu_gid,",");
$ids=explode(',',$grpid);
$gp=array();
$j=0;
for($i=0;$i<count($ids);$i++){
$gpname=with(new sepStudent)->getGrpname($ids[$i]);
if($gpname){
$gp[$j]['id'] = $ids[$i];
$gp[$j]['grpname']=with(new sepStudent)->getGrpname($ids[$i]);
$gp[$j]['posdetails']=with(new sepStudent)->getPos($ids[$i]);
$gp[$j]['negdetails']=with(new sepStudent)->getNeg($ids[$i]);
$j++;
}
}
$this->layout->with('title', 'A - Grade')->nest('content', 'parent.groups',array('groups'=>$gp));
//}

}

public function classfeed($sid){
if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('sid',$sid);
$grps = with(new sepStudent)->getGroups($sid);
$this->layout->with('title', 'A - Grade')->nest('content', 'classfeed',array('groups'=>$grps));
}


public function attendance($sid){

if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('sid',$sid);
$username=with(new sepStudent)->getStuunamefid($sid);
$attdet = with(new Attendance)->getstuAtten($username[0]->stu_username,date('Y-m-d'),date('A'));
$this->layout->with('title', 'A - Grade')->nest('content', 'attendance',array('count' => 0,'attdet'=>$attdet));//,'atfn'=>$atfn,'atan'=>$atan));

}

public function assignments($sid){
if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('sid',$sid);
$username=with(new sepStudent)->getStuunamefid($sid);
$this->layout->with('title', 'A - Grade')->nest('content', 'assignments',array('stulist'=>$username[0]->stu_username));
}

public function reports($sid){
if(Session::has('privilege') && Session::get('privilege')!="parent"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('sid',$sid);
$exams = with(new Reports)->getExams();
$getReport = array();
$username=with(new sepStudent)->getStuunamefid($sid);
$getSubjects=with(new Reports)->getSubjects($username[0]->stu_username);
$studentname=$username[0]->stu_username;
for($i=0;$i<count($exams);$i++) {
$getReport[$i]['examdetails'] = $exams[$i];
$getReport[$i]['examresults'] = with(new Reports)->getResultsall($exams[$i]->clasubkey,$username[0]->stu_username);
$getReport[$i]['examrank']= with(new Reports)->getrank($exams[$i]->clasubkey,$username[0]->stu_username);
}
$this->layout->with('title', 'A - Grade')->nest('content', 'student.reports',array('examreport'=>$getReport,'subjects'=>$getSubjects,'stuname'=>$studentname));
}

}
