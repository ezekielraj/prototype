<?php
class calenderController extends BaseController {
public $layout = 'layouts.wrapper';

public function calender($groupnum,$subnum){
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$groupname = with(new Students)->getGroupName($groupnum);
$subname = with(new Students)->getSubjectName($subnum);
$subkey = with(new Students)->getSubjectKey($subnum);
$this->layout->with('subkey',$subkey);
$this->layout->with('groupnum',$groupnum);
$this->layout->with('subnum',$subnum);
$this->layout->with('groupname',$groupname);
$this->layout->with('subname',$subname);
$this->layout->with('title', 'A - Grade')->nest('content', 'calender');
}

public function getevents(){
$this -> layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=='getEvents'){
if(Input::has('dater') && Input::get('dater')!=""){
$grps=0;
if(Session::has('privilege') && Session::get('privilege')=="student"){
$grpss = with(new sepStudent)->getGroupid();
$grps=rtrim($grpss[0]->stu_gid,",");
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$grps="";
}else{
$grps=Input::get('groupnum');
}
$events = with(new Calender)->getEvents(Input::get('dater'),Session::get('username'),$grps);
$dailyevents = with(new Calender)->dailyEvents(Input::get('dater'),Session::get('username'),$grps);
$weeklyevents = with(new Calender)->weeklyEvents(Input::get('dater'),Session::get('username'),$grps);
$monthlyevents = with(new Calender)->monthlyEvents(Input::get('dater'),Session::get('username'),$grps);
return View::make('calevents')->with('events',array('today' => $events,'daily'=>$dailyevents,'weekly'=>$weeklyevents,'monthly'=>$monthlyevents));


}
}else{
return Redirect::to('/');
}
}
public function createevents(){
$this -> layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=='createEvents'){
if(Input::has('title') && Input::get('title')!="" && Input::has('description') && Input::get('description')!="" && Input::has('fromdate') && Input::get('fromdate')!="" && Input::has('todate') && Input::get('todate')!="" && Input::has('fromhours') && Input::get('fromhours')!="" && Input::has('frommin') && Input::get('frommin')!="" && Input::has('fromampm') && Input::get('fromampm')!="" && Input::has('tohours') && Input::get('tohours')!="" && Input::has('tomin') && Input::get('tomin')!="" && Input::has('toampm') && Input::get('toampm')!="" && Input::has('repeat') && Input::get('repeat')!="" && ((Input::has('grpnum') && Input::get('grpnum')!="") || (Session::has('privilege') && Session::get('privilege')=="administrator"))){
$events = with(new Calender)->createEvent(Input::get('title'),Input::get('description'),Input::get('fromdate'),Input::get('todate'),Input::get('fromhours'),Input::get('frommin'),Input::get('fromampm'),Input::get('tohours'),Input::get('tomin'),Input::get('toampm'),Input::get('repeat'),Input::get('grpnum'),Session::get('username'),Input::get('wtshare'),Input::get('eventid'));
if($events){
return "s";
}else{
return "Not Created try later!";
//return $events;
}
}
}else{
return Redirect::to('/');
}
}

public function getdates(){
$this -> layout = null;
if(Session::has('username') && Session::get('username')!="" && Input::has('action') && Input::get('action')=='getDate'){
//if(Input::has('dater')){
$eventdates = with(new Calender)->getEventDates(Input::get('dater'),Input::get('groupnum'));
$datee="";
for($i=0;$i<count($eventdates);$i++){
if($eventdates[$i]->ag_fromdate!=""){
$odate = explode(" ",$eventdates[$i]->ag_fromdate);
$ldate=explode('-',$odate[0]);
$datee=$datee.",".$ldate[1]."/".$ldate[2]."/".$ldate[0];//eventdates[$i]->ag_fromdate;
}
}
        
echo $datee;//"05/05/2015,06/05/2015";//', '23/01/2012'";//.Input::get('dater');
//}
//echo $eventdates;
}
}


}
