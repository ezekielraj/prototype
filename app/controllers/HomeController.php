<?php
class HomeController extends BaseController {
    public $layout = 'layouts.default';
	public function showLogin()
	{
if (Session::has('fullname'))
{
return Redirect::to('index.php/classes');
}
if(Session::has('privilege') && Session::get('privilege')=="administrator"){
return Redirect::to('/admin');
}
$this->layout->with('title', 'A - Grade')->nest('content', 'login3');
	}
public function showchooselogin(){
$this-> layout=null;
if(Input::has('action') && Input::get('action')=='getIndex' && Input::has('value') && Input::get('value')!=""){

return View::make('index')->with('result',Input::get('value'));
}
}
	public function valLogin(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='ALogin'){
if(Input::has('User') && Input::has('Pass')){
$action = Input::get('action');
$user=Input::get('User');
$pass = Input::get('Pass');
$row = with(new Index)->authenticate($user,$pass);//->firstOrFail();
if($row){
Session::put('userid',$row[0]->slno);
Session::put('username', $user);
Session::put('emailid', strtolower(stripslashes(urldecode($row[0]->emailaddress))));
Session::put('privilege', stripslashes(urldecode($row[0]->privilege)));
Session::put('fullname', stripslashes(urldecode($row[0]->fullname)));
Session::put('token', $row[0]->token);
print json_encode(array("message"=>"s"));
}else{
print json_encode(array("message"=>"Login Failed"));
}

}else{
print json_encode(array("message"=>"Fill all the Details"));
}
}else{
print json_encode(array("message"=>"Fill all the Details"));
}
	}

public function Logout(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='ALogout'){
Session::flush();
echo "s";
}else{
return Redirect::to('/');
}
}

private function gentempkey($key){
$string="";
$capital="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$small="abcdefghijklmnopqrstuvwxyz";
$number="0123456789";
for($i=0;$i<$key;$i++){
$val=rand(0,2);
if($val=="0"){
$string=$string.$capital[rand(0,25)];
}elseif($val=="1"){
$string=$string.$small[rand(0,25)];
}elseif($val=="2"){
$string=$string.$number[rand(0,9)];
}
}
return $string;
}


public function register(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='Register'){
if(Input::has('key') && Input::get('key')!="" && Input::has('whois') && Input::get('whois')!=""){
$row = with(new Index)->checkKey(Input::get('key'),Input::get('whois'));
if(count($row)!=0){
$token=self::gentempkey(25);
$roww = with(new Index)->tempKey(Input::get('key'),$token);
if($roww){
if(isset($row[0]->feature_class) && $row[0]->feature_class!=""){
Session::put('class', $row[0]->feature_class);
}
if(isset($row[0]->feature_section) && $row[0]->feature_section!=""){
Session::put('section', $row[0]->feature_section);
}

Session::put('regkey',Input::get('key'));
Session::put('token',Input::get('key'));
Session::put('regtoken',$token);
Session::put('privilege',$row[0]->privilege);
return View::make('registeruserform');
}
}else{
echo "Enter the Valid Key";
}
}else{
echo "Enter the Key";
}
}else{
return Redirect::to('/');
}
}

public function fregister(){
$this -> layout = null;
if(Input::has('action') && Input::get('action')=='fRegister'){
if(Input::has('firstname') && Input::get('firstname')!="" && Input::has('lastname') && Input::get('lastname')!="" && Input::has('username') && Input::get('username')!="" && Input::has('password') && Input::get('password')!="" && Input::has('emailid') && Input::get('emailid')!=""){
$unique = with(new Index)->checkUnique(Input::get('username'),Input::get('emailid'));
if($unique){
echo $unique;
}else{
$validity = with(new Index)->checkkeyvalidity(Session::get('regkey'),Session::get('regtoken'));
if($validity){
$row = with(new Index)->fregister(Input::get('firstname')." ".Input::get('lastname'),Input::get('username'),Input::get('password'),Input::get('emailid'));
if($row){

if(Input::file('profpicture')!=""){
$file=Input::file('profpicture');
$filename= $file->getClientOriginalName();
$extension = Input::file('profpicture')->getClientOriginalExtension();
//$dpath = "C:\\inetpub\\wwwroot\\laravel\\public\\images\\profile\\";
//$dpath = "D:\\laravelnew1\\public\\images\\profile\\";
$dpath = $_SERVER["DOCUMENT_ROOT"]."/images/profile/";
$upload = $file->move($dpath,$file->getClientOriginalName());
//if($upload){
File::move($dpath.$filename, $dpath.Input::get('username').".".$extension);
}

$result=with(new User)->profileUpdate(Input::get('username'),Input::get('firstname')." ".Input::get('lastname'),Input::get('emailid'));
if($result){


Session::flush();
echo "s";
}//else{
//echo "s";
//}
}else{
echo "Please try later";
}
}else{
echo "Please try later";
}
}
}else{
echo "Enter all the fields";
}
}else{
return Redirect::to('/');
}
}

}
