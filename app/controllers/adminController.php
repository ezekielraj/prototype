<?php
class adminController extends BaseController{
public $layout = 'layouts.admin';

public function adminhome(){
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}

if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('title', 'A - Grade')->nest('content', 'adminhome');
}
private function genkeycode($key){
$string="";
$capital="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$small="abcdefghijklmnopqrstuvwxyz";
$number="0123456789";
for($i=0;$i<$key;$i++){
$val=rand(0,2);
if($val=="0"){
$string=$string.$capital[rand(0,25)];
}elseif($val=="1"){
$string=$string.$small[rand(0,25)];
}elseif($val=="2"){
$string=$string.$number[rand(0,9)];
}
}
return $string;
}

public function createsection(){
$this->layout=null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && Input::get('action')=='createsection'){
if(Input::has('code') && Input::get('code')!=""){
/*do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);
*/
$result1=with(new Admin)->createsection(Input::get('code'));//,$code);

echo $result1;
}else{
return Redirect::to('/');
}
}else{
return Redirect::to('/');
}
}
}

public function deletesection(){
$this->layout=null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && Input::get('action')=='deletesection'){
if(Input::has('code') && Input::get('code')!=""){
/*do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);

}while($result);

*/
$result1=with(new Admin)->deletesection(Input::get('code'));//,$code);

echo $result1;
}else{
return Redirect::to('/');
}
}else{
return Redirect::to('/');
}
}
}


public function clearkey() {
$this->layout=null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && Input::get('action')=='clearkey'){
if(Input::has('sec')){
return with(new Admin)->clearCode(Input::get('std'),Input::get('sec'));
}else{
return with(new Admin)->clearCode(Input::get('std'),NULL);
}
}else{
return NULL;
}
}
}

public function gettkey(){
$this->layout=null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && Input::get('action')=='getkey'){
if(Input::has('std') && Input::get('std')!=""){
do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);

do{
$code1 = self::genkeycode(8);
if($code!=$code1){
$result1=with(new Admin)->chkcode($code);
}else{
$result1 = true;
}
}while($result1);
$result2=with(new Admin)->gettkey(Input::get('std'),Input::get('sec'),$code,$code1);
return View::make('gettkey')->with('result',$result2);
//print_r($result2);
}else{
return Redirect::to('/');
}
}else{
return Redirect::to('/');
}
}
}

public function checkkey(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=='checkkey' && Session::has('privilege') && Session::get('privilege')=="administrator"){
$result=with(new Admin)->checkkey(Input::get('std'),Input::get('sec'));
if($result[0]->studentkey=="" && $result[0]->parentkey==""){
echo "s";
}else{
echo "no";
}

}
}

public function gensection(){
$this->layout=null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && (Input::get('action')=='gensection' || Input::get('action')=='gensection1')){
if(Input::has('code') && Input::get('code')!=""){
$result1=with(new Admin)->getsections(Input::get('code'));

return View::make(Input::get('action'))->with('result',$result1);

//echo "hi";
}else{
return Redirect::to('/');
}
}else{
return Redirect::to('/');
}
}

}
/*
public function gencode(){
$this -> layout = null;
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}else{
if(Input::has('action') && Input::get('action')=='gencode'){
if(Input::has('code') && Input::get('code')!=""){
do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);
$result1=with(new Admin)->codeUpdate($code,Input::get('code'));
if($result1){
echo $code;
}
}else{
return Redirect::to('/');
}
}else{
return Redirect::to('/');
}
}
}*/

public function codegenerator(){
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$result=with(new Admin)->codegenerator();
$this->layout->with('title', 'A - Grade')->nest('content', 'codegenerator',array('result'=>$result));
}
public function permissions(){
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$this->layout->with('title', 'A - Grade')->nest('content', 'permissions');
}

public function teacherkey(){
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}
if (Session::has('fullname'))
{
$this->layout->with('fullname',Session::get('fullname'));
}else{
return Redirect::to('/');
}
$result=with(new Admin)->teacherkey();
if(count($result)==1){
$teacherkey = $result[0]->teacherkey;
}else{
$teacherkey = "no";
/*do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);
$result=with(new Admin)->insertteacherkey($code);
if($result){
$teacherkey = $code;
}else{
$teacherkey="no";
}
*/
}
$this->layout->with('title', 'A - Grade')->nest('content', 'teacherkey',array('result'=>$teacherkey));
}

public function genteachkey(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="genteachkey" && Session::has('privilege') && Session::get('privilege')=="administrator"){
do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);
$result=with(new Admin)->insertteacherkey($code);
if($result){
$teacherkey = $code;
}else{
$teacherkey="no";
}
echo $teacherkey;
}

}


public function people(){

if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}
if (Session::has('fullname')){
$this->layout->with('fullname',Session::get('fullname'));
}
$result=with(new Admin)->availableclasses();
//$result=with(new Admin)->getUsers();
$this->layout->with('title', 'A - Grade')->nest('content','admin.people',array('result'=>$result));
}

public function addentries(){
if(Session::has('privilege') && Session::get('privilege')!="administrator"){
return Redirect::to('/classes');
}
if (Session::has('fullname')){
$this->layout->with('fullname',Session::get('fullname'));
}
$getclass=Clasubkey::where('clasubkey','like','class_%')->get();
$getSubject=Clasubkey::where('clasubkey','like','subject_%')->get();
$getExam=Clasubkey::where('clasubkey','like','exam_%')->get();
$this->layout->with('title', 'A - Grade')->nest('content','admin.addentries',array('class'=>$getclass,'subject'=>$getSubject,'exam'=>$getExam));
}

public function getuser(){
$this->layout=null;
$name="";
if(Input::has('action') && Input::get('action')=="privilege" && Input::has('priv') && Input::get('priv')!=""){
$result=with(new Admin)->getUsers(Input::get('priv'),$name,Input::get('schclass'),Input::get('schsection'));
return View::make('admin.adminuser')->with('result',$result);
//print_r($result);
}
}

public function getuser1(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="privilege" && Input::has('priv') && Input::get('priv')!="" && Input::has('funame') && Input::get('funame')!=""){
$result=with(new Admin)->getUsers(Input::get('priv'),Input::get('funame'),Input::get('schclass'),Input::get('schsection'));
return View::make('admin.adminuser')->with('result',$result);
//print_r($result);
}
}

public function addedituser(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="addedituser"){
$result=with(new Admin)->availableclasses();
return View::make('admin.addedituser')->with('result',$result);
}
}

public function addclass(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="addclass"){
return View::make('admin.addclass');
}
}

public function addsubject(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="addsubject"){
return View::make('admin.addsubject');
}
}

public function addexam(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="addexam"){
return View::make('admin.addexam');
}
}


public function acsubmit(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="acsubmit"){
if(Input::has('classname') && Input::get('classname')!="" && Input::has('classkey') && Input::get('classkey')!=""){
$result=with(new Admin)->addclass(Input::get('classname'),Input::get('classkey'));
if($result){
echo "s";
}else{
echo "n";
}
}
}
}

public function assubmit(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="assubmit"){
if(Input::has('subjectname') && Input::get('subjectname')!="" && Input::has('subjectkey') && Input::get('subjectkey')!=""){
$result=with(new Admin)->addsubject(Input::get('subjectname'),Input::get('subjectkey'));
if($result){
echo "s";//$result;
}else{
echo "n";//$result;
}
}
}
}

public function aesubmit(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="aesubmit"){
if(Input::has('examname') && Input::get('examname')!="" && Input::has('examkey') && Input::get('examkey')!="" && Input::has('exampassper') && Input::get('exampassper')!=""){
$result=with(new Admin)->addexam(Input::get('examname'),Input::get('examkey'));
if($result){
$result=with(new Admin)->addexampassper(Input::get('examkey'),Input::get('exampassper'));

echo "s";//$result;



}else{
echo "n";//$result;
}
}
}
}

public function removeclass(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="removeclass"){
if(Input::has('id') && Input::get('id')!=""){

$select=with(new Admin)->selectclasubkey(Input::get('id'));
if(count($select)==1){
$rempass=with(new Admin)->delexampassper($select[0]->clasubkey);
}
$result=with(new Admin)->deleteclass(Input::get('id'));
if($result){
echo "s";
}else{
echo "n";
}
}
}
}

public function udeactivate(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="udeactivate" && Session::has('privilege') && Session::get('privilege')=="administrator"){
if(Input::has('slno') && Input::get('slno')!=""){
$result=with(new Admin)->userstatuschange(Input::get('slno'),0);
if($result){
return "s";
}else{
return "no";
}

}
}
}


public function uactivate(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="uactivate" && Session::has('privilege') && Session::get('privilege')=="administrator"){
if(Input::has('slno') && Input::get('slno')!=""){
$result=with(new Admin)->userstatuschange(Input::get('slno'),1);
if($result){
return "s";
}else{
return "no";
}

}
}



}

public function createuser(){
$this->layout=null;
if(Input::has('action') && Input::get('action')=="createuser" && Session::has('privilege') && Session::get('privilege')=="administrator"){

if(Input::has('firstname') && Input::get('firstname')!="" && Input::has('lastname') && Input::get('lastname')!="" && Input::has('username') && Input::get('username')!="" && Input::has('email') && Input::get('email')!="" && Input::has('password') && Input::get('password')!="" && Input::has('rpassword') && Input::get('rpassword')!="" && Input::has('role') && Input::get('role')!=""){
$unique = with(new Index)->checkUnique(Input::get('username'),Input::get('email'));
if($unique){
echo $unique;
}else{
if(Input::get('userkey')==""){
if(Input::has('userclass') && Input::get('userclass')!=""){// && Input::has('usersection') && Input::get('usersection')!=""){
if(Input::get('role')=="teacher"){
$teachkey=with(new Admin)->teacherkey();
$userkey=$teachkey[0]->teacherkey;
}elseif(Input::get('role')=="parent" || Input::get('role')=="student"){
$key=with(new Admin)->getkeyy(Input::get('userclass'),Input::get('usersection'),Input::get('role'));
if(Input::get('role')=="student"){
$userkey=$key[0]->studentkey;
}else{
$userkey=$key[0]->parentkey;
}
}elseif(Input::get('role')=="administrator"){
$userkey=1;
}
if($userkey==""){
do{
$code = self::genkeycode(8);
$result=with(new Admin)->chkcode($code);
}while($result);

do{
$code1 = self::genkeycode(8);
if($code!=$code1){
$result1=with(new Admin)->chkcode($code);
}else{
$result1 = true;
}
}while($result1);
$result2=with(new Admin)->gettkey(Input::get('userclass'),Input::get('usersection'),$code,$code1);

if(Input::get('role')=="student"){
$userkey=$result2[0]->studentkey;
}else{
$userkey=$result2[0]->parentkey;
}


}
}
}
if($userkey!=""){
if(Input::get('password')==Input::get('rpassword')){
if(Input::get('role')=="administrator"){
$key="1";
}else{
$key=$userkey;
}
$createuser=with(new Admin)->createuser(Input::get('firstname')." ".Input::get('lastname'),Input::get('username'),Input::get('password'),Input::get('role'),Input::get('email'),$key);
if($createuser){
$getuserslno=with(new Admin)->getuserslno(Input::get('username'),Input::get('email'));
if(Input::get('role')=="student"){
$clasec=with(new Index)->checkKey($userkey,Input::get('role'));
$regstudent=with(new Admin)->regstudent($getuserslno[0]->slno,Input::get('username'),Input::get('firstname')." ".Input::get('lastname'),$clasec[0]->feature_class,$clasec[0]->feature_section);
}elseif(Input::get('role')=="teacher"){
$regstudent=with(new Admin)->regteacher($getuserslno[0]->slno,Input::get('username'));
}elseif(Input::get('role')=="parent"){
$clasec=with(new Index)->checkKey($userkey,Input::get('role'));
$regstudent=with(new Admin)->regparent($getuserslno[0]->slno,Input::get('username'),Input::get('firstname')." ".Input::get('lastname'),Input::get('userkey'),$clasec[0]->feature_section,$clasec[0]->feature_class);
}
if($regstudent){
echo "User created Successfully";
}else{
echo "There is a problem, creating user please try later";
}


}else{
echo "user cannot be created";
}

}else{
echo "Passwords dont match";
}
}else{
echo "please enter whether userkey or class & section";

}
}

}else{
echo "please fill all the fields";
}
}
}

}
?>
