<?php

trait selectTables {

private static $dbnam="agradedb";//"aGradeDBTest1";

private function getkeytable(){
return "`".self::$dbnam."`.`ag_clasubkey`";
}

private function getstudentstable(){
return "`".self::$dbnam."`.`ag_studentsdetails`";
}

private function getuserTable(){
return "`".self::$dbnam."`.`ag_user`";
}

private function getGrouptable(){
return "`".self::$dbnam."`.`ag_groups`";
}

private function getawardstable(){
return "`".self::$dbnam."`.`ag_awards`";
}

private function getawardskeytable(){
return "`".self::$dbnam."`.`ag_awardskey`";
}

private function getattendancetable(){
return "`".self::$dbnam."`.`ag_attendance`";
}

private function getcftable(){
return "`".self::$dbnam."`.`ag_classfeed`";
}

private function getcmltable(){
return "`".self::$dbnam."`.`ag_cflandc`";
}

}

