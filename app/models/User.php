<?php
require_once('selectTables.php');
/*use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/
class User extends Eloquent {
//implements UserInterface, RemindableInterface {
use selectTables;
//	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 
	protected $table = 'users';

	
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 
	protected $hidden = array('password', 'remember_token');

public function loadprofileform($user,$priv){
if($priv=="teacher"){
$result = DB::select(DB::raw("select * from ag_teachersdetails where teacher_username='".urlencode(addslashes(strtolower($user)))."'"));
}elseif($priv=="student"){
$result = DB::select(DB::raw("select * from ag_studentsdetails where stu_username='".urlencode(addslashes(strtolower($user)))."'"));
}
return $result;
}*/
private function getParentCS(){
$sec="";
$ss=DB::select(DB::raw("select b.feature_class,b.feature_section from ag_parentsdetails a,ag_userkeys b,ag_user c where c.privilege = 'parent' and c.username='".Session::get('username')."' and c.username=a.par_username and c.token=b.parentkey and c.token='".Session::get('token')."'"));
if(count($ss)>0){
$sec=$ss[0]->feature_section;
//DB::update(DB::raw("update ag_parentsdetails set par_class='".$ss[0]->feature_class."',par_section='".$sec."' where par_username='".Session::get('username')."' and userkey='".Session::get('token')."' and userid='".Session::get('userid')."'"));
DB::update(DB::raw("update ag_parentsdetails set par_class='".$ss[0]->feature_class."',par_section='".$sec."',userkey='".Session::get('token')."' where par_username='".Session::get('username')."' and userid='".Session::get('userid')."'"));

}
}
private function getStudentCS(){
$sec="";
$ss = DB::select(DB::raw("select b.feature_class,b.feature_section from ag_user a, ag_userkeys b where a.privilege = 'student' and a.username='".Session::get('username')."' and a.token=b.studentkey"));
if(count($ss)>0){
$sec=$ss[0]->feature_section;
DB::update(DB::raw("update ag_studentsdetails set stu_class='".$ss[0]->feature_class."',stu_section='".$sec."' where stu_username='".Session::get('username')."' and userid='".Session::get('userid')."'"));
}
}

private function profupdate($user,$fname){
if(Session::get('privilege')=="teacher"){
try{
$result=DB::update("update ag_teachersdetails set teacher_username ='?' where userid='?'",array(urlencode(addslashes($user)),Session::get('userid')));
//$result="update ag_teachersdetails set teacher_username ='".urlencode(addslashes($user))."' where userid='".Session::get('userid')."')";
return true;
}catch(\Exception $e){
return false;
}

}elseif(Session::get('privilege')=="student"){
try{
//$result=DB::update("update ag_teachersdetails set stu_username=?',stu_fullname='?' where userid='?'",array(urlencode(addslashes($user)),urlencode(addslashes($fname)),Session::get('userid')));
$result=DB::update(DB::raw("update ag_studentsdetails set stu_username='".urlencode(addslashes($user))."',stu_fullname='".urlencode(addslashes($fname))."' where userid='".Session::get('userid')."'"));
self::getStudentCS();
return true;
}catch(\Exception $e){
return false;
}
}elseif(Session::get('privilege')=="parent"){
try{
//$result=DB::update("update ag_teachersdetails set stu_username=?',stu_fullname='?' where userid='?'",array(urlencode(addslashes($user)),urlencode(addslashes($fname)),Session::get('userid')));
$result=DB::update(DB::raw("update ag_parentsdetails set par_username='".urlencode(addslashes($user))."',par_fullname='".urlencode(addslashes($fname))."' where userid='".Session::get('userid')."'"));
return true;
}catch(\Exception $e){
return false;
}

}
//if($result){
//return true;
//}else{
//return false;
//}
}
private function profileinsert($user,$fname){
if(Session::get('privilege')=="teacher"){
$result="insert into ag_teachersdetails (userid,teacher_username) values ('".Session::get('userid')."','".urlencode(addslashes($user))."')";
}elseif(Session::get('privilege')=="student"){
$result="insert into ag_studentsdetails (userid,stu_username,stu_fullname) values ('".Session::get('userid')."','".urlencode(addslashes($user))."','".urlencode(addslashes($fname))."')";
}elseif(Session::get('privilege')=="parent"){
$result="insert into ag_parentsdetails (userid,par_username,par_fullname,userkey) values('".Session::get('userid')."','".urlencode(addslashes($user))."','".urlencode(addslashes($fname))."','".Session::get('token')."')";
}
if(DB::insert(DB::raw($result))){
if(Session::get('privilege')=="student"){
self::getStudentCS();
}
if(Session::get('privilege')=="parent"){
self::getParentCS();
}
return true;
}else{
return false;
}
}
public function profileUpdate($puser,$fname,$emailid){
try{

$query = "update ag_user set username='".urlencode(addslashes($puser))."',fullname = '".urlencode(addslashes($fname))."',emailaddress='".urlencode(addslashes($emailid))."' where slno='".Session::get('userid')."'";
$result=DB::update(DB::raw($query));
//$result=DB::update("update ag_user set username=?,fullname=?,emailaddress=? where slno=?",array(urlencode(addslashes($puser)),urlencode(addslashes($fname)),urlencode(addslashes($emailid)),Session::get('userid')));

$check = self::checkUser($puser,Session::get('privilege'));
if(count($check)=='0'){
$result1=self::profileinsert($puser,$fname);
}else{
$result1=self::profupdate($puser,$fname);
}
//if($result1){
//return true;
//}else{
//return false;
//}
return $result1;

//return true;
}catch(\Exception $e){
return false;
}

}

public function checkUser($user,$priv){
if($priv=="teacher"){
$result = DB::select(DB::raw("select * from ag_teachersdetails where teacher_username='".urlencode(addslashes(strtolower($user)))."' or userid='".Session::get('userid')."'"));
}elseif($priv=="student"){
$result = DB::select(DB::raw("select * from ag_studentsdetails where stu_username='".urlencode(addslashes(strtolower($user)))."' or userid='".Session::get('userid')."'"));
}elseif($priv=="parent"){
$result = DB::select(DB::raw("select * from ag_parentsdetails where par_username='".urlencode(addslashes(strtolower($user)))."' or userid='".Session::get('userid')."'"));
}
return $result;
}
private function additionalgetstudents($slno){
$sel = DB::select(DB::raw("select stu_username from ag_studentsdetails where stu_gid like '%,".$slno.",%'"));
$result="";
for($i=0;$i<count($sel);$i++){
if($result){
$result=$result.",".stripslashes(urldecode($sel[$i]->stu_username));
}else{
$result=stripslashes(urldecode($sel[$i]->stu_username));
}

}
return urlencode(addslashes($result));
}
public function getGroups($user){
//$row = DB::select(DB::raw("select a.slno,a.group_name,a.teacher_username,a.selection,a.group_icon,b.slno as subkey,b.clasubname from ag_groups a, ag_clasubkey b where a.teacher_username='".urlencode(addslashes($user))."' and a.teacher_subject=b.clasubkey"));
if(Session::has('privilege')){
if(Session::get('privilege')=="teacher"){
$row = DB::select(DB::raw("select a.slno,a.group_name,a.teacher_username,a.group_icon,b.slno as subkey,b.clasubname from ag_groups a left join ag_clasubkey b on b.clasubkey=a.teacher_subject where a.ag_status='1' and a.teacher_username='".urlencode(addslashes($user))."'"));// and a.teacher_subject=b.clasubkey"));
}
if(Session::get('privilege')=="student"){
$grpid = DB::select(DB::raw("select stu_gid from ag_studentsdetails where stu_username='".urlencode(addslashes($user))."'"));
$neww=explode(',',$grpid[0]->stu_gid);
$n="";
for($i=0;$i<count($neww);$i++){
if(trim($neww[$i])){
if($n){
$n=$n.",".$neww[$i];
}else{
$n=$neww[$i];
}
}
}
if($n){
$row = DB::select(DB::raw("select a.slno,a.group_name,a.teacher_username,a.group_icon,b.slno as subkey,b.clasubname from ag_groups a left join ag_clasubkey b on a.teacher_subject=b.clasubkey where a.ag_status='1' and a.slno in (".$n.")"));// and a.teacher_subject=b.clasubkey"));
}else{
$row=$n;
}
}
if(Session::get('privilege')=="parent"){
$grpid = DB::select(DB::raw("select a.stu_gid from ag_studentsdetails a, ag_parentsdetails b where b.par_username='".urlencode(addslashes($user))."' and a.stu_username=b.stu_username"));
$n="";
for($j=0;$j<count($grpid);$j++){
$neww=explode(',',$grpid[$j]->stu_gid);
for($i=0;$i<count($neww);$i++){
if(trim($neww[$i])){
if($n){
$n=$n.",".$neww[$i];
}else{
$n=$neww[$i];
}
}
}
}
if($n){
$row = DB::select(DB::raw("select a.slno,a.group_name,a.teacher_username,a.group_icon,b.slno as subkey,b.clasubname from ag_groups a left join ag_clasubkey b on b.clasubkey=a.teacher_subject where a.ag_status='1' and a.slno in (".$n.")"));// and a.teacher_subject=b.clasubkey"));
}else{
$row=$n;
}
}
}
//$row[0]->selection = self::additionalgetstudents($row[0]->slno);
return $row;//"select a.slno,a.group_name,a.teacher_username,a.group_icon,b.slno as subkey,b.clasubname from ag_groups a, ag_clasubkey b where a.slno in (".$n.") and a.teacher_subject=b.clasubkey";
}

public function delGroup($gid){
try{
DB::update(DB::raw("update ag_groups set ag_status='0' where slno='".urlencode(addslashes($gid))."'"));
//$query=DB::delete('delete from ag_groups where slno = ?', array(urlencode(addslashes($gid))));
//$query="delete from ".self::getGrouptable()." where slno='".urlencode(addslashes($gid))."'";
return true;
}catch(Exception $e){
return false;
}
}

public function getClasses(){
$select = DB::select(DB::raw("select clasubname,clasubkey from ag_clasubkey where clasubkey LIKE 'class_%'"));
//$result = $this->dbconnect()->fetchAll($select);//$out;
return $select;
}

public function getStudents($class,$section,$subject){
$select = "select stu_username,stu_fullname from ag_studentsdetails";//.self::getstudentstable();
if($class!="" && $class!="all"){
$select = $select." where stu_class='".addslashes($class)."'";
}
if($section!="" && $section!="all"){
$select = $select." and stu_section='".addslashes($section)."'";
}
if($subject!="" && $subject!="all"){
$select = $select." and (stu_subjects LIKE '%".addslashes($subject)."%')";
}
$select = $select.";";
$row=DB::select(DB::raw($select));
return $row;
}

public function getSection($mclass){
/*$select = "select distinct stu_section from ag_studentsdetails";//.self::getstudentstable();
if($mclass!="all"){
$select = $select." where stu_class='".addslashes($mclass)."'";
}
$select = $select.";";*/
$select = "select distinct feature_section as stu_section from ag_userkeys";
if($mclass!="all"){
$select=$select." where feature_class='".addslashes($mclass)."'";
}
return DB::select(DB::raw($select));
}

public function getSubject($groupname){//$mclass,$msection){
/*$select = "select distinct stu_subjects from ".self::getstudentstable();
if($mclass!="all"){
$select =$select." where stu_class='".addslashes($mclass)."'";
}
if($msection!="all"){
$select=$select." and stu_section='".addslashes($msection)."'";
}
$select = $select.";";*/
//$select= "select * from ag_clasubkey where clasubkey like 'subject%'";
//$select= "select a.clasubname as clasubname,a.clasubkey as clasubkey from ag_clasubkey a, ag_groups b where b.group_name='".urlencode(addslashes($groupname))."' and a.clasubkey like 'subject%' and a.clasubkey!=b.teacher_subject";
$output=array();
$output[0]="block";
$sel=DB::select(DB::raw("select * from ag_groups where ag_status='1' and group_name='".urlencode(addslashes($groupname))."'"));
$select="select * from ag_clasubkey where clasubkey like 'subject%'";
if(count($sel)>0){
$select=$select." and clasubkey not in ('0'";
for($i=0;$i<count($sel);$i++){
$select=$select.",'".$sel[$i]->teacher_subject."'";
if($sel[$i]->teacher_subject=="all"){
$output[0]="none";
}
}
$select=$select.")";
}
$output[1]=DB::select(DB::raw($select));
return $output;
}



public function groupUpdate($sstu,$gname,$uname,$subject,$class,$section){
$students = $sstu;
for($i=0;$i<count($students);$i++){
$query1 = "select * from ag_studentsdetails where stu_username='".urlencode(addslashes($students[$i]))."' and stu_subjects like '%".urlencode(addslashes($subject))."%'";
$result = DB::select(DB::raw($query1));

if(count($result)==0){
$query2 = "update ag_studentsdetails set stu_subjects = if(stu_subjects is null, '".urlencode(addslashes($subject)).",',concat(stu_subjects,'".urlencode(addslashes($subject)).",')) where stu_username = '".urlencode(addslashes($students[$i]))."'";
DB::update(DB::raw($query2));
}
}

$query="insert into ag_groups (group_name, teacher_username,group_class,group_section,teacher_subject, selection, group_icon) values ('".urlencode(addslashes($gname))."','".urlencode(addslashes($uname))."','".urlencode(addslashes($class))."','".urlencode(addslashes($section))."','".urlencode(addslashes($subject))."','','')";//".urlencode(addslashes(implode(',',$sstu)))."','')";
try{
DB::insert(DB::raw($query));

$selectrecent = DB::select(DB::raw("select slno from ag_groups where ag_status='1' and group_name='".urlencode(addslashes($gname))."' and teacher_username='".urlencode(addslashes($uname))."' and teacher_subject='".urlencode(addslashes($subject))."'"));

for($j=0;$j<count($students);$j++){
$query3 = "select * from ag_studentsdetails where stu_username='".urlencode(addslashes($students[$j]))."' and stu_gid like '%".urlencode(addslashes($selectrecent[0]->slno))."%'";
$result1 = DB::select(DB::raw($query3));
if(count($result1)==0){
$query4 = "update ag_studentsdetails set stu_gid = if(stu_gid is null, '".urlencode(addslashes($selectrecent[0]->slno)).",',concat(stu_gid,'".urlencode(addslashes($selectrecent[0]->slno)).",')) where stu_username = '".urlencode(addslashes($students[$j]))."'";
DB::update(DB::raw($query4));
}
}

//return $selectrecent;
return true;
}catch(Exception $e){
return false;
}

}

public function checkStudentfp(){
$query = DB::select(DB::raw("select * from ag_parentsdetails where par_username='".urlencode(addslashes(Session::get('username')))."' and userkey='".Session::get('token')."' and userid='".Session::get('userid')."' and stu_username is null"));
if(count($query)>0){
return true;
}else{
return false;
}
}

public function getkeyStudentsfp(){
$query=DB::select(DB::raw("select s.* from ag_studentsdetails s, ag_userkeys a where a.parentkey='".Session::get('token')."' and s.stu_class=a.feature_class and s.stu_section=a.feature_section"));
return $query;
}

public function updateKidfp($stu){
if(self::checkStudentfp()){
DB::update(DB::raw("update ag_parentsdetails set stu_username='".urlencode(addslashes($stu))."' where par_username='".urlencode(addslashes(Session::get('username')))."' and userkey='".Session::get('token')."' and userid='".Session::get('userid')."' and stu_username is null"));
return true;
}else{
return false;
}
}

public function pngetstudents($key){
$select=DB::select(DB::raw("select * from ag_userkeys where parentkey='".urlencode(addslashes($key))."'"));
if(count($select)){
$ins=DB::insert(DB::raw("insert into ag_parentsdetails (userid,par_username,par_fullname,userkey,par_section,par_class) values ('".urlencode(addslashes(Session::get('userid')))."','".urlencode(addslashes(Session::get('username')))."','".urlencode(addslashes(Session::get('fullname')))."','".urlencode(addslashes($key))."','".urlencode(addslashes($select[0]->feature_section))."','".urlencode(addslashes($select[0]->feature_class))."')"));
}
$result = DB::select(DB::raw("select a.* from ag_studentsdetails a,ag_userkeys b where a.stu_username not in (select stu_username from ag_parentsdetails where stu_username!=\"\") and b.feature_section=a.stu_section and b.feature_class=a.stu_class and parentkey='".urlencode(addslashes($key))."'"));
return $result;//"select a.* from ag_studentsdetails a,ag_userkeys b where b.feature_section=a.stu_section and b.feature_class=a.stu_class and parentkey='".urlencode(addslashes($key))."'";
}

public function pnaddkid($stu,$regc){
try {
DB::update(DB::raw("update ag_parentsdetails set stu_username='".urlencode(addslashes($stu))."' where userid='".urlencode(addslashes(Session::get('userid')))."' and userkey='".urlencode(addslashes($regc))."'"));

return true;
}catch(Exception $e){
return false;
}
}

public function gettSubjects(){
return DB::select(DB::raw("select teacher_subject from ag_groups where teacher_username='".Session::get('username')."' and teacher_subject!='all' and ag_status='1'"));
}

public function gettClasses(){
$class=array();
$k=0;
$grpclass=DB::select(DB::raw("select distinct group_class from ag_groups where teacher_username='".Session::get('username')."' and ag_status='1'"));
for($i=0;$i<count($grpclass);$i++){
if($grpclass[$i]->group_class){
$grpsection = DB::select(DB::raw("select distinct group_section from ag_groups where group_class='".$grpclass[$i]->group_class."' and teacher_username='".Session::get('username')."' and ag_status='1'"));
for($j=0;$j<count($grpsection);$j++){
$class[$k]['class']=$grpclass[$i]->group_class;
$class[$k]['section']=$grpsection[$j]->group_section;
$k++;
}
}
}
return $class;

}

public function getsClass($user=""){
if($user==""){
$user=Session::get('username');
}
return DB::select(DB::raw("select b.clasubname from ag_studentsdetails a,ag_clasubkey b where a.stu_username='".$user."' and a.stu_class=b.clasubkey"));
}

public function getsReport(){
return DB::select(DB::raw("select * from ag_examreports where stu_username='".Session::get('username')."'"));
}

public function getsAttendance($usr=""){
if($usr==""){
$usr=Session::get('username');
}
$atten=array();
$all = DB::select(DB::raw("select count(*) as total from ag_attendance where stu_uname='".$usr."' and (at_marked='1' or at_marked='0')"));
$abs = DB::select(DB::raw("select count(*) as total from ag_attendance where stu_uname='".$usr."' and at_marked='0'"));
$pre = DB::select(DB::raw("select count(*) as total from ag_attendance where stu_uname='".$usr."' and at_marked='1'"));
$atten['all']=$all[0]->total;
$atten['abs']=$abs[0]->total;
$atten['pre']=$pre[0]->total;
return $atten;

}

public function getsParent($usr=""){
if($usr==""){
$usr=Session::get('username');
}

//return DB::select(DB::raw("select * from ag_parentsdetails where stu_username='".$usr."'"));
return DB::select(DB::raw("select a.*,b.emailaddress from ag_parentsdetails a right join ag_user b on a.userid=b.slno where a.stu_username='".$usr."'"));
}

public function GetParstudents(){
return DB::select(DB::raw("select distinct a.stu_username,b.slno,b.fullname from ag_parentsdetails a,ag_user b where a.stu_username is not null and a.stu_username=b.username and a.par_username='".Session::get('username')."'"));
}

public function resetpassword($old,$new){
$res = DB::select(DB::raw("select * from ag_user where username='".Session::get('username')."' and password='".md5($old)."'"));
if(count($res)==1){
try{
DB::update(DB::raw("update ag_user set password='".md5($new)."' where username='".Session::get('username')."'"));
return true;
}catch(Exception $e){
return false;
}
}else{
return false;
}
}

}
