<?php
class Classfeed extends Eloquent {

private function hmulike($id,$usr){
$query1=DB::select(DB::raw("select * from ag_cflandc where ag_likeorcom='Thank' and ag_cfprikey='".urlencode(addslashes($id))."' and ag_uname='".urlencode(addslashes($usr))."'"));
if(count($query1)==1){
return true;
}else{
return false;
}}

private function hmlike($id,$usr){
$row=DB::select(DB::raw("select count(*) as pple from ag_cflandc where ag_likeorcom='Thank' and ag_cfprikey='".urlencode(addslashes($id))."' and ag_uname!='".urlencode(addslashes($usr))."'"));
$out="";
if(self::hmulike($id,$usr)==1){
$out=$out."You";
if($row[0]->pple!=0){
$out=$out." and ".$row[0]->pple." people thank this";
}else{
$out=$out." thank this";
}
}else{
if($row[0]->pple!=0){
$out=$out.$row[0]->pple." people thank this";
}
}
return $out;
}

public function loadcomments($sid,$usr){
$query=DB::select(DB::raw("select * from ag_cflandc where ag_status='1' and ag_cfprikey='".urlencode(addslashes($sid))."' and ag_likeorcom != 'Thank' order by ag_datetime desc;"));
for($i=0;$i<count($query);$i++){
if(self::secuthank($query[$i]->slno,$usr)){
$out = "Unthank";
}else{
$out = "Thank";
}
$query[$i]->secthank = $out.",".self::getsecthank($query[$i]->slno,$usr);
}
return $query;
}


public function shdcontentLoad($gnum,$usr,$count){
if(Session::has('privilege') && Session::get('privilege')=="student"){
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and a.ag_grpname in (".$gnum.") order by a.ag_datetime desc limit 10 offset ".$count.";"));
}else{
if(Session::has('privilege') && Session::get('privilege')=="parent"){
$student=DB::select(DB::raw("select stu_username from ag_studentsdetails where userid='".urlencode(addslashes($usr))."'"));
//$teachername=DB::select(DB::raw("select teacher_username from ag_groups where ag_status='1' and slno in (".$gnum.")"));
//$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and (a.ag_uname='".$teachername[0]->teacher_username."' or a.ag_uname='".$student[0]->stu_username."' or a.ag_uname='".Session::get('username')."') and a.ag_grpname='".urlencode(addslashes($gnum))."' order by a.ag_datetime desc;"));
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and (a.ag_uname=b.teacher_username or a.ag_uname='".$student[0]->stu_username."' or a.ag_uname='".Session::get('username')."') and a.ag_grpname in (".$gnum.") order by a.ag_datetime desc limit 10 offset ".$count.";"));
}else{
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and a.ag_grpname='".urlencode(addslashes($gnum))."' order by a.ag_datetime desc limit 10 offset ".$count.";"));
}
}
for($i=0;$i<count($query);$i++){
if(self::hmulike($query[$i]->slno,$usr)){
$query[$i]->ulike = "Unthank";
}else{
$query[$i]->ulike = "Thank";
}
$query[$i]->likemsg = self::hmlike($query[$i]->slno,$usr);
$query[$i]->comments = self::loadcomments($query[$i]->slno,$usr);
}
return $query;
}

public function anncontentLoad($gnum,$usr,$count){
if(Session::has('privilege') && Session::get('privilege')=="student"){
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and a.ag_announce='1' and a.ag_grpname in (".$gnum.") order by a.ag_datetime desc limit 10 offset ".$count.";"));
}else{

if(Session::has('privilege') && Session::get('privilege')=="parent"){
$student=DB::select(DB::raw("select stu_username from ag_studentsdetails where userid='".urlencode(addslashes($usr))."'"));
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and a.ag_announce='1' and (a.ag_uname=b.teacher_username or a.ag_uname='".$student[0]->stu_username."' or a.ag_uname='".Session::get('username')."') and a.ag_grpname in (".$gnum.") order by a.ag_datetime desc limit 10 offset ".$count.";"));
}else{
$query=DB::select(DB::raw("select a.*,b.teacher_subject from ag_classfeed a,ag_groups b where b.ag_status='1' and a.ag_grpname=b.slno and a.ag_status='1' and a.ag_announce='1' and a.ag_grpname='".urlencode(addslashes($gnum))."' order by a.ag_datetime desc limit 10 offset ".$count.";"));
}
}
for($i=0;$i<count($query);$i++){

if(self::hmulike($query[$i]->slno,$usr)){
$query[$i]->ulike = "Unthank";
}else{
$query[$i]->ulike = "Thank";
}
$query[$i]->likemsg = self::hmlike($query[$i]->slno,$usr);
$query[$i]->comments = self::loadcomments($query[$i]->slno,$usr);
}
return $query;
}

public function feedupdate($content, $grpnum,$suser,$announce,$filename,$image,$title,$desc){
$query=DB::insert(DB::raw("insert into ag_classfeed (ag_classfeed, ag_uname,ag_grpname,ag_announce,ag_status,ag_ffileorlink,ag_urlimage,ag_urltitle,ag_urldesc) values ('".urlencode(addslashes($content))."','".urlencode(addslashes($suser))."','".urlencode(addslashes($grpnum))."','".urlencode(addslashes($announce))."','1','".urlencode(addslashes($filename))."','".urlencode(addslashes($image))."','".urlencode(addslashes($title))."','".urlencode(addslashes($desc))."')"));
$inc=DB::select(DB::raw("select slno from ag_classfeed where ag_classfeed='".urlencode(addslashes($content))."' and ag_uname='".urlencode(addslashes($suser))."' and ag_grpname='".urlencode(addslashes($grpnum))."'"));
if($query){

return $inc[0]->slno;
}else{
return false;
}
}

public function deletefeed($id,$user){
try{
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
DB::update(DB::raw("update ag_classfeed set ag_status='0' where slno='".urlencode(addslashes($id))."'"));
}else{
DB::update(DB::raw("update ag_classfeed set ag_status='0' where slno='".urlencode(addslashes($id))."' and ag_uname='".urlencode(addslashes($user))."'"));
}
return "s";
}catch(Exception $e){
return "no";
}
}

public function likeUpdate($id,$usr){
$query=DB::select(DB::raw("select * from ag_cflandc where ag_uname='".urlencode(addslashes($usr))."' and ag_cfprikey='".urlencode(addslashes($id))."' and ag_likeorcom='Thank'"));
if(count($query)==0){
$query1 = DB::insert(DB::raw("insert into ag_cflandc (ag_uname,ag_likeorcom,ag_cfprikey,ag_status) values ('".urlencode(addslashes($usr))."','Thank','".urlencode(addslashes($id))."','1');"));
if($query1){
return "Unthank,".self::hmlike($id,$usr);
}
}else{
$query1= DB::delete(DB::raw("delete from ag_cflandc where ag_uname='".urlencode(addslashes($usr))."' and ag_cfprikey='".urlencode(addslashes($id))."' and ag_likeorcom='Thank'"));

return "Thank,".self::hmlike($id,$usr);

}
}

public function commentUpdate($id,$cont,$usr){
$ent=explode('_',$id);
$query1 = DB::insert(DB::raw("insert into ag_cflandc (ag_uname,ag_likeorcom,ag_cfprikey,ag_status) values ('".urlencode(addslashes($usr))."','".urlencode(addslashes($cont))."','".urlencode(addslashes($ent[2]))."','1');"));

$inc=DB::select(DB::raw("select slno from ag_cflandc where ag_uname='".urlencode(addslashes($usr))."' and ag_likeorcom='".urlencode(addslashes($cont))."' and ag_cfprikey='".urlencode(addslashes($ent[2]))."'"));


if($query1){
return $inc[0]->slno;
}else{
return false;
}
}

private function secuthank($id,$usr){
$query1=DB::select(DB::raw("select * from ag_secthank where ag_lcprikey='".urlencode(addslashes($id))."' and ag_uname='".urlencode(addslashes($usr))."'"));
if(count($query1)==1){
return true;
}else{
return false;
}
}

private function getsecthank($id,$usr){
$row=DB::select(DB::raw("select count(*) as pple from ag_secthank where ag_lcprikey='".urlencode(addslashes($id))."' and ag_uname!='".urlencode(addslashes($usr))."'"));
$out="";
if(self::secuthank($id,$usr)==1){
$out=$out."You";
if($row[0]->pple!=0){
$out=$out." and ".$row[0]->pple." people thank this";
}else{
$out=$out." thank this";
}
}else{
if($row[0]->pple!=0){
$out=$out.$row[0]->pple." people thank this";
}
}
return $out;
}

public function secthank($id,$usr){
$query=DB::select(DB::raw("select * from ag_secthank where ag_uname='".urlencode(addslashes($usr))."' and ag_lcprikey='".urlencode(addslashes($id))."'"));

if(count($query)==0){
$query1 = DB::insert(DB::raw("insert into ag_secthank (ag_uname,ag_lcprikey) values ('".urlencode(addslashes($usr))."','".urlencode(addslashes($id))."');"));
if($query1){
return "Unthank,".self::getsecthank($id,$usr);
}
}else{
$query1= DB::delete(DB::raw("delete from ag_secthank where ag_uname='".urlencode(addslashes($usr))."' and ag_lcprikey='".urlencode(addslashes($id))."'"));

return "Thank,".self::getsecthank($id,$usr);
}
}


public function secdelete($id,$usr){
try{
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
DB::update(DB::raw("update ag_cflandc set ag_status='0' where slno='".urlencode(addslashes($id))."'"));
}else{
DB::update(DB::raw("update ag_cflandc set ag_status='0' where slno='".urlencode(addslashes($id))."' and ag_uname='".urlencode(addslashes($usr))."'"));
}
return "s";
}catch(Exception $e){
return "no";
}

}




}

