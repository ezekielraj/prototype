<?php
class Calender extends Eloquent {

//public function getallevents($user,$gn){

//}

public function monthlyEvents($dater,$user,$gn=""){
$dat = explode(" ",$dater);
$day = date("d",strtotime($dat[1]."-".$dat[2]."-".$dat[3]));
//$new=DB::select(DB::raw("select * from ag_events where ag_eventuser='".urlencode(addslashes($user))."' and (ag_eventsrepeat='3') and ag_grpnum in (".$gn.")"));
if(Session::has('privilege') && Session::get('privilege')=="parent"){
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='3') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator') and ag_wtshare='2'"));
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='3')"));
}else{
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='3') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator')"));
}
for($i=0;$i<count($new);$i++){
$fromdate = explode(" ",$new[$i]->ag_fromdate);
$todate = explode(" ",$new[$i]->ag_todate);
if(date("d",strtotime($fromdate[0]))!=$day || date("d",strtotime($fromdate[0]))!=$day){
unset($new[$i]);
}
}
return $new;
}

public function weeklyEvents($dater,$user,$gn=""){
$dat = explode(" ",$dater);
$day = date("l",strtotime($dat[1]."-".$dat[2]."-".$dat[3]));
//$new=DB::select(DB::raw("select * from ag_events where ag_eventuser='".urlencode(addslashes($user))."' and (ag_eventsrepeat='2') and ag_grpnum in (".$gn.")"));
if(Session::has('privilege') && Session::get('privilege')=="parent"){
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='2') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator') and ag_wtshare='2'"));
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='2')"));
}else{
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='2') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator')"));
}
for($i=0;$i<count($new);$i++){
$fromdate = explode(" ",$new[$i]->ag_fromdate);
$todate = explode(" ",$new[$i]->ag_todate);
if(date("l",strtotime($fromdate[0]))!=$day || date("l",strtotime($fromdate[0]))!=$day){
unset($new[$i]);
}
}
return $new;
}

public function dailyEvents($dater,$user,$gn=""){
$dat = explode(" ",$dater);
$date = date("Y-m-d",strtotime($dat[1]."-".$dat[2]."-".$dat[3]));
if(Session::has('privilege') && Session::get('privilege')=="parent"){
$new = DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='1') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator') and ag_wtshare='2'"));
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$new=DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='1')"));
}else{
$new = DB::select(DB::raw("select * from ag_events where (ag_eventsrepeat='1') and (ag_grpnum in (".$gn.") or ag_eventuser='administrator')"));
}
//$new = DB::select(DB::raw("select * from ag_events where ag_eventuser='".urlencode(addslashes($user))."' and (ag_eventsrepeat='1') and ag_grpnum in (".$gn.")"));
return $new;
}



public function getEvents($dater,$user,$gn=""){
$dat = explode(" ",$dater);
$date = date("Y-m-d",strtotime($dat[1]."-".$dat[2]."-".$dat[3]));
//$new = DB::select(DB::raw("select * from ag_events where ag_eventuser='".urlencode(addslashes($user))."' and (ag_fromdate like '%".$date."%' or ag_todate like '%".$date."%') and ag_eventsrepeat='0' and ag_grpnum in (".$gn.")"));
if(Session::has('privilege') && Session::get('privilege')=="parent"){
$new = DB::select(DB::raw("select * from ag_events where (ag_fromdate like '%".$date."%' or ag_todate like '%".$date."%') and ag_eventsrepeat='0' and (ag_grpnum in (".$gn.") or ag_eventuser='administrator') and ag_wtshare='2'"));
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$new = DB::select(DB::raw("select * from ag_events where (ag_fromdate like '%".$date."%' or ag_todate like '%".$date."%') and ag_eventsrepeat='0'"));
}else{
$new = DB::select(DB::raw("select * from ag_events where (ag_fromdate like '%".$date."%' or ag_todate like '%".$date."%') and ag_eventsrepeat='0' and (ag_grpnum in (".$gn.") or ag_eventuser='administrator')"));
}
return $new;

}
public function createEvent($title,$description,$fromdate,$todate,$fromhours,$frommin,$fromampm,$tohours,$tomin,$toampm,$repeat,$grpnum,$user,$wtshare,$id){
$fromdat = date("Y-m-d", strtotime($fromdate));
$fromtime = date("H:i:s", strtotime($fromhours.":".$frommin." ".$fromampm));
$fromfinal = date($fromdat." ".$fromtime);

$todat = date("Y-m-d", strtotime($todate));
$totime = date("H:i:s", strtotime($tohours.":".$tomin." ".$toampm));
$tofinal = date($todat." ".$totime);
if(Session::has('privilege') && Session::get('privilege')=="administrator"){
$user="administrator";
$grpnum=0;
}
//$select=DB::select(DB::raw("select count(*) as total from ag_events where ag_eventtitle='".urlencode(addslashes($title))."' and ag_eventdesc='".urlencode(addslashes($description))."' and ag_eventuser='".urlencode(addslashes($user))."'"));
//if($select[0]->total == '1'){
if($id){
try{
DB::update(DB::raw("update ag_events set ag_fromdate='".$fromfinal."',ag_todate='".$tofinal."',ag_eventsrepeat='".urlencode(addslashes($repeat))."',ag_eventtitle='".urlencode(addslashes($title))."',ag_eventdesc='".urlencode(addslashes($description))."',ag_wtshare='".urlencode(addslashes($wtshare))."' where ag_eventuser='".urlencode(addslashes($user))."' and slno='".urlencode(addslashes($id))."'"));
return true;
}catch(Exception $e){
return false;
}
}else{
//$new = "insert into ag_events (ag_eventtitle,ag_fromdate,ag_todate,ag_eventdesc,ag_eventuser,ag_grpnum,ag_eventsrepeat,ag_wtshare) values ('".urlencode(addslashes($title))."','".$fromfinal."','".$tofinal."','".urlencode(addslashes($description))."','".urlencode(addslashes($user))."','".urlencode(addslashes($grpnum))."','".urlencode(addslashes($repeat))."','".urlencode(addslashes($wtshare))."')";
$insert=DB::insert(DB::raw("insert into ag_events (ag_eventtitle,ag_fromdate,ag_todate,ag_eventdesc,ag_eventuser,ag_grpnum,ag_eventsrepeat,ag_wtshare) values ('".urlencode(addslashes($title))."','".$fromfinal."','".$tofinal."','".urlencode(addslashes($description))."','".urlencode(addslashes($user))."','".urlencode(addslashes($grpnum))."','".urlencode(addslashes($repeat))."','".urlencode(addslashes($wtshare))."')"));

if($insert){
return true;
}else{
return false;
}
}
}

public function getEventDates($dater,$grpnum=""){
if($dater==""){
$dater=date('Y-m-')."01";
}
$datemonth = strtotime(date("Y-m-d", strtotime($dater)) . "+1 month");
$enddate = date("Y-m-d",$datemonth);
if(Session::has('privilege') && Session::get('privilege')=="teacher"){
$select=DB::select(DB::raw("select * from ag_events where ag_fromdate > '".$dater."' and ag_fromdate < '".$enddate."' and ag_eventuser='".Session::get('username')."' or ag_eventuser='administrator' and ag_grpnum in (".$grpnum.")"));
}elseif(Session::has('privilege') && Session::get('privilege')=="student"){
$gn = DB::select(DB::raw("select stu_gid from ag_studentsdetails where stu_username='".Session::get('username')."'"));
$j=explode(",",$gn[0]->stu_gid);
$k="";
for($i=0;$i<count($j);$i++){
if($j[$i]!=""){
if($k==""){
$k=$j[$i];
}else{
$k=$k.",".$j[$i];
}
}
}
$select=DB::select(DB::raw("select * from ag_events where ag_fromdate > '".$dater."' and ag_fromdate < '".$enddate."' and ag_grpnum in (".$k.")"));
}elseif(Session::has('privilege') && Session::get('privilege')=="administrator"){
$select=DB::select(DB::raw("select * from ag_events where ag_fromdate > '".$dater."' and ag_fromdate < '".$enddate."'"));

}elseif(Session::has('privilege') && Session::get('privilege')=="parent"){
$st = DB::select(DB::raw("select * from ag_parentsdetails where par_username='".Session::get('username')."'"));
$gn = DB::select(DB::raw("select stu_gid from ag_studentsdetails where stu_username='".$st[0]->stu_username."'"));
$j=explode(",",$gn[0]->stu_gid);
$k="";
for($i=0;$i<count($j);$i++){
if($j[$i]!=""){
if($k==""){
$k=$j[$i];
}else{
$k=$k.",".$j[$i];
}
}
}
$select=DB::select(DB::raw("select * from ag_events where ag_fromdate > '".$dater."' and ag_fromdate < '".$enddate."' and ag_grpnum in (".$k.")"));
}
return $select;
}
}
?>
