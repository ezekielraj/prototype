<?php
class Assignment extends Eloquent {

public function CreateAssign($astitle,$startdate,$enddate,$description,$filename,$grpnum){
$query = DB::insert(DB::raw("insert into ag_assignments (as_title,as_startdate,as_enddate,as_description,as_attachment,as_groupnum) values ('".urlencode(addslashes($astitle))."','".addslashes($startdate)."','".addslashes($enddate)."','".urlencode(addslashes($description))."','".urlencode(addslashes($filename))."','".urlencode(addslashes($grpnum))."')"));
if($query){
return true;
}else{
return false;
}
}

public function UpdateAssign($asid,$astitle,$startdate,$enddate,$description,$filename,$grpnum){
try{
$q = "update ag_assignments set as_title='".urlencode(addslashes($astitle))."',as_startdate='".addslashes($startdate)."',as_enddate='".addslashes($enddate)."',as_description='".urlencode(addslashes($description))."'";
if($filename!=""){
$q=$q.",as_attachment='".urlencode(addslashes($filename))."'";
}
$q=$q." where as_groupnum='".urlencode(addslashes($grpnum))."' and as_id='".urlencode(addslashes($asid))."'";
$query = DB::update(DB::raw($q));
return true;
}catch(Exception $e){
return false;
}
}
public function getassignwithid($id,$groupnum){
return DB::select(DB::raw("select * from ag_assignments where as_groupnum='".urlencode(addslashes($groupnum))."' and as_id='".urlencode(addslashes($id))."'"));
}
public function getassignments($offset, $groupnum){
//return "select * from ag_assignments where as_groupnum='".urlencode(addslashes($groupnum))."' limit 10 offset '".urlencode(addslashes($offset))."'";
if(Session::get('privilege')=="student" || Session::get('privilege')=="parent"){

return DB::select(DB::raw("select a.* from ag_assignments a,ag_groups b where a.as_groupnum in (".$groupnum.") and a.as_groupnum=b.slno and b.ag_status=1 order by a.as_currentdate desc limit 10 offset ".urlencode(addslashes($offset))));

}else{
return DB::select(DB::raw("select a.* from ag_assignments a,ag_groups b where a.as_groupnum='".urlencode(addslashes($groupnum))."' and a.as_groupnum=b.slno and b.ag_status=1 order by as_currentdate desc limit 10 offset ".urlencode(addslashes($offset))));
}
//return "hi";
}

public function delassignment($id,$groupnum){
$result=DB::delete(DB::raw("delete from ag_assignments where as_groupnum='".urlencode(addslashes($groupnum))."' and as_id='".urlencode(addslashes($id))."'"));
return $result;
}
private function checkassignmark($stu, $grp, $aid){
return DB::select(DB::raw("select * from ag_assignmark where stu_uname='".urlencode(addslashes($stu))."' and as_id='".urlencode(addslashes($aid))."' and groupnum='".urlencode(addslashes($grp))."'"));
}
public function assignmark($stu,$grp,$aid){
$j=0;
for($i=0;$i<count($stu);$i++){
$studd = explode(',',$stu[$i]);
$select = self::checkassignmark($studd[0],$grp,$aid);
$mark=explode('_',$studd[1]);
if(count($select)){
try{
DB::update(DB::raw("update ag_assignmark set mas_mark='".urlencode(addslashes($mark[0]))."',mas_outof='".urlencode(addslashes($mark[1]))."' where stu_uname='".urlencode(addslashes($studd[0]))."' and as_id='".urlencode(addslashes($aid))."' and groupnum='".urlencode(addslashes($grp))."'"));
$j=$j+1;
}catch(Exception $e){
}
}else{
$ins = DB::insert(DB::raw("insert into ag_assignmark (stu_uname,mas_mark,mas_outof,as_id,groupnum) values ('".urlencode(addslashes($studd[0]))."','".urlencode(addslashes($mark[0]))."','".urlencode(addslashes($mark[1]))."','".urlencode(addslashes($aid))."','".urlencode(addslashes($grp))."')"));
if($ins){
$j=$j+1;
}
}
}
if($j==count($stu)){
return "s";
}else{
return "no";
}


}

public function getassignmarks($aid,$gid){
if(Session::get('privilege')=="parent"){
$select = DB::select(DB::raw("select stu_uname,mas_mark,mas_outof from ag_assignmark where as_id='".urlencode(addslashes($aid))."'"));
}else{
$select = DB::select(DB::raw("select stu_uname,mas_mark,mas_outof from ag_assignmark where as_id='".urlencode(addslashes($aid))."' and groupnum='".urlencode(addslashes($gid))."'"));
}
return $select;
}


}
?>
