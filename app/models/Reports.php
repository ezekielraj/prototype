<?php
require_once('selectTables.php');
class Reports extends Eloquent {
use selectTables;

public function getExams(){
$select = "select * from ag_clasubkey where clasubkey like '%exam_%' order by slno asc";
return DB::select(DB::raw($select));
}
private function additionalgetstudents($slno){
$sel = DB::select(DB::raw("select stu_username from ag_studentsdetails where stu_gid like '%,".$slno.",%'"));
$result="";
for($i=0;$i<count($sel);$i++){
if($result){
$result=$result.",".stripslashes(urldecode($sel[$i]->stu_username));
}else{
$result=stripslashes(urldecode($sel[$i]->stu_username));
}
}
return $result;
}

public function getStudents($class,$subjectname){
//$select = "select selection from ag_groups where group_name = '".urlencode(addslashes($class))."' and teacher_username='".Session::get('username')."' and teacher_subject = '".urlencode(addslashes($subjectname))."'";
$select = "select slno from ag_groups where ag_status='1' and group_name = '".urlencode(addslashes($class))."' and teacher_username='".Session::get('username')."' and teacher_subject = '".urlencode(addslashes($subjectname))."'";
$student = DB::select(DB::raw($select));
$students = self::additionalgetstudents($student[0]->slno);
$stu = explode(',',$students);
return $stu;
}

public function getResults($class,$exam,$subjectname,$comment){
$stu = self::getStudents($class,$subjectname);
$results = array();
$j=0;
for($i=0;$i<count($stu);$i++){
$select = DB::select(DB::raw("select stu_username,stu_mark,stu_outofmark from ag_examreports where exam_name='".urlencode(addslashes($exam))."' and stu_username='".urlencode(addslashes($stu[$i]))."' and stu_subjectname='".urlencode(addslashes($subjectname))."'"));
if(count($select)){
$results[$j]['stu_username'] = stripslashes(urldecode($select[0]->stu_username));
$results[$j]['stu_mark'] = $select[0]->stu_mark;
$results[$j]['stu_outofmark'] = $select[0]->stu_outofmark;
$j++;
}else{
if($comment=="addedit"){
$results[$j]['stu_username'] = urlencode(addslashes($stu[$i]));
$results[$j]['stu_mark'] = "";
$results[$j]['stu_outofmark'] = "100";
$j++;
}
}
}
return $results;
}
private function checkentry($stuname,$exam,$sub){
$select = DB::select(DB::raw("select * from ag_examreports where exam_name='".urlencode(addslashes($exam))."' and stu_username='".urlencode(addslashes($stuname))."' and stu_subjectname='".urlencode(addslashes($sub))."'"));
if(count($select)){
return true;
}else{
return false;
}
}
public function updatemark($stuname,$exam,$sub,$mark){
if(self::checkentry($stuname,$exam,$sub)){
$entry = DB::update(DB::raw("update ag_examreports set stu_mark = '".urlencode(addslashes($mark))."' where exam_name='".urlencode(addslashes($exam))."' and stu_username='".urlencode(addslashes($stuname))."' and stu_subjectname='".urlencode(addslashes($sub))."'"));
}else{
$entry = DB::insert(DB::raw("insert into ag_examreports (exam_name,stu_username,stu_subjectname,stu_mark) values ('".urlencode(addslashes($exam))."','".urlencode(addslashes($stuname))."','".urlencode(addslashes($sub))."','".urlencode(addslashes($mark))."')"));
}
if($entry){
return true;
}else{
return false;
}
}

public function updateoutofmark($stuname,$exam,$sub,$mark){
if(self::checkentry($stuname,$exam,$sub)){
$entry = DB::update(DB::raw("update ag_examreports set stu_outofmark = '".urlencode(addslashes($mark))."' where exam_name='".urlencode(addslashes($exam))."' and stu_username='".urlencode(addslashes($stuname))."' and stu_subjectname='".urlencode(addslashes($sub))."'"));
}else{
$entry = DB::insert(DB::raw("insert into ag_examreports (exam_name,stu_username,stu_subjectname,stu_outofmark) values ('".urlencode(addslashes($exam))."','".urlencode(addslashes($stuname))."','".urlencode(addslashes($sub))."','".urlencode(addslashes($mark))."')"));
}
if($entry){
return true;
}else{
return false;
}
}

//public function getExams(){
//$exams = DB::select(DB::raw("select * from ag_clasubkey where clasubkey like '%exam_%' order by slno asc"));
//return $exams;/
//}

public function getResultsall($exam,$usr=""){
if($usr==""){
$result=DB::select(DB::raw("select distinct stu_subjectname,exam_name,stu_username,stu_mark,stu_outofmark from ag_examreports where exam_name='".$exam."' and stu_username='".Session::get('username')."' order by stu_subjectname asc"));
}else{
$result=DB::select(DB::raw("select distinct stu_subjectname,exam_name,stu_username,stu_mark,stu_outofmark from ag_examreports where exam_name='".$exam."' and stu_username='".$usr."' order by stu_subjectname asc"));
}
return $result;
}

public function getSubjects($usr=""){
if($usr==""){
return DB::select(DB::raw("select distinct stu_subjectname from ag_examreports where stu_username='".Session::get('username')."' order by stu_subjectname asc"));
}else{
return DB::select(DB::raw("select distinct stu_subjectname from ag_examreports where stu_username='".$usr."' order by stu_subjectname asc"));

}
}


public function getallResults($class,$exam,$subjectname,$comment){
$stu = self::getStudents($class,$subjectname);
$results = array();
$j=0;
for($i=0;$i<count($stu);$i++){
$select = DB::select(DB::raw("select stu_username,stu_mark,stu_outofmark,stu_subjectname from ag_examreports where exam_name='".urlencode(addslashes($exam))."' and stu_username='".urlencode(addslashes($stu[$i]))."'"));
$results[$i]=$select;
}
return $results;
}

public function updaterank($exam,$stuname,$overallper){
$query=DB::select(DB::raw("select * from ag_ranklist where exam_key='".urlencode(addslashes($exam))."' and studentname='".urlencode(addslashes($stuname))."'"));
if(count($query)>0){
$result=DB::update(DB::raw("update ag_ranklist set overallper='".urlencode(addslashes($overallper))."',overallrank='F' where exam_key='".urlencode(addslashes($exam))."' and studentname='".urlencode(addslashes($stuname))."'"));
}else{
$result=DB::insert(DB::raw("insert into ag_ranklist (exam_key,studentname,overallper,overallrank) values ('".urlencode(addslashes($exam))."','".urlencode(addslashes($stuname))."','".urlencode(addslashes($overallper))."','F')"));
}
}

public function updateorank($exam,$stuname,$rank){
$result=DB::update(DB::raw("update ag_ranklist set overallrank='".urlencode(addslashes($rank))."' where exam_key='".urlencode(addslashes($exam))."' and studentname='".urlencode(addslashes($stuname))."'"));
}

public function getpassper($exam){
return DB::select(DB::raw("select * from ag_passmark where exam_key='".urlencode(addslashes($exam))."'"));
}

public function getrank($exam,$stuname){
return DB::select(DB::raw("select * from ag_ranklist where exam_key='".urlencode(addslashes($exam))."' and studentname='".urlencode(addslashes($stuname))."'"));
}

public function getlatestrank($stu){
return DB::select(DB::raw("select * from ag_ranklist where studentname='".urlencode(addslashes($stu))."' order by slno desc limit 1;"));
}

}
?>
