<?php
class Admin extends Eloquent {

public function availableclasses(){
$select = DB::select(DB::raw("select * from ag_clasubkey where clasubkey like '%class_%'"));
return $select;
}

private function dbcheck($csk){
$select = DB::select(DB::raw("select * from ag_userkeys where feature_class='".$csk."'"));
if(count($select)>0){
return true;
}else{
return false;
}
}

public function chkcode($csk){
$select = DB::select(DB::raw("select * from ag_userkeys a,ag_teacherkey b where a.studentkey='".$csk."' or a.parentkey='".$csk."' or b.teacherkey='".$csk."'"));
if(count($select)>0){
return true;
}else{
return false;
}
}

public function clearCode($std,$sec){
try{
$query=DB::delete(DB::raw("update ag_userkeys set studentkey=NULL, parentkey=NULL where feature_class='".$std."' and feature_section='".$sec."'"));
return "s";
}catch(exception $e){
return "no";
}
}

public function gettkey($std,$sec,$code,$code1){
$query = "select * from ag_userkeys where feature_class='".$std."'";
if(trim($sec)!=""){
$query=$query." and feature_section='".$sec."'";
}else{
$query=$query." and feature_section is null";
}
$select = DB::select(DB::raw($query));
if($select[0]->studentkey!="" && $select[0]->parentkey!=""){
return $select;
}else{
try{
$query1 = "update ag_userkeys set studentkey='".$code."',parentkey='".$code1."' where feature_class='".$std."'";
if(trim($sec)!=""){
$query1=$query1." and feature_section='".$sec."'";
}else{
$query1=$query1." and feature_section is null";
}
$select[0]->studentkey=$code;
$select[0]->parentkey=$code1;
DB::update(DB::raw($query1));
return $select;
}catch(exception $e){
return null;
}

}
}

public function checkkey($std,$sec){
$query = "select * from ag_userkeys where feature_class='".$std."'";
if(trim($sec)!=""){
$query=$query." and feature_section='".$sec."'";
}else{
$query=$query." and feature_section is null";
}
$select = DB::select(DB::raw($query));
return $select;
}
public function createsection($code){//,$key){
$select = DB::select(DB::raw("select * from ag_userkeys where (feature_class='".$code."' and feature_section IS NOT NULL) order by feature_section asc"));
$last=count($select);
if($last){
$n=explode('_',$select[$last - 1]->feature_section);
$alpha=++$n[1];
}else{
$alpha="a";
}
//return "insert into ag_userkeys (feature_class,feature_section,featurekey) values ('".$code."','section_".$alpha."','".$key."'";
$create=DB::insert(DB::raw("insert into ag_userkeys (feature_class,feature_section) values ('".$code."','section_".$alpha."')"));
if($create){
return "s";
}else{
return "no";
}
}

public function deletesection($code){//,$key){

$select = DB::select(DB::raw("select * from ag_userkeys where (feature_class='".$code."' and feature_section IS NOT NULL) order by feature_section asc"));
$last=count($select);
if($last){
$n=explode('_',$select[$last - 1]->feature_section);
$alpha=$n[1];
//}else{
//$alpha="a";
//}
//return "insert into ag_userkeys (feature_class,feature_section,featurekey) values ('".$code."','section_".$alpha."','".$key."'";
$create=DB::delete(DB::raw("delete from ag_userkeys where feature_class='".$code."' and feature_section='section_".$alpha."'"));
if($create){
return "s";
}else{
return "no";
}
}else{
return "s";
}
}

public function getsections($code){
$select =DB::select(DB::raw("select * from ag_userkeys where feature_class='".$code."'"));
return $select;
}

/*public function codeUpdate($code,$key){
$select=DB::select(DB::raw("select * from ag_userkeys where feature='".$key."' and featurekey is not null"));
if(count($select)=='0'){
DB::update(DB::raw("update ag_userkeys set featurekey='".$code."' where feature_class='".$key."'"));
return true;
}
}*/

public function codegenerator(){
$aclass=self::availableclasses();
if(count($aclass)>0){
for($i=0;$i<count($aclass);$i++){
$check=self::dbcheck($aclass[$i]->clasubkey);
if(!$check){
DB::insert(DB::raw("insert into ag_userkeys (feature_class) values ('".$aclass[$i]->clasubkey."')"));
}
}
}
$select = DB::select(DB::raw("select distinct a.feature_class,b.clasubname as clasubname from ag_userkeys a,ag_clasubkey b where a.feature_class=b.clasubkey"));
return $select;
}

public function teacherkey(){
$select=DB::select(DB::raw("select * from ag_teacherkey where slno='1'"));
return $select;
}

public function insertteacherkey($code){
$select=DB::select(DB::raw("select * from ag_teacherkey where slno='1'"));
if(count($select)==1){
DB::update(DB::raw("update ag_teacherkey set teacherkey='$code' where slno='1'"));
$insert=true;
}else{
$insert=DB::insert(DB::raw("insert into ag_teacherkey (teacherkey) values ('".$code."')"));
}
if($insert){
return true;
}else{
return false;
}
}

public function getUsers($priv="",$name="",$class="",$section=""){
if($priv=="student"){
$query ="select * from ag_user left join ag_studentsdetails on ag_user.username=ag_studentsdetails.stu_username where ag_user.privilege='student' and ag_studentsdetails.stu_class";
if($class=="%"){
$query=$query." like '%%%'";
}else{
$query=$query." like '%".urlencode(addslashes($class))."%'";
}
if($section=="%"){
$query=$query." and ag_studentsdetails.stu_section like '%%%'";
}else{
$query=$query." and ag_studentsdetails.stu_section like '%".urlencode(addslashes($section))."%'";
}
if($name!=""){
$query=$query." and (ag_user.fullname like '%".urlencode(addslashes($name))."%' or ag_user.username like '%".urlencode(addslashes($name))."%' or ag_user.emailaddress like '%".urlencode(addslashes($name))."%')";
}
}else{
$query="select * from ag_user";
if($priv!="" && $priv!="all"){
$query=$query." where privilege='".urlencode(addslashes($priv))."'";
}
if($priv=="all" && $name!=""){
$query=$query." where ";
}
if($priv!="all" && $name!=""){
$query=$query." and ";
}
if($name!=""){
$query=$query."(fullname like '%".urlencode(addslashes($name))."%' or username like '%".urlencode(addslashes($name))."%' or emailaddress like '%".urlencode(addslashes($name))."%')";
}
}

$select=DB::select(DB::raw($query));
return $select;
}

public function addclass($cname="",$ckey=""){
$query="insert into ag_clasubkey (clasubname,clasubkey) values ('".urlencode(addslashes($cname))."','".urlencode(addslashes("class_".$ckey))."')";
$insert=DB::insert(DB::raw($query));
return $insert;
}

public function addsubject($sname="",$skey=""){
$query="insert into ag_clasubkey (clasubname,clasubkey) values ('".urlencode(addslashes($sname))."','".urlencode(addslashes("subject_".$skey))."')";
$insert=DB::insert(DB::raw($query));
return $insert;
}

public function addexam($ename="",$ekey=""){
$query="insert into ag_clasubkey (clasubname,clasubkey) values ('".urlencode(addslashes($ename))."','".urlencode(addslashes("exam_".$ekey))."')";
$insert=DB::insert(DB::raw($query));
return $insert;
}

public function addexampassper($ekey,$epass){
$query="insert into ag_passmark (exam_key,exam_passmark) values ('exam_".urlencode(addslashes($ekey))."','".urlencode(addslashes($epass))."')";
$insert=DB::insert(DB::raw($query));
return $insert;
}

public function delexampassper($ekey){
$query=DB::delete(DB::raw("delete from ag_passmark where exam_key='".$ekey."'"));
return $query;
}

public function deleteclass($id){
$query="delete from ag_clasubkey where slno='".urlencode(addslashes($id))."'";
$del=DB::delete(DB::raw($query));
return $del;
}

public function userstatuschange($slno,$status){
try{
DB::update(DB::raw("update ag_user set status='".$status."' where slno='".$slno."'"));
return true;
}catch(Exception $e){
}
}

public function createuser($fullname,$username,$password,$priv,$email,$key){
$insert=DB::insert(DB::raw("insert into ag_user (fullname,username,password,privilege,emailaddress,token,status) values ('".urlencode(addslashes($fullname))."','".urlencode(addslashes($username))."','".md5($password)."','".urlencode(addslashes($priv))."','".urlencode(addslashes($email))."','".urlencode(addslashes($key))."',1)"));
if($insert){
return true;
}else{
return false;
}
}


public function getuserslno($username,$email){
$select=DB::select(DB::raw("select slno from ag_user where username='".urlencode(addslashes($username))."' and emailaddress='".urlencode(addslashes($email))."'"));
return $select;
}

public function regstudent($userid,$username,$fname,$class,$section){
$insert=DB::insert(DB::raw("insert into ag_studentsdetails (userid,stu_username,stu_fullname,stu_class,stu_section,stu_gid) values ('".urlencode(addslashes($userid))."','".urlencode(addslashes($username))."','".urlencode(addslashes($fname))."','".urlencode(addslashes($class))."','".urlencode(addslashes($section))."','0,')"));
if($insert){
return true;
}else{
return false;
}
}

public function regteacher($userid,$username){
$insert=DB::insert(DB::raw("insert into ag_teachersdetails (userid,teacher_username) values ('".urlencode(addslashes($userid))."','".urlencode(addslashes($username))."')"));
if($insert){
return true;
}else{
return false;
}


}

public function regparent($userid,$uname,$fname,$ukey,$sec,$class){
$insert=DB::insert(DB::raw("insert into ag_parentsdetails (userid,par_username,par_fullname,userkey,par_section,par_class) values ('".urlencode(addslashes($userid))."','".urlencode(addslashes($uname))."','".urlencode(addslashes($fname))."','".urlencode(addslashes($ukey))."','".urlencode(addslashes($sec))."','".urlencode(addslashes($class))."')"));
if($insert){
return true;
}else{
return false;
}

}

public function getkeyy($class,$section,$role){
$query = "select ".$role."key from ag_userkeys where feature_class='".urlencode(addslashes($class))."' and feature_section";
if($section!=""){
$query=$query."='".urlencode(addslashes($section))."'";
}else{
$query=$query." is null";
}
$select=DB::select(DB::raw($query));//"select ".$role."key from ag_userkeys where feature_class='".urlencode(addslashes($class))."' and feature_section='".urlencode(addslashes($section))."'"));
return $select;
}

public function selectclasubkey($slno){
$query="select * from ag_clasubkey where slno='".urlencode(addslashes($slno))."' and clasubkey like 'exam_%'";
return DB::select(DB::raw($query));
}

}


?>
