<?php
require_once('selectTables.php');
class Students extends Eloquent {

use selectTables;

public function getGroupName($groupnum){
$row=DB::select(DB::raw("select group_name from ag_groups where ag_status='1' and slno='".$groupnum."'"));
return stripslashes(urldecode($row[0]->group_name));
}

public function getSubjectName($subnum){
$row = DB::select(DB::raw("select clasubname from ag_clasubkey where slno='".$subnum."'"));
return stripslashes(urldecode($row[0]->clasubname));
}

public function getSubjectKey($subnum){
$row = DB::select(DB::raw("select clasubkey from ag_clasubkey where slno='".$subnum."'"));
return stripslashes(urldecode($row[0]->clasubkey));
}
private function additionalgetstudents($slno){
$sel = DB::select(DB::raw("select stu_username from ag_studentsdetails where stu_gid like '%,".$slno.",%'"));
$result="";
for($i=0;$i<count($sel);$i++){
if($result){
$result=$result.",".stripslashes(urldecode($sel[$i]->stu_username));
}else{
$result=stripslashes(urldecode($sel[$i]->stu_username));
}
}
return $result;
}

public function getgrpStudents($grpname,$uname){
if(Session::has('privilege') && Session::get('privilege')=='teacher'){
$row = DB::select(DB::raw("select * from ag_groups where ag_status='1' and teacher_username='".urlencode(addslashes($uname))."' and (group_name='".urlencode(addslashes(trim($grpname)))."' or slno='".urlencode(addslashes(trim($grpname)))."')"));
if(count($row)>0){
//return $row;
return self::additionalgetstudents($row[0]->slno);
}else{
return null;
}
}elseif(Session::has('privilege') && Session::get('privilege')=='parent'){
$sel = DB::select(DB::raw("select a.stu_username from ag_parentsdetails a, ag_groups b, ag_studentsdetails c where b.ag_status='1' and (b.group_name='".urlencode(addslashes(trim($grpname)))."' or b.slno='".urlencode(addslashes(trim($grpname)))."') and a.stu_username=c.stu_username and c.stu_gid like concat('%,',b.slno,',%')"));
$result="";
for($i=0;$i<count($sel);$i++){
if($result){
$result=$result.",".stripslashes(urldecode($sel[$i]->stu_username));
}else{
$result=stripslashes(urldecode($sel[$i]->stu_username));
}
}
return $result;
}elseif(Session::has('privilege') && Session::get('privilege')=='student'){
$row = DB::select(DB::raw("select b.stu_username from ag_groups a, ag_studentsdetails b where a.ag_status='1' and (a.group_name='".urlencode(addslashes(trim($grpname)))."' or a.slno='".urlencode(addslashes(trim($grpname)))."') and b.stu_gid like concat('%,',a.slno,',%') and b.stu_username!='".urlencode(addslashes($uname))."'"));
if(count($row)>0){
return $row[0]->stu_username;
}else{
return null;
}
}
}

public function getstuCount($stu){
$select=DB::select(DB::raw("select count(*) as total from ".self::getawardstable()." where stu_uname='".urlencode(addslashes($stu))."' and stu_award like '%pos_%'"));
return $select[0]->total;
}
public function getstunegCount($stu){
$select=DB::select(DB::raw("select count(*) as total from ".self::getawardstable()." where stu_uname='".urlencode(addslashes($stu))."' and stu_award like '%neg_%'"));
return $select[0]->total;
}

public function getposawards(){
$select = DB::select(DB::raw("select * from ag_awardskey where awardskey like '%pos_%'"));
return $select;
}

public function getnegawards(){
$select = DB::select(DB::raw("select * from ag_awardskey where awardskey like '%neg_%'"));
return $select;
}

public function awardUpdate($user,$award,$subjectname,$gnum){

if(is_array($user)){
$fault="";
$i=0;
while($i<count($user)){
if(self::awardUpdate($user[$i],$award,$subjectname,$gnum)){
$i++;
}else{
$fault=$fault.$user.",";
$i++;
}
}
if(strlen(str_replace(',','',trim($fault)))!=0){
return trim($fault);
}else{
return true;
}
}else{
$query="insert into ".self::getawardstable()." (stu_uname,tea_uname,stu_subject,group_num,awd_date,stu_award) values ('".urlencode(addslashes($user))."','".Session::get('username')."','".urlencode(addslashes($subjectname))."','".$gnum."','".date('Y-m-d H:i:s',time())."','".urlencode(addslashes($award))."')";
if(DB::insert(DB::raw($query))){
return true;
}else{
return false;
}
}

}

public function secGetStudents($gnum){

$select = DB::select(DB::raw("select * from ag_groups where ag_status='1' and slno='".urlencode(addslashes($gnum))."'"));
if(count($select) > 0){
$query="select * from ag_studentsdetails where stu_gid not like '%,".urlencode(addslashes($gnum)).",%'";
if($select[0]->group_class!="all"){
$query=$query." and stu_class='".$select[0]->group_class."'";
}
if($select[0]->group_section!="all"){
$query=$query." and stu_section='".$select[0]->group_section."'";
}
$selec=DB::select(DB::raw($query));
if(count($selec) > 0){
return $selec;
}
}
}

public function secGroupUpdate($gnum,$sstu){


$students = $sstu;

$grpsub = DB::select(DB::raw("select * from ag_groups where ag_status='1' and slno='".urlencode(addslashes($gnum))."'"));
$subject=$grpsub[0]->teacher_subject;

for($i=0;$i<count($students);$i++){
$query1 = "select * from ag_studentsdetails where stu_username='".urlencode(addslashes($students[$i]))."' and stu_subjects like '%".urlencode(addslashes($subject))."%'";
$result = DB::select(DB::raw($query1));

if(count($result)==0){
$query2 = "update ag_studentsdetails set stu_subjects = if(stu_subjects is null, '".urlencode(addslashes($subject)).",',concat(stu_subjects,'".urlencode(addslashes(",".$subject)).",')) where stu_username = '".urlencode(addslashes($students[$i]))."'";
DB::update(DB::raw($query2));
}
}

//$query="update ag_groups set selection = if(selection is null, '".urlencode(addslashes(implode(',',$sstu)))."',concat(selection,'".urlencode(addslashes(",".implode(',',$sstu)))."')) where slno='".urlencode(addslashes($gnum))."'";
try{
//DB::update(DB::raw($query));

for($j=0;$j<count($students);$j++){
$query4 = "update ag_studentsdetails set stu_gid = if(stu_gid is null, '".urlencode(addslashes($gnum)).",',concat(stu_gid,'".urlencode(addslashes($gnum)).",')) where stu_username = '".urlencode(addslashes($students[$j]))."'";
DB::update(DB::raw($query4));
}

return true;
}catch(Exception $e){
return false;
}



}

public function removestudent($gn,$sn){
$n=0;
$gnselect = DB::select(DB::raw("select * from ag_groups where slno='".urlencode(addslashes($gn))."'"));
if(count($gnselect)==1){
$selection = stripslashes(urldecode($gnselect[0]->selection));
$students = explode(',',$selection);
$sel=array();
$j=0;
for($i=0;$i<count($students);$i++){
if($students[$i]!=$sn){
$sel[$j]=$students[$i];
$j++;
}
}
$selectionn = implode(",",$sel);
try{
DB::update(DB::raw("update ag_groups set selection='".urlencode(addslashes($selectionn))."' where slno='".urlencode(addslashes($gn))."'"));
$n=1;
}catch(Exception $e){
}
}
$snselect=DB::select(DB::raw("select * from ag_studentsdetails where stu_username='".urlencode(addslashes($sn))."'"));
if(count($snselect)==1){
$grpnums = explode(',',$snselect[0]->stu_gid);
$grpnu=array();
$l=0;
for($k=0;$k<count($grpnums);$k++){
if($grpnums[$k]!=$gn && $grpnums[$k]!=""){
$grpnu[$l]=$grpnums[$k];
$l++;
}
}
$grpgid=implode(",",$grpnu);
try{
DB::update(DB::raw("update ag_studentsdetails set stu_gid='".$grpgid.",' where stu_username='".urlencode(addslashes($sn))."'"));
$n=$n+1;
}catch(Exception $e){
}

}

if($n==2){
return true;
}else{
return false;
}
}

}
