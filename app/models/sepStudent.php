<?php
class sepStudent extends Eloquent {

public function getstudents(){
$select=DB::select(DB::raw("select * from ag_user where token='".Session::get('token')."'"));
return $select;
}

public function getGroupid($sid=""){
if($sid){
$select = DB::select(DB::raw("select stu_gid from ag_studentsdetails where userid='".urlencode(addslashes($sid))."'"));
}else{
$select = DB::select(DB::raw("select stu_gid from ag_studentsdetails where stu_username='".Session::get('username')."'"));
}
return $select;
}

public function getGroups($sid=""){
if($sid!=""){
$select = self::getGroupid($sid);
}else{
$select = self::getGroupid();
}
$groups = DB::select(DB::raw("select * from ag_groups where ag_status='1' and slno in (".rtrim($select[0]->stu_gid,",").")"));
return $groups;
}

public function getclasec(){
$result=DB::select(DB::raw("select a.stu_section,b.clasubname from ag_studentsdetails a, ag_clasubkey b where a.userid='".Session::get('userid')."' and b.clasubkey=a.stu_class"));
return $result;
}

public function getGrpname($id){
$groups = DB::select(DB::raw("select group_name from ag_groups where ag_status='1' and slno='".$id."'"));
if(count($groups)!=0){
return $groups[0]->group_name;
}else{
return false;
}
}

public function getAwards(){
$select = self::getGroupid();
$result=DB::select(DB::raw("select a.group_name,b.* from ag_groups a,ag_awards b where a.ag_status='1' and b.stu_uname='".Session::get('username')."' and a.slno=b.group_num and b.group_num in (".rtrim($select[0]->stu_gid,",").")"));
return $result;
}

public function getPos($uname){
$result=DB::select(DB::raw("select a.* from ag_awards a,ag_groups b where a.group_num=b.slno and b.ag_status=1 and a.stu_award like '%pos_%' and  a.stu_uname='".$uname."' order by a.awd_date desc"));
return $result;
}

public function getNeg($uname){
$result=DB::select(DB::raw("select * from ag_awards where stu_award like '%neg_%' and  stu_uname='".$uname."'"));
return $result;
}

public function getsimStudents(){
$result=DB::select(DB::raw("select * from ag_user where token='".Session::get('token')."' and privilege='student'"));
return $result;
}

public function getuserdetails($user){
$result = DB::select(DB::raw("select fullname from ag_user where username='".urlencode(addslashes($user))."'"));
return $result;
}

public function getStuunamefid($id){
$result = DB::select(DB::raw("select stu_username from ag_studentsdetails where userid='".urlencode(addslashes($id))."'"));
return $result;
}

}

