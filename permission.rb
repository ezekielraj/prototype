script "permission" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #inserted file permission script
     sudo chown -R www-data /srv/www/agrade/current/app/storage
     sudo chown -R www-data /srv/www/agrade/current/public/images/profile
  EOH
end